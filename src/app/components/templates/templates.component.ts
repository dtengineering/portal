import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TemplateService } from 'src/app/services/template.service';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {

  constructor( private _router: Router, private _templateService: TemplateService) { }

  public templatesList: any = [];
  public timezone: any;
  public language: any;
  public templateSurveyList: any = [];


  ngOnInit(): void {
    this.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone
    this.language = localStorage.getItem('language');
    this.getAllTemplates();
  }

  public goToCreateTemplate(){
    this._router.navigate(['/create-template'])
  }

  public getAllTemplates(){
    this._templateService.getAllTemplate('dropthought', -1, false, this.timezone, this.language, '').subscribe(data => {
      if(data) {
        this.templatesList = data?.result ? data?.result : [];
      } else {
        this.templatesList = [];
      }
    });
  }

  public previewTemplate(templateUUID){
    this._templateService.getpreviewTemplate(templateUUID).subscribe(data => {
      if(data?.success) {
        this.templateSurveyList = data?.result ? data?.result : [];
        let surveyUUID = this.templateSurveyList.templateUUID
        localStorage.setItem('language', this.language)
        this._router.navigate(['/template-preview/' + surveyUUID])
      }
    });
  }

}
