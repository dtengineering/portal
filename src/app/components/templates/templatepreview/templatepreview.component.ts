import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AccessSettingsService } from 'src/app/services/access-settings.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-templatepreview',
  templateUrl: './templatepreview.component.html',
  styleUrls: ['./templatepreview.component.css']
})
export class TemplatepreviewComponent implements OnInit {
  public selectedUUID:any;
  public welcomePage = true;
  public sanitizedCustomizedURL: any;
  public viewType = 'desktop';
  public pageType: any;

  constructor(private _route: ActivatedRoute, private router: Router, private sanitizer: DomSanitizer, private accessService: AccessSettingsService) { }

  ngOnInit() {
    this.pageType = localStorage.getItem("pageType") && localStorage.getItem("pageType") == 'gameplan' ? 'gameplan' : "template"
    this._route.params.subscribe(params => {
      console.log("params", params);
      this.selectedUUID = params['id'];
      console.log(this.selectedUUID);
      localStorage.setItem('language', 'en');
      this.getGeneratedToken(this.selectedUUID);
    });
  }

  ngOnDestroy() {
    localStorage.removeItem("pageType")
  }

  public getEUXURL() {
    return environment.TEMPLATE_PREVIEW_URL ? environment.TEMPLATE_PREVIEW_URL : 'https://stage-eux.dropthought.com/';
  }

  public getGeneratedToken(surveyUUID) {
    let type = 'dropthought'
    let env = this.getEUXURL();
    let token = surveyUUID + "$RFQ1";
    let customizedURL = `${env}en/dt/survey/${token}?source=test&&template=1&&templateType=${type}`;
    this.sanitizedCustomizedURL = this.sanitizer.bypassSecurityTrustResourceUrl(customizedURL);
  }

  public back() {
    this.router.navigate(['/templates']);
  }

  public backToGameplan(){
    this.router.navigate(['/gameplans'])
  }
}
