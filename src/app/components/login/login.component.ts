import { Component, OnInit } from '@angular/core';
import { FormControl, UntypedFormGroup, UntypedFormBuilder, Validators } from "@angular/forms"
import { RouterModule, Routes, Router, ActivatedRoute } from "@angular/router"

import { AuthenticationService } from "../../services/authentication.service"

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  returnUrl: string = "/"
  loginForm: UntypedFormGroup

  constructor(private fb: UntypedFormBuilder, private authenticationService: AuthenticationService) {
    this.loginForm = this.fb.group({
      userNameControl: [
        "",
        Validators.compose([Validators.required])
      ],
      passwordControl: [
        "",
        Validators.compose([Validators.required, Validators.minLength(1)])
      ]
    })
   }

  ngOnInit() {
    sessionStorage.clear()
  }


  loginSubmit(){
    let email = this.loginForm.controls["userNameControl"].value.trim()
    let password = this.loginForm.controls["passwordControl"].value.trim()
    this.authenticationService.authenticateUser(
      this.loginForm,
      email,
      password,
      this.returnUrl
    )
  }

}

