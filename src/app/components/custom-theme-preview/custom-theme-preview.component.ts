import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AccessSettingsService } from 'src/app/services/access-settings.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-custom-theme-preview',
  templateUrl: './custom-theme-preview.component.html',
  styleUrls: ['./custom-theme-preview.component.css']
})
export class CustomThemePreviewComponent implements OnInit {
  defaultTheme = {themeName: '', id: 'default'};
  selectedTheme:any;
  welcomePage = true;
  sanitizedCustomizedURL: any;
  viewType = 'desktop';

  constructor(private _route: ActivatedRoute, private router: Router, private sanitizer: DomSanitizer, private accessService: AccessSettingsService) { }

  ngOnInit() {
    this._route.queryParams.subscribe(params => {
      if (!params.themeName) {
        this.selectedTheme = this.defaultTheme;
      } else {
        this.selectedTheme = params;
      }
      this.getAPIKey(this.selectedTheme.themeName);
    });
  }

  getAPIKey(themeName) {
    this.accessService.getPortalDemoAPIKey().subscribe((res: any) => {
      if (res.success) {
        let data = res.result;
        let apiKey = data && data.apiKey ? data.apiKey : '';
        let surveyUUID = data && data.surveyUUID ? data.surveyUUID : '';
        this.getGeneratedToken(surveyUUID, apiKey, themeName);
      }
    });
  }

  getGeneratedToken(surveyUUID, apiKey, themeName) {
    this.accessService.getPortalDemoSurveyToken(surveyUUID, apiKey).subscribe(
      data => {
        if (data && data.success) {
          let apiResult = data && data.result && data.result[0] ? data.result[0] : null;
          let url = apiResult && apiResult.longUrl ? apiResult.longUrl : null;
          let customizedURL = `${url}?source=test&&themeName=${themeName}`;
          this.sanitizedCustomizedURL = this.sanitizer.bypassSecurityTrustResourceUrl(customizedURL);
        }
      }
    )
  }

  loadTheme(themeName) {
    let FRONTEND_EUX_URL = '' //environment?.FRONTEND_EUX_URL ? environment?.FRONTEND_EUX_URL : '';
    let surveyToken = '' //environment?.STATIC_SURVEY_TOKEN ? environment?.STATIC_SURVEY_TOKEN : '';
    let customizedURL = `${FRONTEND_EUX_URL}/en/dt/survey/${surveyToken}?source=test&&themeName=${themeName}`;
    this.sanitizedCustomizedURL = this.sanitizer.bypassSecurityTrustResourceUrl(customizedURL);
  }

  back() {
    if (this.selectedTheme && this.selectedTheme.client_id) {
      localStorage.setItem('clientDetailsTab', 'access')
      this.router.navigate(['/client-details', this.selectedTheme.client_id]);
    } else {
      this.router.navigate(['/custom-theme']);
    }
  }

}
