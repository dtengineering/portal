import { Component, OnInit } from '@angular/core';
import { FormControl, UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';

import { AuthenticationService } from "../../services/authentication.service";
import { checkPassword, updatePwdPatternValidator, matchingPasswords } from '../../validations/validators';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  public updateUserForm: UntypedFormGroup
  public currentpassword: string;
  public newpassword: string;
  public confirmpassword: string;
  public passwordCheck: boolean = false
  public confirmPasswordFlag: boolean = false
  public passwordUpdated: boolean = false

  //user logout

  constructor(public fb: UntypedFormBuilder, private authenticationService: AuthenticationService) {

    this.updateUserForm = this.fb.group({
      currentPasswordControl: ["", Validators.required],
      newPasswordControl: ["", Validators.compose([Validators.required, updatePwdPatternValidator])],
      confirmPasswordControl: ["", Validators.compose([Validators.required, Validators.minLength(8)])]
    }, {
        validator: matchingPasswords('newPasswordControl', 'confirmPasswordControl')
      });

   }

  ngOnInit() {
  }

  updatePassword(){
    let updateUserFlag = true
    if (this.updateUserForm.controls.currentPasswordControl.hasError("required")) {
      updateUserFlag = false
    }

    if (this.updateUserForm.controls.currentPasswordControl.hasError("invalidPassword")) {
      updateUserFlag = false
    }

    if (this.updateUserForm.controls.newPasswordControl.hasError("required")) {
      updateUserFlag = false
    }

    if (this.updateUserForm.controls.newPasswordControl.hasError("invalidLength")) {
      updateUserFlag = false
    }

    if (this.updateUserForm.controls.newPasswordControl.hasError("invalidNumber")) {
      updateUserFlag = false
    }

    if (this.updateUserForm.controls.newPasswordControl.hasError("invalidCapital")) {
      updateUserFlag = false
    }

    if (updateUserFlag) {
      let inputObject = {}
      inputObject["password"] = this.updateUserForm.controls.newPasswordControl.value
      inputObject["modifiedBy"] = sessionStorage.getItem("userUUID")
      let inputJson = JSON.stringify(inputObject)
      if(this.newpassword == this.confirmpassword){
        this.authenticationService.updateUserPassword(inputJson).subscribe(
          data => {
            sessionStorage.setItem("currentpassword", this.updateUserForm.controls.newPasswordControl.value)
            this.updateUserForm.controls.currentPasswordControl.setValue("")
            this.updateUserForm.controls.newPasswordControl.setValue("")
            this.updateUserForm.controls.confirmPasswordControl.setValue("")
            this.passwordUpdated = true
          }
        );
      }
    }
  }

  /**
   * Function to check password
   */
  checkPassword(){
    if(this.updateUserForm.controls.currentPasswordControl.value == sessionStorage.getItem("currentpassword")){
      this.passwordCheck = true
    }
    else{
      this.passwordCheck = false
    }
    
  }
  
  /**
   * Function to check confirm password
   */
  confirmPasswordCheck(){
    if(this.updateUserForm.controls.newPasswordControl.value == this.updateUserForm.controls.confirmPasswordControl.value){
      this.confirmPasswordFlag = true
    }
    else
    this.confirmPasswordFlag = false
  }

}
