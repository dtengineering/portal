import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ClientServiceService } from '../../services/client-service.service'

@Component({
  selector: 'app-grant-approval',
  templateUrl: './grant-approval.component.html',
  styleUrls: ['./grant-approval.component.css']
})
export class GrantApprovalComponent implements OnInit {

  public timezone
  public approval
  businessId
  email
  id

  constructor(private route: ActivatedRoute, private _client: ClientServiceService, private _router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.approval = params["id1"]
      this.businessId = params["id2"]
      this.id = params["id3"]
      this.email = params["id4"] 
      this.grantApproval()
    })
  }

  //function to approve the request
  public grantApproval(){
    let requestBody = {}
    requestBody["id"] = this.id
    requestBody['businessId'] = this.businessId
    requestBody['approvedBy'] = decodeURIComponent(escape(window.atob((this.email))))
    requestBody['approval'] = this.approval
    this._client.approveRequest(requestBody).subscribe(data=>{
      console.log(data)
      // if(data.success)
      // localStorage.setItem('clientDetailsTab', 'access')
      // this._router.navigate(['client-details/' + this.businessId + '/'])
    })
  }

}