import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { RouterModule, Routes, Router } from '@angular/router';
import { element } from 'protractor';
import { LoginComponent } from '../login/login.component';

declare var $: any
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private _router: Router) { }

  @Output() emitStopPageChangeToAccessSettingsTab: EventEmitter<any> = new EventEmitter();
  @Input() newClient: boolean
  @Input() pristine: boolean

  ngOnInit() {
    //this is navigation component on init
    this.loadScripts()
  }

  loadScripts(){
    $(document).mouseup(function (e){
      var downloadDiv = document.getElementById("accountDropdown");
        var topMenuDiv = $(downloadDiv);
        if(downloadDiv){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $(".accountDropdown").hide(200);
          }
        }
    });
  }

  public navigateToCustomThemeScreen(){
    this._router.navigate(['/custom-theme'])  
   }

  public navigateToClientScreen(){
    console.log(this.pristine)
    if(localStorage.getItem("accessSettingsTab") == "1" && this.newClient || localStorage.getItem("accessSettingsTab") == "1" && !this.pristine){
      this.emitStopPageChangeToAccessSettingsTab.emit('client-screen')
    }
    else this._router.navigate(['/client-screen' ])
    if (this._router.url.includes('custom-theme')) {
      this._router.navigate(['/client-screen' ])
    }
  }

  public navigateToAccountPage(){
    if(localStorage.getItem("accessSettingsTab") == "1" && this.newClient || localStorage.getItem("accessSettingsTab") == "1" && !this.pristine){
      this.emitStopPageChangeToAccessSettingsTab.emit('client-screen')
    }
    else this._router.navigate(['/account'])
    
  }

  public logOut(){
    sessionStorage.clear()
    localStorage.clear()
    this._router.navigate(['/login'])
  }
  
  public accountToggle(){
    var element = document.getElementById("accountDropdown")
    if(element.style.display === 'none'){
      element.style.display = 'block'
    }
    else{
      element.style.display = 'none'
    }
  }

  public templateToggle(){
    var element = document.getElementById("templateDropdown")
    if(element.style.display === 'none'){
      element.style.display = 'block'
    }
    else{
      element.style.display = 'none'
    }
  }

  public navigateToCreateTemplates(){
    this._router.navigate(['/templates'])  
  }

  public navigateToCreateGameplans(){
    this._router.navigate(['/gameplans'])
  }

}
