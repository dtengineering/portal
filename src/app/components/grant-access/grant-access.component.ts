import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ClientServiceService } from '../../services/client-service.service'

@Component({
  selector: 'app-grant-access',
  templateUrl: './grant-access.component.html',
  styleUrls: ['./grant-access.component.css']
})
export class GrantAccessComponent implements OnInit {
  public userUUID: string
  public contractStartDate : string
  public contractEndDate : string
  public timezone:any
  public success: boolean = false

  constructor(private route: ActivatedRoute, private _client: ClientServiceService) { }

  ngOnInit() {
    this.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone
    this.userUUID = window.location.search.substr(window.location.search.indexOf('useruuid') + 9, 36);
    this.contractStartDate = window.location.search.substr(window.location.search.indexOf('fromdate') + 9, 21).split("%20").join(" ")
    this.contractEndDate = window.location.search.substr(window.location.search.indexOf('todate') + 7, 21).split("%20").join(" ")
    setTimeout(() => {
      this.grantClientAccess()
    }, 100);
  }

  public grantClientAccess(){
    let requestBody = {}
    requestBody["userUUID"] = this.userUUID
    requestBody['timezone'] = this.timezone
    requestBody['fromDate'] = this.contractStartDate
    requestBody['toDate'] = this.contractEndDate
    this._client.grantAccess(requestBody).subscribe(data=>{
      console.log(data)
      if(data.success)this.success = true
    })
  }

}
