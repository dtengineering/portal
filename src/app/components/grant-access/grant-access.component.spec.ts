import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GrantAccessComponent } from './grant-access.component';

describe('GrantAccessComponent', () => {
  let component: GrantAccessComponent;
  let fixture: ComponentFixture<GrantAccessComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GrantAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrantAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
