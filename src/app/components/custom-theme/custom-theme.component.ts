import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { AccessSettingsService } from 'src/app/services/access-settings.service';


@Component({
  selector: 'app-custom-theme',
  templateUrl: './custom-theme.component.html',
  styleUrls: ['./custom-theme.component.css']
})
export class CustomThemeComponent implements OnInit {
  customthemesList: any = [];

  constructor(private _router: Router, public _accessSettings: AccessSettingsService) { }

  ngOnInit(): void {
    this.getAllThemes();
  }

  public getAllThemes() {
    this._accessSettings.getAllThemes().subscribe(data=>{
      if(data?.success) {
        this.customthemesList = data?.result ? data?.result : [];
      } else {
        this.customthemesList = [];
      }
    });
  }    

  goToPreview(theme) {
    let themeObj = {themeName: theme.themeName, id: theme.themeUUID};
    this._router.navigate(['/theme-preview'],{ queryParams: themeObj});
  }

}
