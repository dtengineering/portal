import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {AccessSettingsService} from '../../../services/access-settings.service';
import {ClientServiceService} from '../../../services/client-service.service';
import {environment} from '../../../../environments/environment';


declare var $: any;

@Component({
  selector: 'app-access-settings',
  templateUrl: './access-settings.component.html',
  styleUrls: ['./access-settings.component.css']
})
export class AccessSettingsComponent implements OnInit {

  //Obj for
  public businessSettings: any;
  public advancedTextAnalyticsModel: any;
  public advancedTextAnalyticsModelByAllSurvey: any;
  public advancedTextAnalyticsModelDuplicate: any;
  public advancedTextAnalyticsCustomModel: any;
  public advancedTextAnalyticsMonkeyLearnModel: any;
  public advancedTextAnalyticsAllModel: any;
  public programsList: any = [];
  public defaultProgramsList: any = [];
  public historicalProgramsList: any = [];
  public apiKeyList: any;
  public pendingSurveyUuidList: any = [];
  public tempPendingSurveyUuidList: any = [];
  public activeSurveyUuidList: any = [];
  public activeCategorymlIdList: any = [];
  public activeSentimentIdList: any = [];
  public activeWordCloudList: any = [];
  public pendingCategorymlIdList: any = [];
  public pendingSentimentIdList: any = [];
  public pendingWordCloudList: any = [];

  //publish options
  public email: boolean = true;
  public sms: boolean;
  public kiosk: boolean;
  public sharableLink: boolean;
  public qrCode: boolean;
  public refreshKioskQr: boolean;
  public whatsapp: boolean;

  //Feature Access
  public textAnalytics: boolean;
  public categorySentimentAnalysis: boolean;
  public logicOptions: boolean;
  public segmentOptions: boolean;
  public advancedScheduling: boolean;
  public multilingualSurveys: boolean;
  public webDashboard: boolean = true;
  public mobileDashboard: boolean;
  public arabicDashboard: boolean;
  public notificationCenter: boolean;
  public downstreamTrigger: boolean;
  public metadataQuestion: boolean;
  public bitly: boolean;
  public preferredMetric: boolean;
  public cohortAnalysis: boolean;
  public categoryAnalysis: boolean;
  // public intentAnalysis: boolean
  public emotionAnalysis: boolean;
  public advancedTxtAnalytics: boolean;
  public advancedTemplates: boolean;
  public advancedTemplatesPopup: boolean;
  public showChecklistPopup: boolean;
  public advancedTemplateValues: any;
  public whatfix: boolean;
  public apiKey: boolean;
  public enableApiKey: boolean = false;
  public apiKeyValue: any;
  public showApiKey: boolean;
  public multipleSublink: Boolean;
  public hippaStatus: any;

  //for advanced analysis
  public advancedAnalysis: boolean;
  public advancedAnalysisURL: string;

  //unique link for each contact Email, sms & whatsapp
  public exportLinks: boolean;
  public uniqueLinkEmail: boolean;
  public uniqueLinkSms: boolean;
  public uniqueLinkWhatsapp: boolean;

  //Text Analytics
  public category: any;
  public modelName: string;
  public tags: string;
  public radioSelected: any;
  public searchModelByEachSurvey: any;
  public searchModelByAllSurvey: any;
  public filterdOptions = [];
  public catMlId: any;
  public activeRadioSelectedItem: any;
  public pendingRadioSelectedItem: any;
  public surveyUuidIndex: any;
  public modelIndex: any;
  public customModelsList: any = [];
  public monkeyLearnModelsList: any = [];
  public allModelsList: any = [];

  //

  //Inputs from Client Details
  @Input() newClient: boolean;
  @Input() clientUUID: string;
  @Input() kingName: string;
  @Input() startDate: string;
  @Input() endDate: string;

  public contractStartDate: any;
  public successfullyUpdated: boolean;


  //Output back to client details
  @Output() emitPristineToClientDetails: EventEmitter<any> = new EventEmitter();
  @Output() emitDisableSaveToClientDetails: EventEmitter<any> = new EventEmitter();
  @Output() emitEditClient: EventEmitter<any> = new EventEmitter();
  @Output() emitEnableUpdateButton: EventEmitter<any> = new EventEmitter();
  @Output() emitIntegrationEnable: EventEmitter<any> = new EventEmitter();


  //accessed by client details component to determine if there is any change on this page
  public pristine: boolean = true;

  //
  public noticePopup: boolean = false;


  //variables for Limitations and Usage
  public noOfUsers: number;
  public noOfActivePrograms: number;
  public noOfActiveTriggers: number;
  public noOfResponses: number;
  public noOfMetrics: number;
  public noOfEmailInvites: number;
  public noOfSMSInvites: number;
  public noOfRecipients: number;
  public noOfLists: number;
  public noOfAPIRequests: number;
  public noOfFiltersSaved: number;
  public dataRetention: number;
  public noOfIntegrations: number;
  public noOfKiosks: number;
  public limitUsage: any;
  public noOfWhatsapp: number;

  public emailResponseLimitWarning: boolean = false;
  public smsResponseLimitWarning: boolean = false;
  public whatsappResponseLimitWarning: boolean = false;
  public textAnalyticsPopup: boolean = false;
  public multiFactorAuthenticationPopup: boolean = false;
  public textAnalyticsEditPlan: boolean = false;
  public kioskPopup: boolean;
  public apiKeyPopup: boolean = false;
  public apiValidationMessage: boolean = false;
  public selectPlanText: boolean = false;
  public selectDuration: boolean = false;
  public contractEndDate: any;

  public advancedText: any;
  public durationTextArr: any = [];
  public durationText: any;
  public durationValue: number = 1;
  public planValue: number = 1;
  public selectModelSurveyCategory: boolean = false;
  public selectAnalysis: boolean = false;
  public selectOneModelToAllSurveys: boolean = false;
  public selectModelsForEachSurvey: boolean = false;
  public isCustomModel: boolean;
  public isFilterAll: boolean = true;

  public filterModels: any = ['All', 'Custom models', 'MonkeyLearn models'];
  public selectedModelFilter: string = 'All';
  //public selectedModelFilterByEachSurvey: string = "Select a model"
  // public selectedModelFilterByEachSurvey: any = []
  public activeModelFilterByEachSurvey: any = [];
  public pendingModelFilterByEachSurvey: any = [];
  public pendingModelFilterByEachSurveyDisplay: any = [];

  public dataDuration: any;
  public textAnalyticsPlan: any = '1';
  public tempStartDate: any;
  public tempEndMinDate: any;
  public tempEndMaxDate: any;
  public noAnalyticsResponseFlag: boolean = false;

  public modelFlag: boolean = true;
  public modelType: any = 'one';
  public activeElizaAnalyticsData: any;
  public pendingElizaAnalyticsData: any;
  public showEditButton: boolean = false;

  // DTV-3867 variables

  public advancedTemplate: boolean = false;
  public timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  public industry_value: any = [];
  public showIndustry: boolean;
  public theme_value: any = [];
  public showTheme: boolean;
  public industry: any;
  public theme: any;
  public searchTerm: any;
  public selectedTemplate: any = [];
  public templatePayload: any = {};
  public updatedTemplate: any;
  public templateCount: any;
  public unselectedTemplate: any = [];
  public selectIndustry: any = 'Select Industry';

  public showApiPopup: boolean = false;
  public showSpinner: boolean = false;
  public updateClicked: boolean = false;

  public showRequestApprovalPopup: boolean = false;
  public resendApproval: number = 0;
  public requestApprovalStatus: string = 'unsent';
  public sendRequestApprovedByCS: boolean = false;
  public pendingTextAnalyticsConfig: any;
  public activeTextAnalyticsConfig: any;
  public selectedTextAnalyticsPlan: any;
  public pendingSelectedTextAnalyticsSurveys: any = [];
  public activeSelectedTextAnalyticsSurveys: any = [];
  public selectedTextAnalyticsSurveyIds: any = [];
  public pendingSelectedDataDuration: any;
  public activeSelectedDataDuration: any;
  public textAnalyticsPlans: any = ['Basic sentiment categorization', 'Advanced Text Analytics'];

  public limitValue: any;
  public showTextAnalyticsDetails: boolean = false;

// DTV-3483 //
  public metricCrossTabulation: boolean;
  public disableRequestApproval: boolean = true;

// DTV-5478 fromEmailAndNameSettings
  public fromCustomization: boolean;

// DTV - 5877 Integrations
  public integrations: boolean = false;
  public dynamicLinks: boolean;

//DTV-7439
  public mfa: boolean;
  public focusMetric: boolean;
  public optionForAll: boolean = false;
  public mandatoryForAll: boolean = false;
  public selectedOption: any;
  public authenticationPopup: any;
// DTV-5482
  public globalRespondentsTracker: boolean;

  //DTV-4452
  public openSurveyPopup: boolean = false;
  public businessUUID: any;
  public isSelectAllProgram: boolean = false;
  public checkedProgramList: any = [];
  public activeCheckedProgramList: any = [];
  public checkeProgramName: any = [];
  public searchAllSurvey: any;
  public filteredPrograms: any;
  public noProgramErrorMsg: boolean = false;
  public defaultAllProgramList: any;
  public basicSentimentSurveyListApproved: any = [];
  public showBasicEditButton: boolean = false;
  public basicSelectAllFlag: boolean = false;

  //DTV-8024
  public predefinedQuestionLibrary: boolean;

  //CSUP-748
  public approvalEmailList: any = {};

  public showEditSelectBasicSurveys: boolean = false;

  //DTV-5877 - uncomment the integrations toggle for sprint-50
  public kiosksCount: number;
  public modalInd: any = [];

  //Advanced report
  public advancedReport: boolean;
  public focusMetricDefaultRecom: boolean = false;
  public focusMetricUserDefinedRecom: boolean = false;
  public focusMetricDefaultRecomOption: any = '1';
  public focusMetricUserDefinedRecomOption: any = '1';

  //DTV-8223 - TA Intent Analysis
  selectModelSurveyIntent: boolean = false;
  showIntentAnalysisSurvey: boolean = false;
  modelTypeIntent: string = 'one';
  // intentMappingEach : Object = {"466e5389-3fe9-47d5-bf37-6e75cc0a7e20":22,"7f42d625-8b44-4b7e-a1fa-49616b3198f6":22,"c0a44b90-04f8-4aca-bc40-07640f414d41":22}
  //intentMappingEach : Object = {}
  showEditIntentButton: boolean = false;
  public modelFlagIntent: boolean = true;
  public selectIntentModelSurveyCategory: boolean = false;
  public selectIntentAnalysis: boolean = false;
  public selectOneIntentModelToAllSurveys: boolean = false;
  public selectIntentModelsForEachSurvey: boolean = false;
  public checkedIntentModelProgramList: any = [];
  public submitIntentModelProgramList: any = [];
  public intentModelSelectAllFlag: boolean = false;

  // previouslyUsedQuestionLibrary
  public previouslyUsedQuestionLibrary: boolean;

  public chatbot: boolean;
  public customthemesList: any = [];
  public customThemes: any = false;
  public selectedCustomThemes: any = [];
  public customthemesPopUpOpen: any = false;
  public tempCustomThemeSelectionList: any = [];
  public programThrottling: boolean;
  public closeLoop: boolean;
  public gamePlan: boolean;
  public textAnalyticsConfigId: number;

  //custom Dashboard DTV-8766
  public customDashboard: boolean;
  public auditProgram: boolean = false;
  public allowCollaboration: boolean = false;
  //Calculate correlation value
  public correlationValue: boolean;
  public correlationPopup: boolean;
  public categoryAndSentimentAnalysis: boolean = false;
  public intentAnalysis: boolean = false;
  public planConfigIndent: boolean = false;
  public planConfigSentiment: boolean = false;
  public closeLoopPopUp: boolean;
  public kioskQrRefreshPopUp: boolean = false;
  public dtPlanName: any;
  setToastersAfterThemeApiCall: any = {
    'success': false,
    'error': false
  };

  // DTV-10603
  public notes: boolean = false;
  public taskManager: boolean = false;
  public aiSurveys: boolean = false;
  public aiSurveysLimit = -1;
  public submissionDelay: boolean = false;
  dtLitePlan: boolean = false;

  //DTV-12910
  public responseQuota: boolean = false
  public customDashboardBuilder: boolean = false

  constructor(private _client: ClientServiceService, private _accessSettings: AccessSettingsService, private _cd:ChangeDetectorRef, private _router: Router, private _route: ActivatedRoute) { }


  // Program Reports
  public programOverview: boolean;
  public programMetric: boolean;

  public uniqueLinkPopup: boolean = false;
  public uniqueLinkType: any;

  // DTV-12202
  public mandatorySSO: boolean = false;
  public sso_microsoft: boolean = false;
  public sso_google: boolean = false;
  public sso_apple: boolean = false;
  public checkSSO: boolean = false;

  // DTV-13021
  public addLTI: boolean = false
  public developerKey: any
  public developerId: any
  public domain: any

  ngOnInit() {
    this.loadScripts();
    this.dtPlanName = JSON.parse(localStorage.getItem('planName'));
    this.approvalEmailList = !(environment.production || environment.prodme) ? ['cbrown@dropthought.com', 'hema@dropthought.com', 'sabarishoneplus@gmail.com', 'anirudh@dropthought.com', 'deepak@dropthought.com', 'aishwarya@dropthought.com', 'bestem@dropthought.com', 'selvi@dropthought.com', 'mani.s@dropthought.com'] : ['anirudh@dropthought.com', 'deepak@dropthought.com', ' heena.n@dropthought.com', 'sunnyg@dropthought.com', 'hema@dropthought.com', 'selvi@dropthought.com', 'arone@dropthought.com'];
    console.log(this.approvalEmailList);
    this.getAllTextAnalyticsModels();
    this.getIndustry();
    this.getLimits();
    this.getAllThemes();
    this.selectedOption = JSON.parse(localStorage.getItem('selectedOption'));
    if (!this.newClient) {
      this.getLimitUsage();
      this.getBusinessDetails();
    } else {
      this.hippaStatus = parseInt(localStorage.getItem('hippaStatus'));
      this.checkSave();
    }
    this.templatePayload.templates = [];
  }

  /* function to get themes list */
  public getAllThemes() {
    this._accessSettings.getAllThemesByClientId(this.clientUUID).subscribe(data => {
      if (data?.success) {
        this.customthemesList = data?.result ? data?.result : [];
        this.selectedCustomThemes = this.customthemesList.filter((item: any) => item.isThemeApplied);
        this.customThemes = this.selectedCustomThemes.length > 0 ? true : false;
      } else {
        this.customthemesList = [];
      }
    });
  }

  public loadScripts() {
    console.log($().jquery);
    $(document).ready(function() {
      $('input[type="number"]').each(function() {
        $(this).on('input', function() {
          if ($(this).val() > Number($(this).attr('max'))) {
            var val = $(this).attr('max');
            $(this).val(val);
          }
        });
      });
      $('input[type="number"]').each(function() {
        $(this).on('input', function() {
          if ($(this).val() < Number($(this).attr('min'))) {
            var val = $(this).attr('min');
            $(this).val(val);
          }
        });
      });
    });
    $('.moreless-button').click(function() {
      $('.show-more').slideToggle();
      if ($('.moreless-button').text() == 'show less') {
        $(this).text('Show all');
      } else {
        $(this).text('show less');
      }
    });

    let self = this;
    let startMinDate = new Date(this.tempStartDate);
    let endMinDate = new Date(this.tempEndMinDate);
    let endMaxDate = new Date(this.tempEndMaxDate);
    $(document).ready(function() {
      $('.start-date').datepicker({
        minDate: startMinDate,
        dateFormat: 'M dd yy',
        onSelect: function(dateText, inst) {
          var dateAsString = dateText; //the first parameter of this function
          var dateAsObject = $(this).datepicker('getDate'); //the getDate method
          self.onStartInput(dateAsString);
        }
      });
      $('.end-date').datepicker({
        minDate: endMinDate,
        maxDate: endMaxDate,
        dateFormat: 'M dd yy',
        onSelect: function(dateText, inst) {
          var dateAsString = dateText; //the first parameter of this function
          var dateAsObject = $(this).datepicker('getDate'); //the getDate method
          self.onEndInput(dateAsString);
        }
      });
    });
  }

  // function to get the business details
  public getBusinessDetails() {
    this._accessSettings.getBusiness(this.clientUUID).subscribe(data => {
      console.log(data);
      // if(!data.result){
      //   this.newClient = true
      //   this.emitEditClient.emit("false")
      //   return
      // }
      this.businessSettings = data.result;

      this.email = this.businessSettings.email == '1' ? true : false;
      this.sms = this.businessSettings.sms == '1' ? true : false;
      this.kiosk = this.businessSettings.kiosk == '1' ? true : false;
      this.sharableLink = this.businessSettings.sharableLink == '1' ? true : false;
      this.qrCode = this.businessSettings.qrcode == '1' ? true : false;
      this.whatsapp = this.businessSettings.whatsapp == '1' ? true : false;

      this.textAnalytics = this.businessSettings.textAnalytics == '1' ? true : false
      this.logicOptions = this.businessSettings.logic == '1' ? true : false
      this.segmentOptions = this.businessSettings.segment == '1' ? true : false
      this.advancedScheduling = this.businessSettings.advancedSchedule == '1' ? true : false
      this.multilingualSurveys = this.businessSettings.multiSurveys == '1' ? true : false
      this.webDashboard = this.businessSettings.english == '1' ? true : false
      this.mobileDashboard = this.businessSettings.mobileApp == '1' ? true : false
      this.taskManager = this.businessSettings?.taskManager == '1' ? true : false
      this.aiSurveys = this.businessSettings?.aiSurveys == '1' ? true : false
      this.responseQuota = this.businessSettings?.responseQuota == '1' ? true : false
      this.customDashboardBuilder = this.businessSettings?.customDashboardBuilder == '1' ? true : false
      this.arabicDashboard = this.businessSettings.arabic == '1' ? true : false
      this.notificationCenter = this.businessSettings.notification == '1' ? true : false
      this.downstreamTrigger = this.businessSettings.trigger == '1' ? true : false 
      this.metadataQuestion = this.businessSettings.metadataQuestion == '1' ? true : false
      this.bitly = this.businessSettings.bitly == '1' ? true : false
      this.preferredMetric = this.businessSettings.preferredMetric == '1' ? true : false
      this.cohortAnalysis = this.businessSettings.cohortAnalysis == '1' ? true : false
      this.categoryAnalysis = this.businessSettings.categoryAnalysis == '1' ? true : false
      this.emotionAnalysis = this.businessSettings.emotionAnalysis == '1' ? true : false
      // this.intentAnalysis = this.businessSettings.intentAnalysis == '1' ? true : false
      this.advancedTxtAnalytics = this.businessSettings.advancedTextAnalytics == '1' ? true : false;
      this.advancedTemplates = this.businessSettings.advancedTemplates == '1' ? true : false;
      this.whatfix = this.businessSettings.whatfix == '1' ? true : false;
      this.apiKey = this.businessSettings.apiKey == '1' ? true : false;
      this.multipleSublink = this.businessSettings.multipleSublink == '1' ? true : false;
      this.advancedAnalysis = this.businessSettings.advancedAnalysis == '1' ? true : false;
      this.exportLinks = this.businessSettings.exportLinks == '1' ? true : false;
      this.uniqueLinkEmail = JSON.parse(this.businessSettings.uniqueLinks).uniqueLinkEmail == '1' ? true : false;
      this.uniqueLinkSms = JSON.parse(this.businessSettings.uniqueLinks).uniqueLinkSms == '1' ? true : false;
      this.uniqueLinkWhatsapp = JSON.parse(this.businessSettings.uniqueLinks).uniqueLinkWhatsapp == '1' ? true : false;
      this.sso_microsoft = JSON.parse(this.businessSettings.sso).microsoftSSO == '1' ? true : false;
      this.sso_google = JSON.parse(this.businessSettings.sso).googleSSO == '1' ? true : false;
      this.sso_apple = JSON.parse(this.businessSettings.sso).appleSSO == '1' ? true : false;
      if (this.sso_microsoft || this.sso_google || this.sso_apple) {
        this.mandatorySSO = true;
      } else {
        this.mandatorySSO = false;
      }
      // from Email & Name Settings
      this.fromCustomization = this.businessSettings.fromCustomization == '1' ? true : false;
      this.predefinedQuestionLibrary = this.businessSettings.predefinedQuestionLibrary == '1' ? true : false;
      this.integrations = this.businessSettings.integrations == '1' ? true : false;
      this.dynamicLinks = this.businessSettings.dynamicLinks == '1' ? true : false;
      this.mfa = this.businessSettings.mfa == '1' || this.businessSettings.mfa == '2' ? true : false;
      this.focusMetric = JSON.parse(this.businessSettings.focusMetric).focusMetric == '1' ? true : false;
      this.focusMetricDefaultRecomOption = JSON.parse(this.businessSettings.focusMetric).defaultRecommendation;
      this.focusMetricUserDefinedRecomOption = JSON.parse(this.businessSettings.focusMetric).userDefinedRecommendation;
      this.focusMetricDefaultRecom = this.focusMetricDefaultRecomOption != 0 ? true : false;
      this.focusMetricUserDefinedRecom = this.focusMetricUserDefinedRecomOption != 0 ? true : false;

      this.advancedReport = this.businessSettings.advancedReport == '1' ? true : false;
      this.programMetric = JSON.parse(this.businessSettings.programFeatures).programMetric == '1' ? true : false;
      this.programOverview = JSON.parse(this.businessSettings.programFeatures).programOverview == '1' ? true : false;
      this.previouslyUsedQuestionLibrary = this.businessSettings.previouslyUsedQuestionLibrary == '1' ? true : false;
      this.chatbot = this.businessSettings.chatbot == '1' ? true : false;
      this.correlationValue = this.businessSettings.rCoefficient == '1' ? true : false;
      //DTV-8766 custom dashboard
      this.customDashboard = this.businessSettings.customDashboard == '1' ? true : false;
      this.customThemes = this.businessSettings.customThemes == '1' ? true : false;
      this.programThrottling = this.businessSettings.programThrottling == '1' ? true : false;
      this.closeLoop = this.businessSettings.closeLoop == '1' ? true : false;
      this.gamePlan = this.businessSettings.gamePlan == '1' ? true : false;
      this.notes = this.businessSettings.notes == '1' ? true : false;
      this.submissionDelay = this.businessSettings.submissionDelay == '1' ? true : false;
      this.refreshKioskQr = this.businessSettings.refreshKioskQr == '1' ? true : false;
      if (this.mfa) {
        if (this.selectedOption == 2) {
          this.optionForAll = true;
        } else {
          this.mandatoryForAll = true;
        }
      }
      //DTV-9018
      this.auditProgram = this.businessSettings.auditProgram == '1' ? true : false,
        this.allowCollaboration = this.businessSettings.collaboration == '1' ? true : false,
        //DTV-3483//
        this.metricCrossTabulation = this.businessSettings.metricCorrelation == '1' ? true : false;

      this.globalRespondentsTracker = this.businessSettings.respondentTracker == '1' ? true : false;
      if (this.advancedAnalysis) {
        this.getAdvancedAnalysisURL();
      }
      if (!this.apiKey) {
        this.apiKeyValue = '';
        this.enableApiKey = false;
      }

      // this.noOfUsers = data.result.noOfUsers == -1 ? 0 : data.result.noOfUsers
      if (this.dtPlanName?.planName == 'DT Lite Pro' || this.dtPlanName?.planName == 'DT Lite') {
        this.noOfUsers = data.result.noOfUsers == -1 ? 99999 : data.result.noOfUsers;
        this.noOfFiltersSaved = data.result.noOfFilters == -1 ? 99999 : data.result.noOfFilters;
        this.aiSurveysLimit = data?.result?.aiSurveysLimit ? data.result.aiSurveysLimit : 3;
        if (this.aiSurveysLimit > 3 || this.aiSurveysLimit < 1) {
          this.aiSurveysLimit = 3;
        }
        this.dtLitePlan = true;
      } else {
        this.noOfUsers = data.result.noOfUsers == -1 ? 0 : data.result.noOfUsers;
        this.noOfFiltersSaved = data.result.noOfFilters == -1 ? 0 : data.result.noOfFilters;
        this.aiSurveysLimit = data?.result?.aiSurveysLimit ? data.result.aiSurveysLimit : -1;
        if (!this.aiSurveys) {
          this.aiSurveysLimit = -1;
        }
        this.dtLitePlan = false;
      }
      this.noOfActivePrograms = data.result.noOfActivePrograms;
      this.noOfActiveTriggers = data.result.noOfTriggers;
      this.noOfResponses = data.result.noOfResponses;
      this.noOfMetrics = data.result.noOfMetrics;
      this.noOfEmailInvites = data.result.noOfEmailsSent;
      this.noOfSMSInvites = data.result.noOfSms;
      this.noOfAPIRequests = data.result.noOfApi;
      // this.noOfFiltersSaved = data.result.noOfFilters == -1 ? 0 : data.result.noOfFilters
      this.noOfLists = data.result.noOfLists;
      this.noOfRecipients = data.result.noOfRecipients;
      this.dataRetention = data.result.dataRetention == -1 ? 0 : data.result.dataRetention;
      this.noOfIntegrations = data.result.noOfIntegrations == -1 ? 0 : data.result.noOfIntegrations;
      this.noOfKiosks = data.result.noOfKiosks == -1 ? 0 : data.result.noOfKiosks;
      this.kiosksCount = this.noOfKiosks;
      this.noOfWhatsapp = data.result.noOfWhatsapp;
      this.categoryAndSentimentAnalysis = this.businessSettings.textAnalyticsConfig.categoryAndSentimentAnalysis == '1' ? true : false;
      this.intentAnalysis = this.businessSettings.textAnalyticsConfig.intentAnalysis == '1' ? true : false;
      if (this.businessSettings.textAnalytics == '1') {
        this.textAnalyticsPopup = true;
        setTimeout(() => {
          this.getTextAnalyticsData();
        }, 200);
      } else {
        this.textAnalyticsPopup = false;
      }
      if (this.businessSettings.kiosk == '1') {
        this.kioskPopup = true;
      } else {
        this.kioskPopup = false;
      }
      let currentDate = new Date();
      if (this.businessSettings.apiKey == '1' && new Date(this.endDate) >= currentDate) {
        this.enableApiKey = true;
        this.getApiKey();
      } else {
        this.enableApiKey = false;
        this.apiKey = false;
      }
      // DTV-13021 story changes starts
      this.addLTI = this.businessSettings.lti == '1' ? true : false
      if(this.businessSettings.ltiConfig){
        this.developerId = JSON.parse(this.businessSettings.ltiConfig).developerId
        this.developerKey = JSON.parse(this.businessSettings.ltiConfig).developerKey
        this.domain = JSON.parse(this.businessSettings.ltiConfig).domain
      }
      // DTV-13021 story changes ends      
    })
    
    setTimeout(() => {
      this.getTextAnalyticsData();
    }, 1000);
    //this.getModelAppliedToBusiness(this.clientUUID)
  }

  customThemeChecked(checked) {
    if (checked) {
      this.customThemes = true;
      this.customthemesPopUpOpen = true;
      this.tempCustomThemeSelectionList = this.selectedCustomThemes;
    } else {
      this.customThemes = false;
      this.updateOptions(false, 'customThemes');
      this.selectedCustomThemes = [];
      this.tempCustomThemeSelectionList = [];
      this.setSelectedTheme();
    }
  }

  customThemeEdit() {
    this.customthemesPopUpOpen = true;
    this.tempCustomThemeSelectionList = this.selectedCustomThemes;
  }

  closeCustomThemesPopup() {
    this.customthemesPopUpOpen = false;
    this.tempCustomThemeSelectionList = [];
    this.customThemes = this.selectedCustomThemes.length > 0 ? true : false;
  }

  closeCorrelationPopup() {
    this.correlationPopup = false;
    this.focusMetricDefaultRecomOption = 1;
    this.focusMetricUserDefinedRecomOption = 1;
  }

  dtWorksLoopPopup(bool) {
    if (bool == true) {
      this.closeLoopPopUp = false;
      this.closeLoop = true;
      this.integrations = true;
      this.noOfIntegrations = 1;
      this.emitPristineToClientDetails.emit(this.pristine);
    } else {
      this.closeLoopPopUp = false;
      this.closeLoop = false;
      this.integrations = false;
      this.noOfIntegrations = 0;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
  }

  kioskQrRefreshCheck(bool, type) {
    if (this.kiosk) {
      this.updateOptions(bool, type);
    } else {
      this.refreshKioskQr = false;
      this.kioskQrRefreshPopUp = true;
    }
  }

  kioskQrRefreshPopUpAction(bool) {
    if (bool == true) {
      this.kioskQrRefreshPopUp = false;
      this.refreshKioskQr = true;
      this.kiosk = true;
      this.updateOptions(!this.kiosk, 'kiosk');
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
    } else {
      this.kioskQrRefreshPopUp = false;
      this.refreshKioskQr = false;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
  }

  updateTheme(theme) {
    let filterList = this.tempCustomThemeSelectionList.filter(item => item.id == theme.id);
    if (filterList && filterList.length > 0) {
      this.tempCustomThemeSelectionList = this.tempCustomThemeSelectionList.filter(item => item.id != theme.id);
    } else {
      this.tempCustomThemeSelectionList.push(theme);
    }
  }

  checkSelectionList(theme) {
    let filterList = this.tempCustomThemeSelectionList.filter(item => item.id == theme.id);
    if (filterList && filterList.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  setSelectedTheme() {
    this.customthemesPopUpOpen = false;
    this.selectedCustomThemes = this.tempCustomThemeSelectionList;
    this.customThemes = this.selectedCustomThemes.length > 0 ? true : false;
    let themeList = [];
    this.customthemesList.forEach(item => {
      let isThemeSelected = this.selectedCustomThemes.find(theme => theme.id == item.id);
      themeList.push({id: item.id, mode: isThemeSelected ? 1 : 0});
    });
    let apiData = {themes: themeList};
    this._accessSettings.updateThemesByClientId(this.clientUUID, apiData).subscribe(data => {
      if (data?.success) {
        this.setToastersAfterThemeApiCall.success = true;
        this.setToastersAfterThemeApiCall.error = true;
        this.updateOptions(true, 'customthemes');
        setTimeout(() => {
          this.setToastersAfterThemeApiCall.success = false;
        }, 2000);
      } else {
        this.setToastersAfterThemeApiCall.error = true;
        this.setToastersAfterThemeApiCall.success = false;
        setTimeout(() => {
          this.setToastersAfterThemeApiCall.error = false;
        }, 2000);
      }
    });
  }

  goToPreview(theme) {
    this._route.params.subscribe(params => {
      let themeObj;
      if (params && params.id) {
        themeObj = {themeName: theme.themeName, id: theme.id, client_id: params.id};
      } else {
        themeObj = {themeName: theme.themeName, id: theme.id};
      }
      this._router.navigate(['/theme-preview'], {queryParams: themeObj});
    });
  }

  public updateOptions(bool, type?){
    this.pristine = false
    this.emitPristineToClientDetails.emit(this.pristine)
    bool = !bool
    console.log("SMS = " +this.sms)
    console.log("EMAIL = " +this.email)
    if(type == 'email'){
      this.email = bool
    }
    if(type == 'sms'){
      this.sms = bool
    }
    if(type == "advancedScheduling" && bool == true && !this.email && !this.sms){      
      this.noticePopup = true
      return
    }
    if(type == "email" && bool == true){
      this.noOfEmailInvites = -1
      this.checkEmailandSMSLimits()
    }
    if(type == "sms" && bool == true){
      this.noOfSMSInvites = -1
      this.checkEmailandSMSLimits()
    }
    if(type == "email" && bool == false && !this.advancedScheduling){
      this.checkEmailandSMSLimits()
    }
    if(type == "sms" && bool == false && !this.advancedScheduling){
      this.checkEmailandSMSLimits()
    }    
    if(type == "email" && bool == false && this.advancedScheduling  && !this.sms){
      this.noticePopup = true
      return
    }
    if(type == "sms" && bool == false && this.advancedScheduling && !this.email ){
      this.noticePopup = true
      return
    }
    if (type == 'email' && bool == false) {
      this.uniqueLinkEmail = false;
    }
    if (type == 'sms' && bool == false) {
      this.uniqueLinkSms = false;
    }
    if (!this.textAnalytics && type == 'textAnalytics') {
      console.log(type);
      document.getElementById('textAnalytics-popup').style.display = 'block';
      this.getTextAnalyticsData();
      this.textAnalyticsPopup = true;
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
      if (this.requestApprovalStatus.includes('approved' || 'disapproved')) {
        this.textAnalyticsEditPlan = true;
      }
    }
    if (this.kiosk && type == 'kiosk') {
      this.kioskPopup = true;
      if (this.noOfKiosks < 1) {
        this.noOfKiosks = 1;
      } else {
        this.noOfKiosks = this.kiosksCount;
      }
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    if (type == 'refreshKioskQr') {
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    if (bool == false && type == 'kiosk') {
      this.refreshKioskQr = false;
    }
    if (!this.kiosk && type == 'kiosk') {
      this.kioskPopup = false;
      this.noOfKiosks = this.kiosksCount;
      this.apiValidationMessage = false;
      if (this.apiKey == false) {
        this.pristine = true;
      } else {
        this.apiValidationMessage = false;
        this.pristine = false;
      }
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    if (type === 'integrations' && bool === false) {
      if (this.businessSettings) {
        this.noOfIntegrations = this.businessSettings.noOfIntegrations;
        this.emitPristineToClientDetails.emit(this.pristine);
      }
    }
    if (type === 'integrations' && bool === true) {
      this.integrations = true;
      this.noOfIntegrations = 1;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    if (!this.dynamicLinks && type == 'dynamicLinks') {
      this.qrCode = true;
    }
    if (this.qrCode && type == 'qrCode') {
      this.dynamicLinks = false;
    }
    if (this.textAnalytics && type == 'textAnalytics') {
      this.textAnalyticsPopup = false;
    }
    if (!this.mfa && type == 'mfa') {
      this.multiFactorAuthenticationPopup = true;
      this.selectedOption = '';
    }
    if (this.mfa && type == 'mfa') {
      this.optionForAll = false;
      this.mandatoryForAll = false;
    }

    if (bool == false && type == 'apiKey') {
      this.apiKey = bool;
      if (this.kiosk == true) {
        this.kioskPopup = true;
        this.apiValidationMessage = true;
        this.pristine = true;
        this.checkSave();
      } else {
        this.apiValidationMessage = false;
        this.checkSave();
      }
    }
    if (bool == true && type == 'apiKey') {
      this.apiKey = bool;
      this.kioskPopup = false;
      this.apiValidationMessage = false;
      this.getApiKey();
      this.showPopup();
      this.checkSave();
    } else {
      // this.noOfKiosks = 1
      this.enableApiKey = false;
      this.checkSave();
    }
    if (!this.advancedTemplates && type == 'advancedTemplates') {
      this.advancedTemplatesPopup = true;
      this._accessSettings.getAdvancedTemplates(this.clientUUID, 'dropthought', 'templateName', -1, false, this.timezone, 'en', '1', '', '', '').subscribe(data => {
        console.log(data);
        this.advancedTemplateValues = data.result;
        for (let i = 0; i < this.advancedTemplateValues.length; i++) {
          if (this.advancedTemplateValues[i].isEnabled) {
            this.selectedTemplate[i] = this.advancedTemplateValues[i].templateUUID;
            this.unselectedTemplate[i] = '';
          } else {
            this.selectedTemplate[i] = '';
            this.unselectedTemplate[i] = this.advancedTemplateValues[i].templateUUID;
          }
        }
      });
    }
    if (this.advancedTemplates && type == 'advancedTemplates') {
      this.advancedTemplatesPopup = false;
    }
    if (this.focusMetric == true) {
      if (this.focusMetricDefaultRecom == false) {
        this.focusMetricDefaultRecomOption = '0';
        this.emitPristineToClientDetails.emit(this.pristine);
      }
      if (this.focusMetricUserDefinedRecom == false) {
        this.focusMetricUserDefinedRecomOption = '0';
        this.emitPristineToClientDetails.emit(this.pristine);
      }
      if (this.focusMetricDefaultRecom && type == 'focusMetricDefaultRecom') {
        this.focusMetricDefaultRecomOption = '1';
        this.emitPristineToClientDetails.emit(this.pristine);
      }
      if (this.focusMetricUserDefinedRecom && type == 'focusMetricUserDefinedRecom') {
        this.focusMetricUserDefinedRecomOption = '1';
        this.emitPristineToClientDetails.emit(this.pristine);
      }
    } else {
      this.focusMetricDefaultRecom = false;
      this.focusMetricDefaultRecomOption = '0';
      this.focusMetricUserDefinedRecom = false;
      this.focusMetricUserDefinedRecomOption = '0';
    }
    if (type == 'auditProgram') {
      this.allowCollaboration = false;
      this.checkSave();
    }
    if (type == 'whatsapp' && bool == false) {
      if (this.businessSettings) {
        this.noOfWhatsapp = this.businessSettings.noOfWhatsapp;
      }
      this.pristine = false;
      this.uniqueLinkWhatsapp = false;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    if (type == 'whatsapp' && bool == true) {
      this.whatsapp = true;
      // this.noOfWhatsapp = 0 // Commenting to enable unlimited when whatsapp is enabled
      this.pristine = true;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    if (type == 'correlationValue' && bool == false) {
      this.focusMetricDefaultRecomOption = '1';
      this.focusMetricUserDefinedRecomOption = '1';
    }
    if (type == 'customthemes' && bool === true) {
      this.pristine = true;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    // else{
    //   this.pristine = false
    //   this.emitPristineToClientDetails.emit(this.pristine)
    // }
    if (type == 'gamePlan' && bool === true && !this.advancedTemplates) {
      this.advancedTemplates = true;
    }
    if (type == 'programThrottling') {
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    if (type == 'indent') {
      this.planConfigIndent = bool;
      this.intentAnalysis = bool;
    }
    if (type == 'categorySentiment') {
      this.planConfigSentiment = bool;
      this.categoryAndSentimentAnalysis = bool;
    }
    if (type == 'notes') {
      this.notes = bool;
      if (this.businessSettings) {
        if (this.businessSettings.notes == '0' && bool == false || this.businessSettings.notes == '1' && bool == true) {
          this.pristine = true;
          this.emitPristineToClientDetails.emit(this.pristine);
        }
      }
    }
    if (type == 'taskManager') {
      if (!this.mobileDashboard) {
        // set mobileDashboard to true
        this.mobileDashboard = true;
      }
    }
    if (type == 'mobileApp') {
      if (this.mobileDashboard) {
        // set mobileDashboard to true
        this.taskManager = false;
      }
    }
    if (type == 'closeLoop' && !this.integrations) {
      this.closeLoopPopUp = true;
      this.closeLoop = false;
    }
    if (this.closeLoop && type == 'integrations' && bool == false) {
      this.closeLoop = false;
    }
    if (type == 'uniqueLinkEmail' && !this.email) {
      this.uniqueLinkPopup = true;
      this.uniqueLinkType = 'Email';
    }
    if (type == 'uniqueLinkSms' && !this.sms) {
      this.uniqueLinkPopup = true;
      this.uniqueLinkType = 'SMS';
    }
    if (type == 'uniqueLinkWhatsapp' && !this.whatsapp) {
      this.uniqueLinkPopup = true;
      this.uniqueLinkType = 'Whatsapp';
    }
    if (type == 'mandatorySSO' && bool == true) {
      this.checkSSO = true;
    }
    if (type == 'mandatorySSO' && bool == false) {
      this.mandatorySSO = true;
      this.sso_microsoft = false;
      this.sso_google = false;
      this.sso_apple = false;
      this.checkSSO = false;
    }
    if (type == 'sso_microsoft' && this.mandatorySSO) {
      this.sso_microsoft = true;
      this.sso_google = false;
      this.sso_apple = false;
      this.checkSSO = false;
      this.emitEnableUpdateButton.emit(true);
    }
    if (type == 'sso_google' && this.mandatorySSO) {
      this.sso_google = true;
      this.sso_microsoft = false;
      this.sso_apple = false;
      this.checkSSO = false;
      this.emitEnableUpdateButton.emit(true);
    }
    if (type == 'sso_apple' && this.mandatorySSO) {
      this.sso_apple = true;
      this.sso_microsoft = false;
      this.sso_google = false;
      this.checkSSO = false;
      this.emitEnableUpdateButton.emit(true);
    }
    if(type == 'aiSurveys' && bool == true){
      this.aiSurveysLimit = 3;
    }
    if(type == "aiSurveys" && bool == false){
      this.aiSurveysLimit = -1
    }
    if(type == "addLTI" && bool == true){ //off
      this.addLTI = false
      this.developerKey = ""
      this.developerId = ""
      this.domain = ""
    }
    if(type == "addLTI" && bool == false){ //on
      this.checkSave()
      this.addLTI = true
      this.developerKey = ""
      this.developerId = ""
      this.domain = ""
    }
    if(type == "responseQuota"){
      this.responseQuota = bool ? false : true
    }
    if(type == "customDashboardBuilder"){
      this.customDashboardBuilder = bool ? false : true
    }
  }

  public createObjectandSave() {
    this.showSpinner = true;
    this.updateClicked = true;
    if (this.newClient) {
      this.createBusinessSettings();
    } else {
      if (!this.checkSSO) {
        this.updateBusinessSettings();
      } else {
        this.showSpinner = false;
      }
    }
  }

  public CloseKioskPopup() {
    this.kioskPopup = false;
    if (this.noOfKiosks == 0 && this.kiosk == false) {
      this.apiValidationMessage = true;
      this.pristine = true;
    } else if (this.kiosk == true && this.apiKey == false) {
      this.apiValidationMessage = true;
      this.pristine = true;
    } else {
      this.apiValidationMessage = false;
      this.pristine = false;
    }
  }


//function to create business settings
  public createBusinessSettings() {
    const programFeatures = {
      programMetric: this.programMetric ? '1' : '0',
      programOverview: this.programOverview ? '1' : '0'
    };
    let obj = {
      'intentAnalysis': this.intentAnalysis ? '1' : '0',
      'emotionAnalysis': '0',
      // "preferredMetric": "0",
      // "cohortAnalysis": "0",
      'categoryAnalysis': '0',
      'businessId': localStorage.getItem('businessId'),
      'businessConfigId': '',
      'webHooks': '0',
      // "trigger": "0",
      'createdBy': '164',

      "email": this.email ? '1' : '0',
      "qrcode": this.qrCode ? '1' : '0',
      "shareableLink": this.sharableLink ? '1' : '0',
      "sms": this.sms ? '1' : '0',
      "whatsapp": this.whatsapp ? '1' : '0',
      "textAnalytics": this.textAnalytics ? '1' : '0',
      "logic": this.logicOptions ? '1' : '0',
      // "respondent": '0',
      "respondentTracker" : this.globalRespondentsTracker ? '1' : '0',
      "segment": this.segmentOptions ? '1' : '0',
      "kiosk": this.kiosk ? '1' : '0',
      "advancedSchedule": this.advancedScheduling ? '1' : '0',
      "english": this.webDashboard ? '1' : '0',
      "arabic": this.arabicDashboard ? '1' : '0',
      "multiSurveys": this.multilingualSurveys ? '1' : '0',
      "mobileApp": this.mobileDashboard ? '1' : '0',
      "taskManager": this.taskManager ? '1' : '0',
      "aiSurveys": this.aiSurveys ? '1' : '0',
      "responseQuota": this.responseQuota ? '1' : '0',
      "customDashboardBuilder": this.customDashboardBuilder ? '1' : '0',
      "notification": this.notificationCenter ? '1' : '0',
      "trigger": this.downstreamTrigger ? '1' : '0',
      "metadataQuestion": this.metadataQuestion ? '1' : '0',
      "bitly": this.bitly ? '1' : '0',
      "preferredMetric": this.preferredMetric ? '1' : '0',
      "cohortAnalysis": this.cohortAnalysis ? '1' : '0',
      "advancedTextAnalytics": this.advancedTxtAnalytics ? '1': '0',
      "advancedTemplates": this.advancedTemplates ? '1' : '0',
      "whatfix": this.whatfix ? '1' : '0',
      "apiKey": this.apiKey ? '1' : '0',
      "multipleSublink": this.multipleSublink ? '1' : '0',
      "advancedAnalysis": this.advancedAnalysis ? '1' : '0',
      "exportLinks": this.exportLinks ? '1' : '0',
      "uniqueLinks": '1',
      "uniqueLinkEmail": this.uniqueLinkEmail ? '1' : '0',
      "uniqueLinkSms": this.uniqueLinkSms ? '1' : '0', 
      "uniqueLinkWhatsapp": this.uniqueLinkWhatsapp ? '1' : '0',

      'sso': this.mandatorySSO ? '1' : '0',
      'microsoftSSO': this.sso_microsoft ? '1' : '0',
      'googleSSO': this.sso_google ? '1' : '0',
      'appleSSO': this.sso_apple ? '1' : '0',

      'fromCustomization': this.fromCustomization ? '1' : '0',
      'integrations': this.integrations ? '1' : '0',
      'dynamicLinks': this.dynamicLinks ? '1' : '0',
      'mfa': this.mfa ? this.selectedOption : '0',
      'focusMetric': this.focusMetric ? '1' : '0',
      'predefinedQuestionLibrary': this.predefinedQuestionLibrary ? '1' : '0',
      'defaultRecommendation': this.focusMetricDefaultRecom ? this.focusMetricDefaultRecomOption : '0',
      'userDefinedRecommendation': this.focusMetricUserDefinedRecom ? this.focusMetricUserDefinedRecomOption : '0',
      'advancedReport': this.advancedReport ? '1' : '0',
      'programMetric': this.programMetric ? '1' : '0',
      'programOverview': this.programOverview ? '1' : '0',
      'programFeatures': '1',
      'previouslyUsedQuestionLibrary': this.previouslyUsedQuestionLibrary ? '1' : '0',
      'chatbot': this.chatbot ? '1' : '0',
      'rCoefficient': this.correlationValue ? '1' : '0',
      'customDashboard': this.customDashboard ? '1' : '0',
      'programThrottling': this.programThrottling ? '1' : '0',
      'closeLoop': this.closeLoop ? '1' : '0',
      'gamePlan': this.gamePlan ? '1' : '0',
      'textAnalyticsConfigId': this.textAnalyticsConfigId,
      //DTV-9018
      'auditProgram': this.auditProgram ? '1' : '0',
      'collaboration': this.allowCollaboration ? '1' : '0',
      //DTV-3483//
      'metricCorrelation': this.metricCrossTabulation ? '1' : '0',
      'hippa': this.hippaStatus,

      "noOfUsers": this.noOfUsers,
      "noOfActivePrograms" : this.noOfActivePrograms,
      "noOfTriggers" : this.noOfActiveTriggers,
      "noOfResponses" : this.noOfResponses,
      "noOfMetrics" : this.noOfMetrics,
      "noOfEmailsSent" : this.noOfEmailInvites,
      "noOfSms" : this.noOfSMSInvites,
      "noOfApi" : this.noOfAPIRequests,
      "noOfFilters" : this.noOfFiltersSaved,
      "aiSurveysLimit" : this.aiSurveysLimit,
      "noOfLists" : this.noOfLists,
      "noOfRecipients" : this.noOfRecipients,
      "dataRetention" : this.dataRetention,
      "noOfIntegrations" : this.noOfIntegrations,
      "noOfKiosks" : this.noOfKiosks,
      "noOfWhatsapp" : this.noOfWhatsapp,
      "customThemes" : this.customThemes ? '1' : '0',
      "notes" : this.notes ? '1' : '0',
      "submissionDelay" : this.submissionDelay ? '1' : '0',
      "refreshKioskQr" : this.refreshKioskQr ? '1' : '0',
      "lti": this.addLTI ? '1' : '0',
      "ltiConfig": {
          "developerId": this.developerId,
          "developerKey": this.developerKey,
          "issuer": "https://canvas.instructure.com",
          "domain": this.domain?.replace(/(^\w+:|^)\/\//, ''),
          "apiKey": "",
          "businessUUID": ""
      } 
    }
    this._accessSettings.createBusiness(this.clientUUID, obj).subscribe(data => {
      this.showSpinner = false;
      console.log(data);
      if (this.advancedTemplates) {
        for (let i = 0, j = 0; i < this.selectedTemplate.length, j < this.unselectedTemplate.length; i++, j++) {
          if (this.selectedTemplate[i] != '') {
            let obj = {'id': '', 'mode': 1};
            obj.id = this.selectedTemplate[i];
            this.templatePayload.templates.push(obj);
          }
          if (this.unselectedTemplate[j] != '') {
            let obj1 = {'id': '', 'mode': 0};
            obj1.id = this.unselectedTemplate[i];
            this.templatePayload.templates.push(obj1);
          }
        }
        let payload = this.templatePayload;
        if (payload) {
          this._accessSettings.updateTemplates(this.clientUUID, payload).subscribe(data => {
            console.log(data);
          });
        }
      }
      localStorage.removeItem('accessSettingsTab');
      localStorage.removeItem('hippaStatus');
      this.pristine = true;
      this.emitPristineToClientDetails.emit(this.pristine);
      this.newClient = false;
      this.emitEditClient.emit('true');
      this.contractStartDate = new Date(data.result.contractStartDate);
      this.contractEndDate = new Date(data.result.contractEndDate);
      localStorage.setItem('saveSuccess', this.contractStartDate.toString());
      this._router.navigate(['/client-screen']);
    });
  }

  /* function to update business settings */
  public updateBusinessSettings() {
    const programFeatures = {
      programMetric: this.programMetric ? '1' : '0',
      programOverview: this.programOverview ? '1' : '0'
    };
    if (this.noOfIntegrations == 0 && this.integrations == true) {
      this.pristine = true;
      this.integrations = false;
    } else {
      this.pristine = false;
    }
    let obj = {
      "email": this.email ? '1' : '0',
      "qrcode": this.qrCode ? '1' : '0',
      "shareableLink": this.sharableLink ? '1' : '0',
      "sms": this.sms ? '1' : '0',
      "whatsapp": this.whatsapp ? '1' : '0',
      "textAnalytics": this.textAnalytics ? '1' : '0',
      "logic": this.logicOptions ? '1' : '0',
      "respondent": '0',
      "segment": this.segmentOptions ? '1' : '0',
      "kiosk": this.kiosk ? '1' : '0',
      "advancedSchedule": this.advancedScheduling ? '1' : '0',
      "english":  this.webDashboard ? '1' : '0',
      "arabic": this.arabicDashboard ? '1' : '0',
      "multiSurveys": this.multilingualSurveys ? '1' : '0',
      "mobileApp": this.mobileDashboard ? '1' : '0',
      "taskManager": this.taskManager ? '1' : '0',
      "aiSurveys": this.aiSurveys ? '1' : '0',
      "responseQuota" : this.responseQuota ? '1' : '0',
      "customDashboardBuilder" : this.customDashboardBuilder ? '1' : '0',
      "notification": this.notificationCenter ? '1' : '0',
      "trigger": this.downstreamTrigger ? '1' : '0',
      "metadataQuestion": this.metadataQuestion ? '1' : '0',
      "bitly": this.bitly ? '1' : '0',
      "preferredMetric": this.preferredMetric ? '1' : '0',
      "cohortAnalysis": this.cohortAnalysis ? '1' : '0',
      "categoryAnalysis": this.categoryAnalysis ? '1' : '0',
      "emotionAnalysis": this.emotionAnalysis ? '1' : '0',
      "intentAnalysis": this.intentAnalysis ? '1' : '0',
      "advancedTextAnalytics": this.advancedTxtAnalytics ? '1' : '0',
      "advancedTemplates": this.advancedTemplates ? '1' : '0',
      "whatfix": this.whatfix ? '1' : '0',
      "apiKey": this.apiKey ? '1' : '0',
      "multipleSublink": this.multipleSublink ? '1' : '0',
      "advancedAnalysis": this.advancedAnalysis ? '1' : '0',
      "exportLinks": this.exportLinks ? '1' : '0',
      "uniqueLinks": '1',
      "uniqueLinkEmail": this.uniqueLinkEmail ? '1' : '0', 
      "uniqueLinkSms": this.uniqueLinkSms ? '1' : '0', 
      "uniqueLinkWhatsapp": this.uniqueLinkWhatsapp ? '1' : '0',
      "fromCustomization" : this.fromCustomization ? '1' : '0',
      "integrations" : this.integrations ? '1' : '0',
      "dynamicLinks" : this.dynamicLinks ? '1' : '0',
      "mfa": this.mfa ? this.selectedOption : '0',
      "focusMetric": this.focusMetric ? '1' : '0',
      "predefinedQuestionLibrary": this.predefinedQuestionLibrary ? '1' : '0',
      "defaultRecommendation": this.focusMetricDefaultRecom ? this.focusMetricDefaultRecomOption : '0',
      "userDefinedRecommendation": this.focusMetricUserDefinedRecom ? this.focusMetricUserDefinedRecomOption : '0',
      "advancedReport": this.advancedReport ? '1' : '0',
      "programMetric": this.programMetric ? '1' : '0',
      "programOverview": this.programOverview ? '1' : '0',
      "programFeatures": '1',
      "previouslyUsedQuestionLibrary": this.previouslyUsedQuestionLibrary ? '1' : '0',
      "chatbot": this.chatbot ? '1' : '0',
      "rCoefficient": this.correlationValue ? '1' : '0',
      "programThrottling": this.programThrottling ? '1': '0',
      "closeLoop": this.closeLoop ? '1': '0',
      "gamePlan": this.gamePlan ? '1': '0',
      "textAnalyticsConfigId": this.textAnalyticsConfigId,
      //DTV-9018 
      "auditProgram" : this.auditProgram ? '1' : '0',
      "collaboration" : this.allowCollaboration ? '1' : '0',
      //DTV-3483//
      'metricCorrelation': this.metricCrossTabulation ? '1' : '0',
      'customDashboard': this.customDashboard ? '1' : '0',

      'sso': this.mandatorySSO ? '1' : '0',
      'microsoftSSO': this.sso_microsoft ? '1' : '0',
      'googleSSO': this.sso_google ? '1' : '0',
      'appleSSO': this.sso_apple ? '1' : '0',

      "respondentTracker" : this.globalRespondentsTracker ? '1' : '0',
      "noOfUsers": this.noOfUsers,
      "noOfActivePrograms" : this.noOfActivePrograms,
      "noOfTriggers" : this.noOfActiveTriggers,
      "noOfResponses" : this.noOfResponses,
      "noOfMetrics" : this.noOfMetrics,
      "noOfEmailsSent" : this.noOfEmailInvites,
      "noOfSms" : this.noOfSMSInvites,
      "noOfApi" : this.noOfAPIRequests,
      "noOfFilters" : this.noOfFiltersSaved,
      "aiSurveysLimit" : this.aiSurveysLimit,
      "noOfLists" : this.noOfLists,
      "noOfRecipients" : this.noOfRecipients,
      "dataRetention" : this.dataRetention,
      "noOfIntegrations" : this.noOfIntegrations,
      "noOfKiosks" : this.noOfKiosks,
      "noOfWhatsapp" : this.noOfWhatsapp,
      "customThemes" : this.customThemes ? '1' : '0',
      "notes": this.notes ? '1' : '0',
      "submissionDelay" : this.submissionDelay ? '1' : '0',
      "refreshKioskQr" : this.refreshKioskQr ? '1' : '0',
      "lti": this.addLTI ? '1' : '0',
      "ltiConfig": {
          "developerId": this.developerId,
          "developerKey": this.developerKey,
          "issuer": "https://canvas.instructure.com",
          "domain": this.domain?.replace(/(^\w+:|^)\/\//, ''),
          "apiKey": "",
          "businessUUID": ""
      }
    }
    this._accessSettings.updateBusiness(this.clientUUID, obj).subscribe(data => {
      console.log(data);
      this.showSpinner = false;
      if (data.success == false && data.developerMessages == 'Business configurations not found') {
        this.createBusinessSettings();
      } else {
        this.getBusinessDetails();
        this.emitEnableUpdateButton.emit(true);
        localStorage.removeItem('accessSettingsTab');
        this.pristine = true;
        this.emitPristineToClientDetails.emit(this.pristine);
        this.successfullyUpdated = true;
        setTimeout(() => {
          this.successfullyUpdated = false;
        }, 2000);
      }
      //this.updateTextAnalyticsData()
      if (this.advancedTemplates) {
        for (let i = 0, j = 0; i < this.selectedTemplate.length, j < this.unselectedTemplate.length; i++, j++) {
          if (this.selectedTemplate[i] != '') {
            let obj = {'id': '', 'mode': 1};
            obj.id = this.selectedTemplate[i];
            this.templatePayload.templates.push(obj);
          }
          if (this.unselectedTemplate[j] != '') {
            let obj1 = {'id': '', 'mode': 0};
            obj1.id = this.unselectedTemplate[i];
            this.templatePayload.templates.push(obj1);
          }
        }
        setTimeout(() => {
          let payload = this.templatePayload;
          if (payload) {
            this._accessSettings.updateTemplates(this.clientUUID, payload).subscribe(data => {
              console.log(data);
            });
          }
        }, 100);
      }
      setTimeout(() => {
        if (this.advancedText == '2') {
          this.getModelAppliedToBusiness(this.clientUUID);
        }
      }, 200);
    });
  }

  public cancel() {
    this.pristine = true;
    this.emitPristineToClientDetails.emit(this.pristine);
    this.getBusinessDetails();
  }

  public removeSuccessfullyUpdated() {
    this.successfullyUpdated = false;
  }

  public closeNoticePopup() {
    if (this.pendingElizaAnalyticsData && this.pendingElizaAnalyticsData['categoryMlId']) {
      this.pendingCategorymlIdList = this.pendingElizaAnalyticsData['categoryMlId'];
    }
    if (this.pendingElizaAnalyticsData && this.pendingElizaAnalyticsData['surveyId']) {
      this.pendingSurveyUuidList = this.pendingElizaAnalyticsData['surveyId'];
    }
    console.log(this.pendingCategorymlIdList);
    this.noticePopup = false;
    // this.advancedScheduling = false
    this.selectModelSurveyCategory = false;
    this.selectOneModelToAllSurveys = false;
    this.selectModelsForEachSurvey = false;
    this._cd.detectChanges();
  }

  public closeAdvScheduleNoticePopup(){
    this.noticePopup = false
    this.advancedScheduling = false
    this._cd.detectChanges()
  }

  public closeModeltoAllSurvey(){
    this.selectOneModelToAllSurveys = false
    this._cd.detectChanges()
  }

  public closeModeltoEachSurvey() {
    if (this.activeElizaAnalyticsData && this.activeElizaAnalyticsData['textAnalyticsPlanId'] == '2') {
      if (this.activeElizaAnalyticsData && this.activeElizaAnalyticsData['categoryMlId']) {
        this.pendingCategorymlIdList = this.activeElizaAnalyticsData['categoryMlId'];
      } else {
        this.pendingCategorymlIdList = [];
      }
      if (this.activeElizaAnalyticsData && this.activeElizaAnalyticsData['surveyId']) {
        this.pendingSurveyUuidList = this.activeElizaAnalyticsData['surveyId'];
      } else {
        this.pendingSurveyUuidList = [];
      }
      if (this.activeElizaAnalyticsData && this.activeElizaAnalyticsData['wordCloud']) {
        this.pendingWordCloudList = this.activeElizaAnalyticsData['wordCloud'];
      } else {
        this.pendingWordCloudList = [];
      }
      if (this.activeElizaAnalyticsData && this.activeElizaAnalyticsData['sentimentMlId']) {
        this.pendingSentimentIdList = this.activeElizaAnalyticsData['sentimentMlId'];
      } else {
        this.pendingSentimentIdList = [];
      }
      this.pendingModelFilterByEachSurvey = [];
      this.pendingModelFilterByEachSurveyDisplay = [];
    } else {
      this.pendingModelFilterByEachSurvey = [];
      this.pendingModelFilterByEachSurveyDisplay = [];
      this.pendingSurveyUuidList = [];
      this.pendingWordCloudList = [];
      this.pendingCategorymlIdList = [];
      this.pendingSentimentIdList = [];
    }
    for (let i = 0; i < this.pendingSurveyUuidList.length; i++) {
      let surveyIndex = this.programsList.map(function(o) {
        return o.surveyUUID;
      }).indexOf(this.pendingSurveyUuidList[i]);
      let modelIndex = this.advancedTextAnalyticsModel.map(function(m) {
        return m.mlId;
      }).indexOf(this.pendingCategorymlIdList[i]);
      if (this.advancedTextAnalyticsModel[modelIndex]) {
        this.pendingModelFilterByEachSurvey[i] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
        this.pendingModelFilterByEachSurveyDisplay[surveyIndex] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
      }

    }
    console.log(this.pendingModelFilterByEachSurvey);
    console.log('Category Id list: ' + this.pendingCategorymlIdList);
    console.log('Sentiment Id list: ' + this.pendingSentimentIdList);
    console.log('WordCloud Id list: ' + this.pendingWordCloudList);
    console.log('Survey Id list: ' + this.pendingSurveyUuidList);
    this.selectModelsForEachSurvey = false;
    this._cd.detectChanges();
  }

  public setInfinity(div) {
    if (this[div] != -1) {
      this[div] = -1;
    } else {
      this[div] = 0;
    }
    console.log(this[div]);
    this.updateLimitUsage(div);
  }

  public getLimitUsage() {
    this._accessSettings.getUsageLimit(this.clientUUID).subscribe(data => {
      console.log(data);
      this.limitUsage = data.result;
    });
  }

  public updateLimitUsage(div?) {
    console.log(div);
    if (div == 'noOfEmailInvites' || div == 'noOfSMSInvites' || div == 'noOfResponses') {
      this.checkEmailandSMSLimits();
    }
    if (div == 'noOfWhatsapp') {
      this.checkWhatsappLimits();
    }
    if (this.noOfKiosks == 0) {
      this.noOfKiosks = 1;
    }
    this.checkSave();
    this.pristine = false;
    this.emitPristineToClientDetails.emit(this.pristine);
  }

  public checkDtLiteValue(dtLitePlan) {
    setTimeout(() => {
      if (dtLitePlan) {
        // check input value is greater than 0 and less than 4 and is a number
        if (this.aiSurveysLimit > 0 && this.aiSurveysLimit < 4 && !isNaN(this.aiSurveysLimit)) {
          return true;
        } else {
          this.aiSurveysLimit = 3;
          return false;
        }
      }
    }, 10);
  }

  public checkEmailandSMSLimits(){
    console.log(this.noOfEmailInvites + " < " + this.noOfResponses)
    console.log(this.noOfSMSInvites + " < " + this.noOfResponses)
    if((this.email) && (this.noOfEmailInvites < this.noOfResponses && this.noOfEmailInvites != -1 || this.noOfEmailInvites !== -1 && this.noOfResponses == -1)){
      this.emailResponseLimitWarning = true
    }
    else this.emailResponseLimitWarning = false
    if((this.sms) && (this.noOfSMSInvites < this.noOfResponses && this.noOfSMSInvites != -1 || this.noOfSMSInvites !== -1 && this.noOfResponses == -1)){
      this.smsResponseLimitWarning = true
    }
    else this.smsResponseLimitWarning = false
  }

  public checkWhatsappLimits(){
    if(this.noOfWhatsapp < this.noOfResponses && this.noOfWhatsapp !== -1 || this.noOfWhatsapp !== -1 && this.noOfResponses == -1){
      this.whatsappResponseLimitWarning = true
    } else{
      this.whatsappResponseLimitWarning = false
      this.emitDisableSaveToClientDetails.emit(false)
    }
  }

  public checkSave() {
    if (this.smsResponseLimitWarning || this.emailResponseLimitWarning || this.noOfUsers === undefined || this.noOfUsers === null || this.noOfUsers === 0 || this.noOfActivePrograms === undefined || this.noOfActivePrograms === 0 || this.noOfActivePrograms === null || this.noOfActiveTriggers === undefined || this.noOfActiveTriggers === 0 || this.noOfActiveTriggers === null || this.noOfResponses === undefined || this.noOfResponses === 0 || this.noOfResponses === null || this.noOfMetrics === undefined || this.noOfMetrics === 0 || this.noOfMetrics === null || ((this.email && (this.noOfEmailInvites === undefined || this.noOfEmailInvites === 0 || this.noOfEmailInvites === null)) || (this.sms && (this.noOfSMSInvites === undefined || this.noOfSMSInvites === 0 || this.noOfSMSInvites === null))) || this.noOfAPIRequests === undefined || this.noOfAPIRequests === 0 || this.noOfAPIRequests === null || this.noOfFiltersSaved === undefined || this.noOfFiltersSaved === null || this.noOfFiltersSaved === 0 || (this.aiSurveys && (this.aiSurveysLimit === undefined || this.aiSurveysLimit === null || this.aiSurveysLimit === 0)) || this.noOfLists === undefined || this.noOfLists === 0 || this.noOfLists === null || this.noOfRecipients === undefined || this.noOfRecipients === 0 || this.noOfRecipients === null || this.dataRetention === undefined || this.dataRetention === null || this.dataRetention === 0 || ((this.noOfIntegrations === undefined || this.noOfIntegrations === null || this.noOfIntegrations === 0) && this.integrations === true) || ((this.noOfKiosks === undefined || this.noOfKiosks === null || this.noOfKiosks === 0) && this.kiosk === true) || (this.noOfKiosks > 0 && this.kiosk === true && !this.apiKey) || ((this.noOfWhatsapp === undefined || this.noOfWhatsapp === null || this.noOfWhatsapp === 0) && this.whatsapp === true || this.whatsappResponseLimitWarning) || (this.addLTI && (this.developerId === '' || this.developerKey === '' || this.domain === ''))) { 
      this.emitDisableSaveToClientDetails.emit(true);
    } else {
      this.emitDisableSaveToClientDetails.emit(false);
    }
  }

  public advancedTextAnalytics(value, isChecked, resetFlag?) {
    if (resetFlag && this.pendingTextAnalyticsConfig) {
      this.resetPendingTextAnalyticsConfig();
    }
    this.programsList.sort((a, b) => new Date(b.createdTime).getTime() - new Date(a.createdTime).getTime());
    this.textAnalyticsPlan = value;
    this.advancedText = value;
    if (value == '2') { //value 2 is Advanced Text
      this.selectPlanText = true;
      this.advancedTxtAnalytics = true;
    } else {
      this.selectPlanText = false;
      this.advancedTxtAnalytics = false;
    }
    if (isChecked) {
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
      console.log(this.pristine);
    } else {
      this.pristine = true;
      this.emitPristineToClientDetails.emit(this.pristine);
      console.log(this.pristine);
    }
    this._cd.detectChanges();
  }

  public toSelectDuration(durationText, isChecked) {
    console.log(this.textAnalyticsPlan);
    console.log(this.dataDuration);
    if (durationText === '1') {
      this.checkedProgramList = [];
    } //resetting checked program list if the duration is set due to the historical data requirement that there be at least one response
    if (!isNaN(durationText)) {
      durationText = durationText.toString();
    }
    if (this.durationTextArr.length == 0) {
      if (durationText == '4') {
        this.durationTextArr.push('1');
        this.durationTextArr.push('2');
      } else {
        this.durationTextArr.push(durationText);
      }
      console.log('printing duration text array 1');
      console.log(this.durationTextArr);
    } else {
      if (this.durationTextArr.indexOf(durationText) > -1) {
        let index = this.durationTextArr.indexOf(durationText);
        this.durationTextArr.splice(index, 1);
        console.log('printing duration text array 2');
        console.log(this.durationTextArr);
        if (durationText === '3') {
          document.getElementById('showDurationDropdown').style.display = 'none';
        }
      } else {
        if (this.durationTextArr[0] == '3' && durationText !== '3') {
          console.log('condition 1');
          this.durationTextArr = [];
          this.durationTextArr.push(durationText);
        } else if ((this.durationTextArr[0] == '2' || this.durationTextArr[0] == '1') && durationText == '3') {
          console.log('condition 2');
          this.durationTextArr = [];
          this.durationTextArr.push(durationText);
        } else {
          console.log('condition 3');
          this.durationTextArr.push(durationText);
        }
        console.log('printing duration text array 3');
        console.log(this.durationTextArr);
      }
    }

    if (this.durationTextArr.length > 1) {
      this.durationText = '4';
    } else {
      this.durationText = this.durationTextArr[0];
    }
    this.dataDuration = this.durationText;
    if (this.durationTextArr.length == 1 && this.durationTextArr[0] == '3') {  //duration text 3 is Demo/dry run data
      this.selectDuration = true;
      this.setMinMaxDate();
    } else {
      this.selectDuration = false;
    }
    if (isChecked && this.textAnalyticsPopup) {
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
      console.log(this.pristine);
    } else {
      if ((this.durationTextArr == '1' && isChecked) || (this.durationTextArr == '2' && isChecked)) {
        this.pristine = false;
        this.emitPristineToClientDetails.emit(this.pristine);
        console.log(this.pristine);
      } else {
        this.pristine = true;
        this.emitPristineToClientDetails.emit(this.pristine);
        console.log(this.pristine);
      }
    }
    // this.updateTextAnalyticsData()
  }

  public showCustomDateDropDown() {
    let element = document.getElementById('showDurationDropdown');
    if (element) {
      element.style.display = 'block';
      this.loadScripts();
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
  }

  public hideCustomDateDropDown() {
    let element = document.getElementById('showDurationDropdown');
    if (element) {
      element.style.display = 'none';
    }
    this.hideDateSelector();
    event.stopPropagation();
  }

  public hideDateSelector() {
    let element = document.getElementById('dateSelectorDropdown');
    if (element) {
      element.style.display = 'none';
    }
  }

  onStartInput(sdate) {
    let oldDate = new Date(sdate);
    let newDate = ((oldDate.getMonth() + 1) + '/' + oldDate.getDate() + '/' + oldDate.getFullYear()).toString();
    newDate = newDate.replace(/\//g, '-');
    let sdateArray = newDate.split('-');
    sdate = sdateArray[2] + '-' + sdateArray[0] + '-' + sdateArray[1];
    this.startDate = sdate;
    // this.getTempMonthsRemaining()
    // this.getTempTotalMonths()
    this.setMinMaxDate();
    console.log(this.startDate);
  }

  onEndInput(edate) {
    let oldDate = new Date(edate);
    let newDate = ((oldDate.getMonth() + 1) + '/' + oldDate.getDate() + '/' + oldDate.getFullYear()).toString();
    newDate = newDate.replace(/\//g, '-');
    let sdateArray = newDate.split('-');
    edate = sdateArray[2] + '-' + sdateArray[0] + '-' + sdateArray[1];
    this.endDate = edate;
    this.setMinMaxDate();
    console.log(this.endDate);
  }

  getAPIReadyDate(edate) {
    let oldDate = new Date(edate);
    let newDate = ((oldDate.getMonth() + 1) + '/' + oldDate.getDate() + '/' + oldDate.getFullYear()).toString();
    newDate = newDate.replace(/\//g, '-');
    let sdateArray = newDate.split('-');
    edate = sdateArray[2] + '-' + sdateArray[0] + '-' + sdateArray[1];
    return edate;
  }

  public selectedModel(modelList) {
    this.pendingRadioSelectedItem = [];
    this.pendingRadioSelectedItem = modelList;
  }

  public updateTextAnalyticsData() {
    //alert(this.durationText)
    if (this.durationText == '1') {  //duration text 1 is historical
      this.durationValue = 1;
    }
    if (this.durationText == '2') {   //duration text 2 is upcoming
      this.durationValue = 2;
    }
    if (this.durationText == '4') { // //duration text 4 is both historical & upcoming
      this.durationValue = 4;
    }
    if (this.advancedText == '1') {   // advancedText 1 is Basic Sentiment
      this.planValue = 1;
    }
    if (this.advancedText == '2') {   // advancedText 2 is Advanced text
      this.planValue = 2;
    }
    let obj = {};
    let startD: any = new Date(this.startDate);
    let endD: any = new Date(this.endDate);
    startD = startD.getFullYear() + '-' + ('0' + (startD.getMonth() + 1)).slice(-2) + '-' + ('0' + startD.getDate()).slice(-2);
    endD = endD.getFullYear() + '-' + ('0' + (endD.getMonth() + 1)).slice(-2) + '-' + ('0' + endD.getDate()).slice(-2);
    if (this.durationText == '3') {    //duration text 3 is Demo/dry run data
      this.durationValue = 3;
      this.selectDuration = true;
      obj['dataDurationId'] = this.durationValue,
        obj['textAnalyticsPlanId'] = this.planValue,
        obj['startDate'] = startD,
        obj['endDate'] = endD;
      obj['enabled'] = this.textAnalytics;
    } else {
      obj['dataDurationId'] = this.durationValue,
        obj['textAnalyticsPlanId'] = this.planValue;
      obj['enabled'] = this.textAnalytics;
    }
    this._accessSettings.updateTextAnalyticsData(this.clientUUID, obj).subscribe(data => {
      console.log(data);
      if (data.success) {
        if (this.modelFlag) {
          if (this.planValue == 2) {
            if (this.modelType == 'many') {
              obj['surveyId'] = this.pendingSurveyUuidList;
              obj['categoryMlId'] = this.pendingCategorymlIdList;
            }
            if (this.modelType == 'one') {
              obj['categoryMlId'] = this.pendingRadioSelectedItem.mlId;
            }
          }
          // this.setModelToSurvey(this.clientUUID,obj)
        }
      }
    });
    this.setMinMaxDate();
  }

  public setModelToSurvey(clientUUID, payload) {
    this._accessSettings.setModelToSurvey(clientUUID, payload).subscribe(data => {
      console.log('Inside text analytics success');
      console.log(data);
      /*if(data['status'] == "success"){
        this.modelFlag = false
      }*/
    });
  }

  public getModelAppliedToBusiness(clientUUID) {
    this._accessSettings.getModelAppliedToBusinessReformated(clientUUID).subscribe(data => {
      console.log('Inside get Model applied to business');
      if (data['result']['pendingApproval']) {
        this.pendingElizaAnalyticsData = data['result']['pendingApproval'];
      } else {
        this.pendingElizaAnalyticsData = null;
      }
      if (data['result'][clientUUID]) {
        this.activeElizaAnalyticsData = data['result'][clientUUID];
        // this.activeElizaAnalyticsData['surveyIdIntent'] = this.intentMappingEach
        this.activeTextAnalyticsConfig = JSON.parse(JSON.stringify(this.activeElizaAnalyticsData));
        this.textAnalyticsPlan = this.activeTextAnalyticsConfig.textAnalyticsPlanId;
        this.activeSelectedDataDuration = this.activeTextAnalyticsConfig['dataDurationId'];
      }
      this.getProgramsForTextAnalytics();
      console.log(this.activeElizaAnalyticsData);
      if (this.activeElizaAnalyticsData) {
        let modelIndex;
        let sentimentIndex;
        let wordCloudIndex;
        //console.log("I am here 1" + this.activeElizaAnalyticsData['surveyId'].length)
        if (this.activeElizaAnalyticsData['surveyId'] && this.activeElizaAnalyticsData['surveyId'].length > 0) {
          this.modelType = 'many';
          //console.log("I am here 3")
          this.showEditButton = true;
          this.activeCategorymlIdList = this.activeElizaAnalyticsData['categoryMlId'];
          this.activeSentimentIdList = this.activeElizaAnalyticsData['sentimentMlId'];
          if (this.activeElizaAnalyticsData['wordCloud']) {
            this.activeWordCloudList = this.activeElizaAnalyticsData['wordCloud'];
          }
          this.activeSurveyUuidList = this.activeElizaAnalyticsData['surveyId'];
          for (let i = 0; i < this.activeSurveyUuidList.length; i++) {
            let surveyIndex = this.programsList.map(function(o) {
              return o.surveyUUID;
            }).indexOf(this.activeSurveyUuidList[i]);
            modelIndex = this.advancedTextAnalyticsModel.map(function(m) {
              return m.mlId;
            }).indexOf(this.activeCategorymlIdList[i]);
            sentimentIndex = this.activeSentimentIdList[i];
            wordCloudIndex = this.activeWordCloudList[i];
            if (sentimentIndex == '10002' && wordCloudIndex == true) {
              modelIndex = modelIndex + 1;
            }
            if (sentimentIndex == '10002' && wordCloudIndex == false) {
              modelIndex = modelIndex + 2;
            }
            if (this.advancedTextAnalyticsModel[modelIndex]) {
              this.activeModelFilterByEachSurvey[i] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
              if (this.advancedTextAnalyticsModel[modelIndex]?.isDeprecated == true) {
                this.modalInd[surveyIndex] = surveyIndex;
              } else {
                this.modalInd[surveyIndex] = -1;
              }
            }
            if (this.advancedTextAnalyticsModel[modelIndex]) {
              this.programsList[surveyIndex].tagName = this.advancedTextAnalyticsModel[modelIndex].mlTags;
            }
            console.log('printing the selected model filter for each survey');
            console.log(this.activeModelFilterByEachSurvey);
          }
          this.disableRequestApproval = false;
        } else {
          this.modelType = 'one';
          let mlId = this.activeElizaAnalyticsData['categoryMlId'];
          let sentimentId = this.activeElizaAnalyticsData['sentimentMlId'];
          let wordCloud = this.activeElizaAnalyticsData['wordCloud'];
          modelIndex = this.advancedTextAnalyticsModelByAllSurvey.map(function(o) {
            return o.mlId;
          }).indexOf(mlId);
          if (sentimentId == 10002 && wordCloud == true) {
            modelIndex = modelIndex + 1;
          }
          if (sentimentId == 10002 && wordCloud == false) {
            modelIndex = modelIndex + 2;
          }
          //alert(modelIndex)
          this.activeRadioSelectedItem = this.advancedTextAnalyticsModelByAllSurvey[modelIndex];
          //console.log("I am printing radio selected item within ine case")
          console.log(this.pendingTextAnalyticsConfig);
          this.showEditButton = true;
          this.disableRequestApproval = false;
        }
        if (this.activeElizaAnalyticsData['surveyIdIntent']) {
          if (Object.keys(this.activeElizaAnalyticsData['surveyIdIntent']).length > 0) {
            this.checkedIntentModelProgramList = Object.keys(this.activeElizaAnalyticsData['surveyIdIntent']);
            this.submitIntentModelProgramList = this.activeElizaAnalyticsData['surveyIdIntent'];
            this.showIntentAnalysisSurvey = true;
            this.modelTypeIntent = 'many';
          } else if (this.activeElizaAnalyticsData['surveyIdIntent'].length > 0) {
            this.checkedIntentModelProgramList = this.activeElizaAnalyticsData['surveyIdIntent'];
            this.submitIntentModelProgramList = this.activeElizaAnalyticsData['surveyIdIntent'];
            this.showIntentAnalysisSurvey = false;
            this.modelTypeIntent = 'one';
          } else {
            this.checkedIntentModelProgramList = [];
            this.showIntentAnalysisSurvey = false;
          }
        }
      }
      if (this.pendingElizaAnalyticsData) {
        let modelIndex;
        let sentimentIndex;
        let wordCloudIndex;
        //console.log("I am here 1" + this.pendingElizaAnalyticsData['surveyId'].length)
        if (this.pendingElizaAnalyticsData['surveyId'] && this.pendingElizaAnalyticsData['surveyId'].length > 0) {
          this.modelType = 'many';
          //console.log("I am here 3")
          this.showEditButton = true;
          this.pendingCategorymlIdList = this.pendingElizaAnalyticsData['categoryMlId'];
          this.pendingSentimentIdList = this.pendingElizaAnalyticsData['sentimentMlId'];
          if (this.pendingElizaAnalyticsData['wordCloud']) {
            this.pendingWordCloudList = this.pendingElizaAnalyticsData['wordCloud'];
          }
          this.pendingSurveyUuidList = this.pendingElizaAnalyticsData['surveyId'];
          for (let i = 0; i < this.pendingSurveyUuidList.length; i++) {
            let surveyIndex = this.programsList.map(function(o) {
              return o.surveyUUID;
            }).indexOf(this.pendingSurveyUuidList[i]);
            modelIndex = this.advancedTextAnalyticsModel.map(function(m) {
              return m.mlId;
            }).indexOf(this.pendingCategorymlIdList[i]);
            sentimentIndex = this.pendingSentimentIdList[i];
            wordCloudIndex = this.pendingWordCloudList[i];
            if (sentimentIndex == '10002' && wordCloudIndex == true) {
              modelIndex = modelIndex + 1;
            }
            if (sentimentIndex == '10002' && wordCloudIndex == false) {
              modelIndex = modelIndex + 2;
            }
            if (this.advancedTextAnalyticsModel[modelIndex]) {
              this.pendingModelFilterByEachSurvey[i] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
              this.pendingModelFilterByEachSurveyDisplay[surveyIndex] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
            }
            if (this.advancedTextAnalyticsModel[modelIndex]) {
              this.programsList[surveyIndex].tagName = this.advancedTextAnalyticsModel[modelIndex].mlTags;
            }
            console.log('printing the selected model filter for each survey');
            console.log(this.pendingModelFilterByEachSurvey);
          }
          this.disableRequestApproval = false;
        } else {
          this.modelType = 'one';
          let mlId = this.pendingElizaAnalyticsData['categoryMlId'];
          let sentimentId = this.pendingElizaAnalyticsData['sentimentMlId'];
          let wordCloud = this.pendingElizaAnalyticsData['wordCloud'];
          modelIndex = this.advancedTextAnalyticsModelByAllSurvey.map(function(o) {
            return o.mlId;
          }).indexOf(mlId);
          if (sentimentId == 10002 && wordCloud == true) {
            modelIndex = modelIndex + 1;
          }
          if (sentimentId == 10002 && wordCloud == false) {
            modelIndex = modelIndex + 2;
          }
          this.pendingRadioSelectedItem = this.advancedTextAnalyticsModelByAllSurvey[modelIndex];
          //console.log("I am printing radio selected item within ine case")
          console.log(this.pendingTextAnalyticsConfig);
          this.showEditButton = true;
          this.disableRequestApproval = false;
        }
      }
      if (!this.pendingElizaAnalyticsData && this.activeElizaAnalyticsData) {
        if (this.activeElizaAnalyticsData['surveyId'] && this.activeElizaAnalyticsData['surveyId'].length > 0) {
          this.modelType = 'many';
          this.pendingCategorymlIdList = [...this.activeElizaAnalyticsData['categoryMlId']];
          this.pendingSentimentIdList = [...this.activeElizaAnalyticsData['sentimentMlId']];
          if (this.activeElizaAnalyticsData['wordCloud']) {
            this.pendingWordCloudList = [...this.activeElizaAnalyticsData['wordCloud']];
          }
          this.pendingSurveyUuidList = [...this.activeElizaAnalyticsData['surveyId']];
        } else {
          this.modelType = 'one';
          let mlId = this.activeElizaAnalyticsData['categoryMlId'];
          let sentimentId = this.activeElizaAnalyticsData['sentimentMlId'];
          let wordCloud = this.activeElizaAnalyticsData['wordCloud'];
          let modelIndex = this.advancedTextAnalyticsModelByAllSurvey.map(function(o) {
            return o.mlId;
          }).indexOf(mlId);
          if (sentimentId == 10002 && wordCloud == true) {
            modelIndex = modelIndex + 1;
          }
          if (sentimentId == 10002 && wordCloud == false) {
            modelIndex = modelIndex + 2;
          }
          //alert(modelIndex)
          this.pendingRadioSelectedItem = this.advancedTextAnalyticsModelByAllSurvey[modelIndex];
        }
        if (this.pendingSurveyUuidList) {
          for (let i = 0; i < this.pendingSurveyUuidList.length; i++) {
            let surveyIndex = this.programsList.map(function(o) {
              return o.surveyUUID;
            }).indexOf(this.pendingSurveyUuidList[i]);
            let modelIndex = this.advancedTextAnalyticsModel.map(function(m) {
              return m.mlId;
            }).indexOf(this.pendingCategorymlIdList[i]);
            let sentimentIndex = this.pendingSentimentIdList[i];
            let wordCloudIndex = this.pendingWordCloudList[i];
            if (sentimentIndex == '10002' && wordCloudIndex == true) {
              modelIndex = modelIndex + 1;
            }
            if (sentimentIndex == '10002' && wordCloudIndex == false) {
              modelIndex = modelIndex + 2;
            }
            if (this.advancedTextAnalyticsModel[modelIndex]) {
              this.pendingModelFilterByEachSurvey[i] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
              this.pendingModelFilterByEachSurveyDisplay[surveyIndex] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
            }
          }
        }
        console.log(this.pendingCategorymlIdList);
        console.log(this.advancedTextAnalyticsModel);
      }
    });
  }

  public getTextAnalyticsData() {
    this.getRequestApproval();
    this._accessSettings.getTextAnalyticsData(this.clientUUID).subscribe(data => {
      let analyticsData = data.result[this.clientUUID];
      console.log('I am inside get text analytics data');
      console.log(analyticsData);
      if (analyticsData['dataDurationId']) {
        this.dataDuration = analyticsData['dataDurationId'];
        if (this.dataDuration == '3') {
          this.selectDuration = true;
        }
        if (analyticsData.startDate && analyticsData.endDate) {
          this.startDate = analyticsData.startDate;
          this.endDate = analyticsData.endDate;
          this.setMinMaxDate();
        }
        this.durationTextArr = [];
        if (this.dataDuration) {
          this.toSelectDuration(this.dataDuration, false);
        }
        // setTimeout(() => {
        //   this.getModelAppliedToBusiness(this.clientUUID)
        // }, 1000);
      }
    });

    /*if(!this.dataDuration){
      // this.dataDuration = '1'
      this.durationTextArr = []
      // this.toSelectDuration(this.dataDuration, false)
      this.noAnalyticsResponseFlag = true
    } uncomment this if needed */

    // if(!this.textAnalyticsPlan){
    //   this.textAnalyticsPlan = 2
    // }
    /*if(this.textAnalyticsPlan == '2'){
      this.modelFlag = true
    }*/
  }

  public updateData() {
    // this.updateTextAnalyticsData()
    this.hideCustomDateDropDown();
  }

  public updateCategorySentimentAnalysis(bool, type?) {
    if (this.categorySentimentAnalysis && bool == true) {
      this.selectAnalysis = false;
    } else {
      this.selectAnalysis = true;
    }
  }

  public applyModelSurvey(value) {
    if (value == 'applyModelSurvey') {
      this.selectModelSurveyCategory = true;
    } else {
      this.selectModelSurveyCategory = false;
    }
  }

  public applyOneModelToAllSurveys(value) {
    if (value == 'Apply one model to all surveys') {
      this.selectOneModelToAllSurveys = true;
      if (this.selectModelsForEachSurvey) {
        this.selectModelsForEachSurvey = false;
      }
    } else {
      this.selectOneModelToAllSurveys = false;
    }
  }

  public applyModelsForEachSurvey(value) {
    if (value == 'Apply models for each survey') {
      this.selectModelsForEachSurvey = true;
      // this.activeCategorymlIdList = []
      // this.pendingSurveyUuidList = []
      if (this.selectOneModelToAllSurveys) {
        this.selectOneModelToAllSurveys = false;
      }
    } else {
      this.selectModelsForEachSurvey = false;
    }
  }

  public getAllTextAnalyticsModels() {
    let obj = {
      confidenceThreshold: 1,
      isCustomModel: true,
      labelTypeId: 1,
      mlId: 10001,
      sentimentMlId: 10002,
      mlModelId: 'mt_category',
      mlModelName: 'Hand Tagging',
      mlTags: 'Tagged manually',
      wordCloud: true
    };
    let sentenceSplitter = {
      confidenceThreshold: 1,
      isCustomModel: true,
      labelTypeId: 1,
      mlId: 10001,
      sentimentMlId: 10002,
      mlModelId: 'mt_category',
      mlModelName: 'Sentence Splitter',
      mlTags: 'Sentence splitting only',
      wordCloud: false
    };
    this._accessSettings.getListOfTextAnalyticsModels().subscribe(data => {
      if (data.Category) {
        data.Category.push(obj);
        data.Category.push(sentenceSplitter);
      }
      console.log('Printing models');
      console.log(data.Category);
      this.advancedTextAnalyticsModel = data.Category;
      this.advancedTextAnalyticsModelByAllSurvey = this.advancedTextAnalyticsModel;
      this.advancedTextAnalyticsModel.forEach((element, index) => {
        this.modalInd[index] = undefined;
      });
      this.advancedTextAnalyticsModelDuplicate = this.advancedTextAnalyticsModel;
      this.advancedTextAnalyticsCustomModel = this.advancedTextAnalyticsModel;
      this.advancedTextAnalyticsMonkeyLearnModel = this.advancedTextAnalyticsModel;
      this.advancedTextAnalyticsAllModel = this.advancedTextAnalyticsModel;
      this.getProgramListForABusiness();
    });
  }

  public showModelFilterDropDownEachSurvey(index) {
    let element = document.getElementById('model-dropdown-menu-filter-each-survey-' + index);
    console.log('Printing id ' + 'model-dropdown-menu-filter-each-survey-' + index);
    element.style.display = 'block';
    $(document).mouseup(function(e) {
      var menuDiv: any = document.querySelectorAll('[id^="model-dropdown-menu-filter-each-survey-"]');
      if (menuDiv && menuDiv.length > 0) {
        for (let i of menuDiv) {
          var container = $(i);
          if (!container.is(e.target) && container.has(e.target).length === 0) {
            if (i.style.display === 'block') {
              i.style.display = 'none';
              container.fadeOut();
            }
          }
        }
      }
    });
  }

  public hideModelFilterDropDown() {
    let element = document.getElementById('model-dropdown-menu-filter');
    element.style.display = 'none';
  }

  public showModelFilterDropDownAllSurvey() {
    let element = document.getElementById('model-dropdown-menu-filter');
    if (element) {
      element.style.display = 'block';
    }
  }

  public updateFilterModel(value) {
    //this.hideModelFilterDropDown()
    this.selectedModelFilter = value;
    this.filterCustomAndMonkeyLearnModels(this.selectedModelFilter);
    if (this.selectedModelFilter == 'All') {
      this.allModelsList = [];
      this.isFilterAll = true;
      for (this.modelIndex = 0; this.modelIndex < this.advancedTextAnalyticsAllModel.length; this.modelIndex++) {
        if (this.isFilterAll == true) {
          this.allModelsList.push(this.advancedTextAnalyticsAllModel[this.modelIndex]);
        }
      }
      this.advancedTextAnalyticsModelByAllSurvey = this.filterdOptions;
      this.advancedTextAnalyticsModelByAllSurvey = this.allModelsList;
    } else {
      this.isFilterAll = false;
    }
    setTimeout(() => {
      let element = document.getElementById('model-dropdown-menu-filter');
      element.style.display = 'none';
    }, 100);
  }

  public filterCustomAndMonkeyLearnModels(value) {
    if (this.selectedModelFilter == 'Custom models') {
      this.customModelsList = [];
      this.isCustomModel = true;
      for (this.modelIndex = 0; this.modelIndex < this.advancedTextAnalyticsCustomModel.length; this.modelIndex++) {
        if (this.advancedTextAnalyticsCustomModel[this.modelIndex].isCustomModel == this.isCustomModel) {
          this.customModelsList.push(this.advancedTextAnalyticsCustomModel[this.modelIndex]);
        }
      }
      this.advancedTextAnalyticsModelByAllSurvey = this.filterdOptions;
      this.advancedTextAnalyticsModelByAllSurvey = this.customModelsList;
    }
    if (this.selectedModelFilter == 'MonkeyLearn models') {
      this.monkeyLearnModelsList = [];
      this.isCustomModel = false;
      for (this.modelIndex = 0; this.modelIndex < this.advancedTextAnalyticsMonkeyLearnModel.length; this.modelIndex++) {
        if (this.advancedTextAnalyticsMonkeyLearnModel[this.modelIndex].isCustomModel == this.isCustomModel) {
          this.monkeyLearnModelsList.push(this.advancedTextAnalyticsMonkeyLearnModel[this.modelIndex]);
        }
      }
      this.advancedTextAnalyticsModelByAllSurvey = this.filterdOptions;
      this.advancedTextAnalyticsModelByAllSurvey = this.monkeyLearnModelsList;
    }
  }

  public setMinMaxDate() {
    let sdateArray1 = this.startDate.split('-');
    let sdateArray2 = sdateArray1[2].split(' ');
    this.tempEndMinDate = sdateArray1[1] + '/' + (parseInt(sdateArray2[0]) + 1) + '/' + sdateArray1[0];
    this.tempEndMaxDate = sdateArray1[1] + '/' + sdateArray2[0] + '/' + (parseInt(sdateArray1[0]) + 20);
    this.tempStartDate = sdateArray1[1] + '/' + sdateArray2[0] + '/' + (parseInt(sdateArray1[0]) - 10);
    $('#start-date').datepicker('option', 'minDate', new Date(this.tempStartDate));
    $('#end-date').datepicker('option', 'minDate', new Date(this.tempEndMinDate));
    $('#end-date').datepicker('option', 'maxDate', new Date(this.tempEndMaxDate));
  }

  public getProgramListForABusiness() {
    this.showTextAnalyticsDetails = false;
    this._accessSettings.getProgramListForABusiness(this.clientUUID).subscribe(data => {
      this.programsList = data.result;
      this.defaultProgramsList = JSON.parse(JSON.stringify(this.programsList));
      this._accessSettings.getHistoricalListForABusiness(this.clientUUID).subscribe(data => {
        this.historicalProgramsList = data.result;
        if (this.historicalProgramsList.length > 0) {
          this.filterProgramsListWithHistorical();
        }
      });
      this.defaultAllProgramList = this.programsList;
      console.log(this.programsList);
      // if(this.advancedText == '2'){
      this.getModelAppliedToBusiness(this.clientUUID);
      // }
      this.showTextAnalyticsDetails = true;
      //console.log(this.programsList)
    });
  }

  public filterProgramsListWithHistorical() {
    if (this.programsList.length > 0 && this.historicalProgramsList.length > 0) {
      for (let index = 0; index < this.programsList.length; index++) {
        const element = this.programsList[index];
        let val = this.historicalProgramsList.find(historical => historical.surveyUUID === element.surveyUUID);
        if (val) {
          element.feedbackCount = val.feedbackCount;
        }
      }
    }
  }

  public updateFilterModelByEachSurvey(modelName, surveyIndex, modelIndex) {
    if (modelIndex === 'none') {
      this.programsList[surveyIndex].tagName = '';
      this.programsList[surveyIndex].categoryMlId = '';
      if (this.pendingSurveyUuidList.includes(this.programsList[surveyIndex].surveyUUID)) {
        this.pendingWordCloudList.splice(this.pendingSurveyUuidList.indexOf(this.programsList[surveyIndex].surveyUUID), 1);
        this.pendingSentimentIdList.splice(this.pendingSurveyUuidList.indexOf(this.programsList[surveyIndex].surveyUUID), 1);
        this.pendingCategorymlIdList.splice(this.pendingSurveyUuidList.indexOf(this.programsList[surveyIndex].surveyUUID), 1);
        this.pendingSurveyUuidList.splice(this.pendingSurveyUuidList.indexOf(this.programsList[surveyIndex].surveyUUID), 1);
      }
      this.pendingModelFilterByEachSurvey[surveyIndex] = '';
      this.pendingModelFilterByEachSurveyDisplay[surveyIndex] = '';
    } else {
      //console.log("printing id" + 'listModel-'+surveyIndex+'-'+modelIndex)
      this.programsList[surveyIndex].tagName = '';
      this.programsList[surveyIndex].tagName = this.advancedTextAnalyticsModel[modelIndex].mlTags;
      this.programsList[surveyIndex].categoryMlId = this.advancedTextAnalyticsModel[modelIndex].mlId;
      if (!this.pendingSurveyUuidList.includes(this.programsList[surveyIndex].surveyUUID)) {
        this.pendingSurveyUuidList.push(this.programsList[surveyIndex].surveyUUID);
        if (this.advancedTextAnalyticsModel[modelIndex].mlModelName == 'Hand Tagging' || this.advancedTextAnalyticsModel[modelIndex].mlModelName == 'Sentence Splitter') {
          this.pendingSentimentIdList.push(10002);
          this.pendingCategorymlIdList.push(10001);
        } else {
          this.pendingSentimentIdList.push(1);
          this.pendingCategorymlIdList.push(this.programsList[surveyIndex].categoryMlId);
        }
        if (this.advancedTextAnalyticsModel[modelIndex].mlModelName == 'Sentence Splitter') {
          this.pendingWordCloudList.push(false);
        } else {
          this.pendingWordCloudList.push(true);
        }
      } else {
        this.surveyUuidIndex = this.pendingSurveyUuidList.indexOf(this.programsList[surveyIndex].surveyUUID);
        if (this.advancedTextAnalyticsModel[modelIndex].mlModelName == 'Hand Tagging' || this.advancedTextAnalyticsModel[modelIndex].mlModelName == 'Sentence Splitter') {
          this.pendingSentimentIdList[this.surveyUuidIndex] = 10002;
          this.pendingCategorymlIdList[this.surveyUuidIndex] = 10001;
        } else {
          this.pendingSentimentIdList[this.surveyUuidIndex] = 1;
          this.pendingCategorymlIdList[this.surveyUuidIndex] = this.programsList[surveyIndex].categoryMlId;
        }
        if (this.advancedTextAnalyticsModel[modelIndex].mlModelName == 'Sentence Splitter') {
          this.pendingWordCloudList[this.surveyUuidIndex] = false;
        } else {
          this.pendingWordCloudList[this.surveyUuidIndex] = true;
        }
      }
      let ele = document.getElementById('listModel-' + surveyIndex + '-' + modelIndex).innerHTML;
      if (ele) {
        this.pendingModelFilterByEachSurvey[surveyIndex] = modelName;
        this.pendingModelFilterByEachSurveyDisplay[surveyIndex] = modelName;
      }
      if (this.advancedTextAnalyticsModel[modelIndex].isDeprecated == true) {
        this.modalInd[surveyIndex] = surveyIndex;
      } else {
        this.modalInd[surveyIndex] = -1;
      }
    }
    setTimeout(() => {
      let element = document.getElementById('model-dropdown-menu-filter-each-survey-' + surveyIndex);
      element.style.display = 'none';
    }, 100);
  }

  public filterModelToEachSurvey() {
    if (this.searchModelByEachSurvey != '') {
      this.filterdOptions = this.advancedTextAnalyticsModel;
      this.filterdOptions = this.filterdOptions.filter(
        item => item.mlModelName.toLowerCase().includes(this.searchModelByEachSurvey.toLowerCase())
      );
      this.advancedTextAnalyticsModel = this.filterdOptions;
    } else {
      this.advancedTextAnalyticsModel = this.advancedTextAnalyticsModelDuplicate;
    }
    console.log(this.filterdOptions);
  }

  public filterModelToAllSurvey() {
    if (this.searchModelByAllSurvey != '') {
      this.filterdOptions = this.advancedTextAnalyticsModelByAllSurvey;
      this.filterdOptions = this.filterdOptions.filter(
        item => item.mlModelName.toLowerCase().includes(this.searchModelByAllSurvey.toLowerCase())
      );
      this.advancedTextAnalyticsModelByAllSurvey = this.filterdOptions;
    } else {
      this.filterCustomAndMonkeyLearnModels(this.selectedModelFilter);
      if (this.selectedModelFilter == 'All') {
        this.advancedTextAnalyticsModelByAllSurvey = this.advancedTextAnalyticsModelDuplicate;
      }
    }
    console.log(this.filterdOptions);
  }

  public setModelFlag(value, type) {
    this.modelFlag = value;
    this.modelType = type;
    this.showEditButton = true;
    this.selectOneModelToAllSurveys = false;
    this.selectModelsForEachSurvey = false;
    this.selectModelSurveyCategory = false;
    this.pristine = false;
    this.emitPristineToClientDetails.emit(this.pristine);
    // console.log(JSON.parse(JSON.stringify(this.pendingTextAnalyticsConfig)))
    console.log(this.pristine);
  }

  public setIntentModelFlag(value) {
    if (value) {
      this.modelTypeIntent = 'many';
      let x: any = {};
      this.checkedIntentModelProgramList.forEach(element => {
        let y = element;
        x[y] = 22;
      });
      this.submitIntentModelProgramList = x;
    } else {
      this.modelTypeIntent = 'one';
      this.submitIntentModelProgramList = 22;
    }

    this.showEditIntentButton = true;
    this.selectOneIntentModelToAllSurveys = false;
    this.selectIntentModelsForEachSurvey = false;
    this.selectIntentModelSurveyCategory = false;
    this.pristine = false;
    this.emitPristineToClientDetails.emit(this.pristine);
    // console.log(JSON.parse(JSON.stringify(this.pendingTextAnalyticsConfig)))
    console.log(this.pristine);
  }

  public showSelectedModelByType(modelType) {
    this.selectModelSurveyCategory = true;
    /*if(modelType == "many"){
      this.selectModelsForEachSurvey=true
    }
    if(modelType == "one"){
      this.selectOneModelToAllSurveys=true
    }*/
  }

  public showSelectedIntentModelByType(modelType) {
    this.selectIntentModelSurveyCategory = true;
  }

  public closeAdvancedTemplate() {
    this.advancedTemplatesPopup = false;
    if (this.businessSettings.advancedTemplates !== '1') {
      this.advancedTemplates = false;
    }
  }

  public addSelectedTemplates() {
    this.showChecklistPopup = true;
  }

  public getSelectedTemplates(selectedTemplate, checked, index) {
    if (checked) {
      this.selectedTemplate[index] = selectedTemplate;
      this.unselectedTemplate[index] = '';
    } else {
      this.unselectedTemplate[index] = selectedTemplate;
      this.selectedTemplate[index] = '';
    }
  }

  public closeaddSelectedTemplates() {
    this.showChecklistPopup = false;
    this.advancedTemplatesPopup = false;
  }

  public filterTemplates() {
    let progId = document.getElementById('dropdown-menu-filter');
    if (progId) {
      if (progId.hidden === true) {
        progId.hidden = false;
        this.selectIndustry = 'Select Industry';
      } else {
        progId.hidden = true;
      }

    }
  }

  public searchTemplates(searchTerm) {
    this.searchTerm = searchTerm;
    if (!this.industry) {
      this.industry = '';
    }
    if (!this.theme) {
      this.theme = '';
    }
    setTimeout(() => {
      this._accessSettings.getAdvancedTemplates(this.clientUUID, 'dropthought', 'templateName', -1, false, this.timezone, 'en', '1', this.searchTerm, this.industry, this.theme).subscribe(data => {
        console.log(data);
        this.advancedTemplateValues = data.result;
      });
    }, 500);
  }

  public getIndustry() {
    this._accessSettings.getTemplateIndustry().subscribe(data => {
      if (data) {
        let industries: any = data['result'];
        for (let i = 0; i < industries.length; i++) {
          this.industry_value.push(industries[i].industry_name);
        }
        if (this.industry_value.length == 0) {
          this.showIndustry = true;
        } else {
          this.showIndustry = false;
        }
      } else {
        this.showIndustry = false;
      }
    });
  }

  public getIndustryValue(industry) {
    this.industry = industry;
    this.selectIndustry = industry;
    if (!this.searchTerm) {
      this.searchTerm = '';
    }
    this._accessSettings.getAdvancedTemplates(this.clientUUID, 'dropthought', 'templateName', -1, false, this.timezone, 'en', '1', this.searchTerm, this.industry, '').subscribe(data => {
      console.log(data);
      this.advancedTemplateValues = data.result;
    });
  }

  public showAllTemplates() {
    if (!this.searchTerm) {
      this.searchTerm = '';
    }
    this.selectIndustry = 'All';
    this.industry = '';
    this._accessSettings.getAdvancedTemplates(this.clientUUID, 'dropthought', 'templateName', -1, false, this.timezone, 'en', '1', this.searchTerm, this.industry, '').subscribe(data => {
      console.log(data);
      this.advancedTemplateValues = data.result;
    });
  }

  public getApiKey() {
    this._accessSettings.getApiKeyForClient(this.clientUUID).subscribe(data => {
      console.log(data);
      let result = data['result'];
      this.apiKeyValue = result.apiKey;
      if (this.apiKeyValue == '') {
        this.enableApiKey = false;
      } else {
        this.enableApiKey = true;
      }
    });
  }

  public copyApiKey(copiedLink) {
    console.log(copiedLink);
    copiedLink.select();
    document.execCommand('copy');
  }

  public showPopup() {
    if (this.apiKeyValue == '') {
      this.showApiPopup = true;
      this.apiValidationMessage = false;
      this.kioskPopup = false;
    } else {
      this.showApiPopup = false;
    }
  }

  public closePopup() {
    this.showApiPopup = false;
  }

  public getAdvancedAnalysisURL() {
    this._client.getClientDetails(this.clientUUID, this.timezone).subscribe(data => {
      console.log(data);
      if (data.result[0].advancedAnalysisUrl) {
        this.advancedAnalysisURL = data.result[0].advancedAnalysisUrl;
      } else {
        this.advancedAnalysisURL = '';
      }

      if (data.result[0].businessUUID) {
        this.businessUUID = data.result[0].businessUUID;
      }
    });
  }

  public setAdvancedAnalysisURL() {
    let obj = {
      'advancedAnalysisUrl': this.advancedAnalysisURL
    };
    this._client.updateClient(JSON.stringify(obj), this.clientUUID).subscribe(data => {
      console.log(data);
    });
  }

  public getPlanConfig() {
    let planConfigs = {};
    if (this.textAnalyticsPlan == '1') {
      this.modelType = 'many';
    }
    if (this.modelType == 'one') {
      console.log(this.pendingRadioSelectedItem);
      planConfigs = {
        'categoryMlId': this.textAnalyticsPlan == '1' ? this.getBasicCategory() : parseInt(this.pendingRadioSelectedItem?.mlId),
        'wordCloud': this.textAnalyticsPlan == '1' ? this.getBasicWordCloud() : this.pendingRadioSelectedItem?.mlModelName == 'Sentence Splitter' ? false : true,
        'dataDurationId': parseInt(this.durationText),
        'emotionMlId': '',
        'enabled': true,
        'endDate': this.endDate ? this.getAPIReadyDate(this.endDate) : '',
        'intentMlId': '',
        'lastUpdated': '',
        'sentimentMlId': this.textAnalyticsPlan == '1' ? this.getBasicSentiment() : this.pendingRadioSelectedItem?.mlModelName == 'Hand Tagging' || this.pendingRadioSelectedItem?.mlModelName == 'Sentence Splitter' ? 10002 : 1,
        'startDate': this.startDate ? this.getAPIReadyDate(this.startDate) : '',
        'textAnalyticsPlanId': parseInt(this.textAnalyticsPlan)
      };
    }
    if (this.modelType == 'many') {
      planConfigs = {
        'categoryMlId': this.textAnalyticsPlan == '1' ? this.getBasicCategory() : this.pendingCategorymlIdList,
        'wordCloud': this.textAnalyticsPlan == '1' ? this.getBasicWordCloud() : this.pendingWordCloudList,
        'dataDurationId': parseInt(this.durationText),
        'emotionMlId': '',
        'enabled': true,
        'endDate': this.endDate ? this.getAPIReadyDate(this.endDate) : '',
        'intentMlId': '',
        'lastUpdated': '',
        'sentimentMlId': this.textAnalyticsPlan == '1' ? this.getBasicSentiment() : this.pendingSentimentIdList, //Basic Sentiment categorization
        'startDate': this.startDate ? this.getAPIReadyDate(this.startDate) : '',
        'surveyIdIntent': this.submitIntentModelProgramList,
        'surveyId': this.textAnalyticsPlan == '1' ? this.checkedProgramList : this.pendingSurveyUuidList,
        'textAnalyticsPlanId': parseInt(this.textAnalyticsPlan)
      };
    }
    if (this.planConfigSentiment == false && this.categoryAndSentimentAnalysis == false) {
      delete planConfigs['surveyId']; //surveyIdIntent
    }
    if (this.intentAnalysis == false && this.planConfigIndent == false) {
      delete planConfigs['surveyIdIntent'];
    }
    return planConfigs;
  }

  public getBasicWordCloud() {
    let returnArray = [];
    for (let index = 0; index < this.checkedProgramList.length; index++) {
      returnArray.push(true);
    }
    return returnArray;
  }

  public getBasicSentiment() {
    let returnArray = [];
    for (let index = 0; index < this.checkedProgramList.length; index++) {
      returnArray.push(1);
    }
    return returnArray;
  }

  public getBasicCategory() {
    return 0;
  }

  public sendRequestApproval() {
    if (this.textAnalyticsPlan == '1' && this.programsList.length != 0 && (this.checkedProgramList.length == this.defaultProgramsList.length)) {
      this.checkedProgramList = [];
      this.pendingSentimentIdList = [];
    }
    let configObj = this.getPlanConfig();
    let obj = {
      'to': this.approvalEmailList,
      // "to": ["anirudh@dropthought.com", "deepak@dropthought.com"],
      'businessId': this.clientUUID,
      'fromName': 'Dropthought',
      'fromEmail': 'arone@dropthought.com',
      'textAnalyticsPlanId': parseInt(this.textAnalyticsPlan),
      'subject': 'Request plan change',
      'body': '',
      'host': window.location.hostname,
      'resend': this.resendApproval,
      'createdBy': localStorage.getItem('userUUID'),
      'planConfigs': configObj
    };
    this._client.sendRequestApproval(obj).subscribe(data => {
      console.log(data);
      if (data.success) {
        this.showRequestApprovalPopup = false;
        let result = data.result;
        if (result.length !== 0) {
          this.requestApprovalStatus = result.approvalStatus;
          this.pendingTextAnalyticsConfig = JSON.parse(result.configs);
          this.pendingElizaAnalyticsData = this.pendingTextAnalyticsConfig;
          this.pendingSelectedDataDuration = this.pendingTextAnalyticsConfig['dataDurationId'];
          setTimeout(() => {
            this.selectedTextAnalyticsPlan = this.textAnalyticsPlans[this.pendingTextAnalyticsConfig['textAnalyticsPlanId'] - 1];
          }, 10);
          if (this.pendingTextAnalyticsConfig['surveyId']) {
            this.getProgramsForTextAnalytics();
          }
          console.log(this.pendingTextAnalyticsConfig);
        }
      }
    });
  }

  public getRequestApproval() {
    this._client.getRequestApproval(this.clientUUID).subscribe(data => {
      console.log(data);
      let result = data.result;
      console.log('-----------------------------------------');
      console.log('I am inside get request approval');
      console.log(data.result);
      console.log('-----------------------------------------');
      if (result.length !== 0) {
        this.requestApprovalStatus = result.approvalStatus;
        this.textAnalyticsConfigId = result.configId;
        if (result.approvalStatus == 'pending') {
          this.pendingTextAnalyticsConfig = JSON.parse(result.configs);
          this.pendingElizaAnalyticsData = this.pendingTextAnalyticsConfig;
          // this.activeElizaAnalyticsData = JSON.parse(result.configs)
          this.advancedTextAnalytics(this.textAnalyticsPlan, false);
          this.pendingSelectedDataDuration = this.pendingTextAnalyticsConfig['dataDurationId'];
          if (this.pendingTextAnalyticsConfig['surveyId']) {
            this.basicSentimentSurveyListApproved = this.pendingTextAnalyticsConfig['surveyId'];
            if (this.basicSentimentSurveyListApproved.length > 0) {
              this.showEditSelectBasicSurveys = true;
            }
            this.checkedProgramList = this.pendingTextAnalyticsConfig['surveyId'];
            this.showBasicEditButton = true;
          } else {
            this.showBasicEditButton = false;
          }
          console.log(this.pendingSelectedDataDuration);
          setTimeout(() => {
            this.selectedTextAnalyticsPlan = this.textAnalyticsPlans[this.pendingTextAnalyticsConfig['textAnalyticsPlanId'] - 1];
          }, 10);
          if (this.pendingTextAnalyticsConfig['surveyId']) {
            this.getProgramsForTextAnalytics();
          }
        }
        console.log(this.pendingTextAnalyticsConfig);
      }
    });
  }

  public getLimits() {
    this._client.getTALimits(this.clientUUID).subscribe(data => {
      console.log(data);
      this.limitValue = data.result.limitRemainingPct;
    });
  }

  public getProgramsForTextAnalytics() {
    this.pendingSelectedTextAnalyticsSurveys = [];
    this.activeSelectedTextAnalyticsSurveys = [];
    this.selectedTextAnalyticsSurveyIds = [];
    this.programsList.forEach(element => {
      if (this.pendingTextAnalyticsConfig) {
        if (this.pendingTextAnalyticsConfig['surveyId'] && this.pendingTextAnalyticsConfig['surveyId'].includes(element.surveyUUID)) {
          this.pendingSelectedTextAnalyticsSurveys[this.pendingTextAnalyticsConfig['surveyId'].indexOf(element.surveyUUID)] = element.programName;
          this.selectedTextAnalyticsSurveyIds[this.pendingTextAnalyticsConfig['surveyId'].indexOf(element.surveyUUID)] = element.surveyUUID;
          for (let i = 0; i < this.pendingSurveyUuidList.length; i++) {
            let surveyIndex = this.programsList.map(function(o) {
              return o.surveyUUID;
            }).indexOf(this.pendingSurveyUuidList[i]);
            let modelIndex = this.advancedTextAnalyticsModel.map(function(m) {
              return m.mlId;
            }).indexOf(this.pendingCategorymlIdList[i]);
            let sentimentIndex = this.pendingSentimentIdList[i];
            let wordCloudIndex = this.pendingWordCloudList[i];
            if (sentimentIndex == '10002' && wordCloudIndex == true) {
              modelIndex = modelIndex + 1;
            }
            if (sentimentIndex == '10002' && wordCloudIndex == false) {
              modelIndex = modelIndex + 2;
            }
            if (this.advancedTextAnalyticsModel[modelIndex]) {
              this.pendingModelFilterByEachSurvey[i] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
              this.pendingModelFilterByEachSurveyDisplay[surveyIndex] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
            }

          }
        }
      }
      if (this.activeTextAnalyticsConfig) {
        if (this.activeTextAnalyticsConfig['surveyId'] && this.activeTextAnalyticsConfig['surveyId'].includes(element.surveyUUID)) {
          this.activeSelectedTextAnalyticsSurveys[this.activeTextAnalyticsConfig['surveyId'].indexOf(element.surveyUUID)] = element.programName;
          for (let i = 0; i < this.activeSurveyUuidList.length; i++) {
            let modelIndex = this.advancedTextAnalyticsModel.map(function(m) {
              return m.mlId;
            }).indexOf(this.activeCategorymlIdList[i]);
            let sentimentIndex = this.activeSentimentIdList[i];
            let wordCloudIndex = this.activeWordCloudList[i];
            if (sentimentIndex == '10002' && wordCloudIndex == true) {
              modelIndex = modelIndex + 1;
            }
            if (sentimentIndex == '10002' && wordCloudIndex == false) {
              modelIndex = modelIndex + 2;
            }
            if (this.advancedTextAnalyticsModel[modelIndex]) {
              this.activeModelFilterByEachSurvey[i] = this.advancedTextAnalyticsModel[modelIndex].mlModelName;
            }
          }
        }
      }
    });
    this._cd.detectChanges();
  }

  public editPlan() {
    this.advancedTextAnalytics(this.textAnalyticsPlan, true);
    if (!this.dataDuration) {
      this.toSelectDuration(this.pendingSelectedDataDuration, false);
    }
  }

  public closeSurveyListPopup(noReset?) {
    console.log(this.checkedProgramList);
    if (!noReset) {
      if (this.activeElizaAnalyticsData && this.activeElizaAnalyticsData['textAnalyticsPlanId'] == '1' && this.activeElizaAnalyticsData['surveyId'].length > 0) {
        this.checkedProgramList = this.activeElizaAnalyticsData['surveyId'];
      } else {
        this.checkedProgramList = [];
      }
    }
    this.openSurveyPopup = false;
    this.searchAllSurvey = '';
  }

  public surveyPopupOpen(value) {
    if (value == 1) {
      this.showEditSelectBasicSurveys = true;
      this.openSurveyPopup = true;
      this.searchSurveyName('');
      setTimeout(() => {
        if (this.dataDuration == '1' || this.dataDuration == '4') {
          let elementsChildB = document.getElementsByClassName('hide-parent') as HTMLCollectionOf<HTMLElement>;
          for (let i = 0; i < elementsChildB.length; i++) {
            let style = elementsChildB[i].parentNode['style'];
            style.display = 'none';
          }
        }
      }, 50);
    }
    if (value == 0) {
      this.activeSentimentIdList = [];
      this.checkedProgramList = [];
    }
  }

  public checkUncheckAll() { //Select All function
    if (!this.basicSelectAllFlag) {
      this.basicSelectAllFlag = true;
      this.checkedProgramList = [];
      for (const iterator of this.programsList) {
        if (this.dataDuration == '1' || this.dataDuration == '4') {
          if (iterator.feedbackCount) {
            this.checkedProgramList.push(iterator.surveyUUID);
          }
        } else {
          this.checkedProgramList.push(iterator.surveyUUID);
        }
      }
    } else {
      this.basicSelectAllFlag = false;
      this.checkedProgramList = [];
      this.activeSentimentIdList = [];
    }
  }

  public isAllSelected() { // Individual Program selection function
    this.isSelectAllProgram = this.programsList.every(function(item: any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }

  public getCheckedItemList() {
    this.checkedProgramList = [];
    this.activeSentimentIdList = [];
    for (var i = 0; i < this.programsList.length; i++) {
      if (this.programsList[i].isSelected == true) {
        this.checkedProgramList.push(this.programsList[i].surveyUUID);
        this.activeSentimentIdList.push(1);
      }
    }
  }

  public searchSurveyName(searchText) {
    this.noProgramErrorMsg = false;

    if (searchText != '') {
      this.filteredPrograms = this.defaultProgramsList;
      this.filteredPrograms = this.filteredPrograms.filter(
        survey =>
          survey.programName.toLowerCase().includes(searchText.toLowerCase())
      );
      this.programsList = this.filteredPrograms;

      if (this.defaultAllProgramList.length != 0 && this.filteredPrograms.length == 0) {
        this.noProgramErrorMsg = true;
      }
    } else {
      this.programsList = this.defaultAllProgramList;
    }
  }

  public applySelectedPrograms() {
    this.closeSurveyListPopup(true);
  }

  public toggleSurveySelection(surveyUUID) {
    if (this.checkedProgramList.length > 0) {
      let surveyIndex = this.checkedProgramList.indexOf(surveyUUID);
      if (surveyIndex > -1) {
        this.checkedProgramList.splice(surveyIndex, 1);
        this.activeSentimentIdList.splice(surveyIndex, 1);
      } else {
        this.checkedProgramList.push(surveyUUID);
        this.activeSentimentIdList.push(1);
      }
      if (this.checkedProgramList.length === this.programsList.length) {
        this.basicSelectAllFlag = true;
      } else {
        this.basicSelectAllFlag = false;
      }
    } else {
      this.checkedProgramList.push(surveyUUID);
      this.activeSentimentIdList.push(1);
    }
  }

  public resetPendingTextAnalyticsConfig() {
    this.pendingTextAnalyticsConfig.surveyId = [];
    this.pendingTextAnalyticsConfig.wordCloud = [];
    this.pendingTextAnalyticsConfig.categoryMlId = [];
    this.pendingTextAnalyticsConfig.sentimentMlId = [];
  }

  public multifactorOption(value) {
    this.selectedOption = value;
    // localStorage.setItem("selectedOption",JSON.stringify(this.selectedOption))
    if (this.selectedOption == 2) {
      this.optionForAll = true;
      this.mandatoryForAll = false;
    }
    if (this.selectedOption == 1) {
      this.mandatoryForAll = true;
      this.optionForAll = false;
    }
  }

  public addMultifactorUser() {
    if (this.selectedOption == 2) {
      this.optionForAll = true;
      this.multiFactorAuthenticationPopup = false;
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
      this.mandatoryForAll = false;
    }
    if (this.selectedOption == 1) {
      this.mandatoryForAll = true;
      this.multiFactorAuthenticationPopup = false;
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
      this.optionForAll = false;
    }
    localStorage.setItem('selectedOption', JSON.stringify(this.selectedOption));
  }

  public OpenAuthenticationPopup(selectedValue) {
    this.authenticationPopup = selectedValue;
    this.multiFactorAuthenticationPopup = true;
  }

  public closeMultiFactorPopup() {
    let localSelection = JSON.parse(localStorage.getItem('selectedOption'));
    if (localSelection == '2') {
      this.mfa = true;
      this.optionForAll = true;
      this.mandatoryForAll = false;
    } else if (localSelection == '1') {
      this.mfa = true;
      this.mandatoryForAll = true;
      this.optionForAll = false;
    } else {
      this.mfa = false;
      this.optionForAll = false;
      this.mandatoryForAll = false;
      this.selectedOption = '';
    }
    this.multiFactorAuthenticationPopup = false;
  }

  public focusMetricDefaultRecomChange(value, isChecked) {
    this.focusMetricDefaultRecomOption = value;
    if (this.focusMetricDefaultRecomOption == 2 && isChecked && this.correlationValue == false) {
      this.correlationPopup = true;
    } else {
      this.correlationPopup = false;
    }

    if (isChecked) {
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
    } else {
      this.pristine = true;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    this._cd.detectChanges();
  }

  public focusMetricUserDefinedRecomChange(value, isChecked) {
    this.focusMetricUserDefinedRecomOption = value;
    if (this.focusMetricUserDefinedRecomOption == 2 && isChecked && this.correlationValue == false) {
      this.correlationPopup = true;
    }
    if (isChecked) {
      this.pristine = false;
      this.emitPristineToClientDetails.emit(this.pristine);
    } else {
      this.pristine = true;
      this.emitPristineToClientDetails.emit(this.pristine);
    }
    this._cd.detectChanges();
  }

  public preventInput(event, type) {
    switch (type) {
      case 'noOfFiltersSaved':
        this.noOfFiltersSaved = event.target.value;
        break;
      case 'noOfUsers':
        this.noOfUsers = event.target.value;
        break;
      case 'dataRetention':
        this.dataRetention = event.target.value;
        break;
      case 'noOfIntegrations':
        this.noOfIntegrations = event.target.value;
        break;
      case 'noOfKiosks' :
        this.noOfKiosks = event.target.value;
        break;
      case 'aiSurveysLimit' :
        this.aiSurveysLimit = event.target.value;
    }
  }

  public updateIntentSurveyOptions(type) {
    this.modelTypeIntent = type;
    type == 'many' ? this.showIntentAnalysisSurvey = true : this.showIntentAnalysisSurvey = false;
  }

  public closeIntentPopup() {
    this.selectIntentModelSurveyCategory = false;
    this.showIntentAnalysisSurvey = false;
    this.checkedIntentModelProgramList = [];
  }

  public searchIntentSurveyName(searchText) {
    this.noProgramErrorMsg = false;

    if (searchText != '') {
      this.filteredPrograms = this.defaultProgramsList;
      this.filteredPrograms = this.filteredPrograms.filter(
        survey =>
          survey.programName.toLowerCase().includes(searchText.toLowerCase())
      );
      this.programsList = this.filteredPrograms;

      if (this.defaultAllProgramList.length != 0 && this.filteredPrograms.length == 0) {
        this.noProgramErrorMsg = true;
      }
    } else {
      this.programsList = this.defaultAllProgramList;
    }

  }

  public toggleIntentSurveySelection(surveyUUID) {
    if (this.checkedIntentModelProgramList.length > 0) {
      let surveyIndex = this.checkedIntentModelProgramList.indexOf(surveyUUID);
      if (surveyIndex > -1) {
        this.checkedIntentModelProgramList.splice(surveyIndex, 1);
      } else {
        this.checkedIntentModelProgramList.push(surveyUUID);
      }
    } else {
      this.checkedIntentModelProgramList.push(surveyUUID);
    }
  }

  public toggleUnselectAllIntentSurveySelection() {
    this.checkedIntentModelProgramList = [];
  }

  public closeTextAnalyticsPopup() {
    document.getElementById('textAnalytics-popup').style.display = 'none';
    console.log('this.businessSettings', this.businessSettings)
  }

  public closeUniqueLinkPopup(bool) {
    if (this.uniqueLinkType == 'Email') {
      if (bool) {
        this.email = true
      } else {
        this.email = false
        this.uniqueLinkEmail = false
      }
    }
    if (this.uniqueLinkType == 'SMS') {
      if (bool) {
        this.sms = true
      } else {
        this.sms = false
        this.uniqueLinkSms = false
      }
    }
    if (this.uniqueLinkType == 'Whatsapp') {
      if (bool) {
        this.whatsapp = true
      } else {
        this.whatsapp = false
        this.uniqueLinkWhatsapp = false
      }
    }
    this.uniqueLinkPopup = false
  }

  public updateLTIValues(){
    this.checkSave();
  }


}
