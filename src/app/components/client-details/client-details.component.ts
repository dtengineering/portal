import { Component, OnInit, DoCheck, ChangeDetectorRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ClientServiceService } from '../../services/client-service.service'
import{ AccessSettingsComponent } from '../client-details/access-settings/access-settings.component'
import { AccessSettingsService } from '../../services/access-settings.service'
// import { ImageCropperComponent, CropperSettings } from "ngx-img-cropper";

import { ISubscription } from "rxjs/Subscription";
import { PaginationService } from '../../services/pagination.service';

declare var $ : any

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {

  public clientUUID: string
  public client: any
  public startDate: string
  public endDate: string
  public monthsRemaining: number
  public totalMonths: number
  public tempMonthsRemaining: number
  public tempTotalMonths: number
  public businessDetails: Object
   = {
    "businessName": "",
    "contractStartDate": "",
    "contractEndDate": "",
    "businessUUID": ""
  }
  public updateContractDatePopup: boolean = false
  public clientPortalAccessPopup: boolean = false
  public clientPortalAccess: boolean = false
  public hippaPopup: boolean = false
  public hippa: boolean = false
  public accessSettingsEmail
  public activeStatus: boolean = false
  public customBool: boolean
  public contractBool: boolean
  public contractStartDate: string
  public contractEndDate: string
  public tempContractStartDate: string
  public tempContractEndDate: string
  public successfullyUpdated: boolean
  public reason: string = ""
  public tempReason: string = ""
  public status: string
  public duration: string
  public industry: Array<number> = []
  public industryPlaceholder: any = []
  public contractPeriod: string;
  public csStartDate: string
  public csEndDate: string
  public tempCsStartDate: string
  public tempCsEndDate: string
  public csDisplayStartDate: string
  public csDisplayEndDate: string
  public customDate: boolean = true
  public location: string
  public empRange: string
  public tempStartDate: any
  public tempEndMinDate: any
  public tempEndMaxDate: any


  public editClient: boolean
  public defaultRange: Array<String> = ['1-50', '51-100', '101-500', '501-1000', '1001-5000', '5001-10000', '>10000']
  public defaultIndustry: Array<String> = ['Automotive', 'Airlines', 'Agriculture', 'Advertising/Public Relations', 'Banking and Finance','Construction', 'Education', 'Energy', 'Entertainment', 'Event Management', 'Food & Beverage', 'Government service', 'Human Resource', 'Healthcare', 'Hospitality', 'Insurance', 'Information Technology', 'Lawyers / Law Firms', 'Logistics', 'Media', 'Oil & Gas', 'Pharma', 'Real Estate', 'Retail', 'Sports', 'Telecom', 'Transportation', 'Travel & Tourism', 'Others']


  public empRangePopup: number


  public errorCheck: boolean
  public industryPlaceholderError: boolean
  public savePressed: boolean
  public editClientMessage: boolean
  public editKingSuccess : boolean
  public changeKingSuccess : boolean

  public activeTabs = []
  public currentTab : string

  public companyName: string = ""
  public empRangePlaceholder: string = ""
  public empRangePlaceholderPopup: string = ""
  public editKingName : string = ""
  public editKingMobile : string = ""
  public editKingTitle : string = ""
  public changeKingName : string = ""
  public changeKingMobile : string = ""
  public changeKingTitle : string = ""
  public changeKingEmail : string = ""

  //Error boolean variables
  public editKingNameError : boolean = false
  public editKingTitleError : boolean = false
  public editKingMobileError : boolean = false
  public changeKingNameError : boolean = false
  public changeKingTitleError : boolean = false
  public changeKingMobileError : boolean = false
  public changeKingEmailError : boolean = false
  public invalidEmailError : boolean = false
  public changeKingEmailExistError : boolean = false
  public companyNameError: boolean
  public empRangePlaceholderError: boolean

  public countryPlaceholder: Array<string> = ["India", "Oman", "United Arab Emirates", "United States"]
  public country: number


  public newClient: boolean = false
  public pristine: boolean = true
  public pageChangePopup:boolean = false
  public pageRoute: string
  public accessSettingVersions: any
  private pageSubscription: ISubscription
  public currentPageNumber: number = 1
  public totalPages: number = 1

  public disableUpdateButton: boolean = false
  public saveButtonDisabled: boolean = false

  public phase2implimented: boolean = false // remove this var from entire project when we move to phase 2. This will limit features until that time

  public kingExpired: boolean = false


  public forgotUsernameOrPasswordPopup: boolean = false
  public clientAccountAccessPopup: boolean = false
  public clientAccountAccess: boolean = false
  public clientAccountAccessClicked: boolean = false

  public hippaStatus: boolean = false
  public planName: string

  public programFeatures: any

  // DTV-4724 HIPAA Ghost Account


  //Used to access funtions and variables in Access Settings component
  @ViewChild(AccessSettingsComponent) accessSettingsComponent
  set appShark(directive: AccessSettingsComponent) {
    this.pristine = directive.pristine;
  };





  constructor(private route: ActivatedRoute, private cd: ChangeDetectorRef, private _router: Router, private _pagination: PaginationService, private _client: ClientServiceService, private _accessSettings: AccessSettingsService) {

    this.pageSubscription = this._pagination.paginationClickObservable.subscribe(data => {
      this.currentPageNumber = data
    })
   }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.clientUUID = params["id"]
    })
    this.getClientInfo()
    this.getBusinessDetails()
  }


  ngDoCheck(){
    if(this.updateContractDatePopup || this.clientPortalAccessPopup){
      setTimeout(() => {
        this.loadScripts()
      }, 50);
    }
  }

  ngOnDestroy(){
    localStorage.removeItem("clientDetailsTab")
  }

  public clickNext() {
    if (this.currentPageNumber === this.totalPages) {
      //this.console.consoleLogPrint("last page")
    }
    else {
      this.currentPageNumber = this.currentPageNumber + 1
      this.getAccessSettingVersions()
    }
  }

  public clickPrev() {
    if (this.currentPageNumber === 1 || this.currentPageNumber === 0) {
      //this.console.consoleLogPrint("first page")
    }
    else {
      this.currentPageNumber = this.currentPageNumber - 1
      this.getAccessSettingVersions()
    }
  }


  public getClientInfo(){
    let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    this._client.getClientDetails(this.clientUUID, timezone).subscribe( data => {
      this.client = data.result[0]
      console.log(this.client)
      localStorage.setItem("businessId", this.client['businessId'])
      localStorage.setItem("businessUUID", this.client['businessUUID'])
      this.startDate = new Date(this.client['contractStartDate'].replace(/\s/g, "T")).toDateString().split(' ').slice(1).join(' ')
      this.endDate = new Date(this.client['contractEndDate'].replace(/\s/g, "T")).toDateString().split(' ').slice(1).join(' ')
      this.businessDetails['businessName'] = this.client['businessName']
      this.contractStartDate = this.client['contractStartDate']
      this.contractEndDate = this.client['contractEndDate']
      this.csStartDate = this.client['csStartDate']
      this.csEndDate = this.client['csEndDate']
      this.tempContractStartDate = this.client['contractStartDate']
      this.tempContractEndDate = this.client['contractEndDate']
      this.tempCsStartDate = this.client['csStartDate']
      this.tempCsEndDate = this.client['csEndDate']
      this.csDisplayStartDate = new Date(this.client['csStartDate'].replace(/\s/g, "T")).toDateString().split(' ').slice(1).join(' ')
      this.csDisplayEndDate = new Date(this.client['csEndDate'].replace(/\s/g, "T")).toDateString().split(' ').slice(1).join(' ')
      this.status = this.client['status']
      this.location = this.client['businessTimezone']
      this.companyName = this.client['businessName']
      this.empRangePopup = this.client['employeesRange']
      this.country = this.client['countryId']
      this.clientAccountAccess = this.client['csAccess'] === '1' ? true : false
      this.hippaStatus = this.client['hippa'] === '1' ? true : false
      this.planName = this.client['planName'].toLowerCase().includes('dt lite'.toLowerCase()) ? (this.client['trial_status']== '1') ? 'DT Lite (Free trial)' : 'DT Lite' : 'Enterprise';
      this.getEmpRange()
      if(this.client['csAccess'] == '1') this.clientPortalAccess = true
      if(!this.csEndDate || !this.csStartDate){
        this.customDate = false
        this.csStartDate = this.client['contractStartDate']
        this.csEndDate = this.client['contractEndDate']
        this.csDisplayStartDate = this.startDate
        this.csDisplayEndDate = this.endDate
      }
      this.reason = this.client['reason']
      this.tempReason = this.reason
      if(this.client['industry']) this.industry = JSON.parse(this.client['industry'])
      else this.industry = []
      if(this.industry) this.getIndustry()
      this.editKingName = this.client['kingName']
      this.editKingMobile = this.client['kingMobile']
      this.editKingTitle = this.client['kingTitle']
      this.changeKingName = this.client['kingName']
      this.changeKingMobile = this.client['kingMobile']
      this.changeKingTitle = this.client['kingTitle']
      this.changeKingEmail = this.client['kingEmail']
      // this.contractPeriod = this.client['']
      // this.contractEndDate = data.result['contractEndDate']
      this.setMinMaxDate()
      this.getMonthsRemaining()
      this.getTotalMonths()
      this.getAccessSettingVersions()
      this.cd.detectChanges()
      window.scrollTo(0,0);
    })
  }

  public selectTab(selectedTab) {
    console.log(this.pristine)
    console.log(this.newClient)
    console.log(selectedTab)
    switch (selectedTab) {
    case 'client':
      if(localStorage.getItem("accessSettingsTab") == "1" && this.newClient || localStorage.getItem("accessSettingsTab") == "1" && !this.pristine){
        this.stopPageChange('client')
      }
      else{
        localStorage.removeItem("accessSettingsTab")
        this.activeTabs = ['active', '', '']
        this.currentTab = selectedTab
        localStorage.removeItem("accessSettingsTab")
        localStorage.setItem("clientDetailsTab", 'client')
        this.getAccessSettingVersions()
      }
      break
    case 'king':
      if(localStorage.getItem("accessSettingsTab") == "1" && this.newClient || localStorage.getItem("accessSettingsTab") == "1" && !this.pristine){
        this.stopPageChange('king')
      }
      else{
        localStorage.removeItem("accessSettingsTab")
        this.activeTabs = ['', 'active', '']
        this.currentTab = selectedTab
        localStorage.removeItem("accessSettingsTab")
        localStorage.setItem("clientDetailsTab", 'king')
      }
      break
    case 'access':
      localStorage.setItem("accessSettingsTab", '1')
      localStorage.setItem("clientDetailsTab", 'access')
      this.activeTabs = ['', '', 'active']
      this.currentTab = selectedTab
      break
    }
  }

  public showKingCardOptions(){
    let element = document.getElementById('kingCardOptions')
    if(element.style.display == "none")
      element.style.display = "block"
    else
      element.style.display = "none"
  }

  public editKing(){
    let element = document.getElementById('editKingPopup')
    if(element.style.display == "none")
      element.style.display = "block"
    else
      element.style.display = "none"
    document.getElementById('kingCardOptions').style.display = "none"
  }

  public cancelEditKing(){
    this.editKing();
    this.editKingName = this.client.kingName
    this.editKingTitle = this.client.kingTitle
    this.editKingMobile = this.client.kingMobile
    this.editKingNameError = false
    this.editKingTitleError = false
    this.editKingMobileError = false
  }

  public cancelChangeKing(){
    this.changeKing();
    this.changeKingName = this.client.kingName
    this.changeKingTitle = this.client.kingTitle
    this.changeKingMobile = this.client.kingMobile
    this.changeKingEmail = this.client.kingEmail
    this.changeKingNameError = false
    this.changeKingTitleError = false
    this.changeKingMobileError = false
    this.changeKingEmailError = false
    this.changeKingEmailExistError = false
    this.invalidEmailError = false
  }

  // method to handle change king
  public changeKing(){
    let element = document.getElementById('changeKingPopup')
    if(element.style.display == "none")
      element.style.display = "block"
    else
      element.style.display = "none"
    document.getElementById('kingCardOptions').style.display = "none"
  }

  public showHideResetPasswordPopup(){
    let element = document.getElementById('kingResetPasswordPopup')
    if(element.style.display == "none")
      element.style.display = "block"
    else
      element.style.display = "none"
  }

  public showHideResendActivationPopup(){
    let element = document.getElementById('kingResendActivationPopup')
    if(element.style.display == "none")
      element.style.display = "block"
    else
      element.style.display = "none"
  }

  public editKingPopupValidation(field){
    if(field == "name")
    this.editKingNameError = this.editKingName == "" ? true : false
    if(field == "title")
    this.editKingTitleError = this.editKingTitle == "" ? true : false
    if(field == "mobile")
    this.editKingMobileError = this.editKingMobile == "" ? true : false
  }

  //This function uses to check invalid and empty validation of email
  public emailValidator() {
    this.changeKingEmailError = this.changeKingEmail == "" ? true : false
    var emailRegexp = /^[A-Za-z0-9._%+-]+@(?:[A-Za-z0-9-]+\.)+[A-Za-z]{2,}$/;
    if (emailRegexp.test(this.changeKingEmail) == false) {
      this.invalidEmailError = true
    }
    else{
      this.invalidEmailError = false
    }
  }

  public changeKingPopupValidation(field){
    if(field == "name")
    this.changeKingNameError = this.changeKingName == "" ? true : false
    if(field == "title")
    this.changeKingTitleError = this.changeKingTitle == "" ? true : false
    if(field == "mobile")
    this.changeKingMobileError = this.changeKingMobile == "" ? true : false
  }

  // function to update king
  public updateKing(){
    this.editKingNameError = this.editKingName == "" ? true : false
    this.editKingTitleError = this.editKingTitle == "" ? true : false
    this.editKingMobileError = this.editKingMobile == "" ? true : false
    if(!this.editKingNameError && !this.editKingTitleError && !this.editKingMobileError){
    let editKingDetails =
    {
      "fullName": this.editKingName,
      "userEmail": this.client.kingEmail,
      "phone": this.editKingMobile,
      "title": this.editKingTitle
    }
    this._client.updateKingProfile(this.client.kingUserUUID, editKingDetails).subscribe( data => {
      if(data.success){
        this.client = data.result[0]
        this.changeKingName = this.client['kingName']
        this.changeKingMobile = this.client['kingMobile']
        this.changeKingTitle = this.client['kingTitle']
        this.changeKingEmail = this.client['kingEmail']
        this.editKing()
        this.editKingSuccess = true
        setTimeout(()=> this.editKingSuccess = false,3000)
      }
    })
    }
  }

  public modifyKing(){
    this.changeKingNameError = this.changeKingName == "" ? true : false
    this.changeKingEmailError = this.changeKingEmail == "" ? true : false
    this.changeKingTitleError = this.changeKingTitle == "" ? true : false
    this.changeKingMobileError = this.changeKingMobile == "" ? true : false
    if(!this.changeKingNameError && !this.changeKingEmailError && !this.changeKingTitleError && !this.changeKingMobileError && !this.invalidEmailError){
    let changeKingDetails =
    {
      "fullName": this.changeKingName,
      "userEmail": this.changeKingEmail,
      "phone": this.changeKingMobile,
      "title": this.changeKingTitle ,
      "modifyUserUUID": this.client.kingUserUUID
    }
    this._client.changeKingProfile(changeKingDetails ,this.client.businessUUID).subscribe( data => {

      if(data.success){
        this.client = data.result[0]
        this.changeKing()
        this.changeKingSuccess = true
        setTimeout(()=> this.changeKingSuccess = false,3000)
      }
      else{
        if(data.message == "User email is already available with other business"){
          this.changeKingEmailExistError = true
        }
      }
    })
    }
  }

  public resetKingPassword(){
    let language = localStorage.getItem('language')
    let payload = {
      "token": this.client.kingEmail,
      "language": language,
      "activation": false,
      "login": false
    }
    this._client.resetPasswordOrResendActivation(payload).subscribe(data => {
      if(data.success){
        this.showHideResetPasswordPopup()
      }
    })
  }

  public resendActivationEmail(){
    let language = localStorage.getItem('language')
    let payload = {
      "token": this.client.kingEmail,
      "language": language,
      "activation": false,
      "login": true
    }
    this._client.resetPasswordOrResendActivation(payload).subscribe(data => {
      if(data.success){
        this.showHideResendActivationPopup()
      }
    })
  }

  public showHideAccordion(index){
    let ele = document.getElementById('accessSettingVersionDiv_'+index)
    if(ele.style.display == "none")
    ele.style.display = ""
    else
    ele.style.display = "none"
  }

  public getAccessSettingVersions(){
    let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    this._client.getAccessSettingVersion(this.clientUUID, this.currentPageNumber, timezone).subscribe( data => {
      if(data.success){
        data.result.forEach(element => {
          let focusMetric = JSON.parse(element?.focusMetric)
          element.focusMetric = focusMetric
          let uniqueLinks = JSON.parse(element?.uniqueLinks)
          element.uniqueLinks = uniqueLinks
        });
        console.log(data.result)

        this.accessSettingVersions = data.result.filter(t=>t.default != true)
        if(this.accessSettingVersions){
          this.accessSettingVersions.forEach(element => {
            let programFeatures = JSON.parse(element.programFeatures)
            element.programFeatures = programFeatures
            if(element.kiosk == 0){
              element.noOfKiosks = 0
            }
          });
        }
        if(data.totalPages)
        this.totalPages = data.totalPages
        setTimeout(()=> {
          if(document.getElementById('accessSettingVersionDiv_0'))
          document.getElementById('accessSettingVersionDiv_0').style.display = ""}, 100)
      }
    })
  }

  public getIndustry(){
    this.industryPlaceholder = []
    if(this.industry.includes(1)) this.industryPlaceholder.push('Automotive')
    if(this.industry.includes(2)) this.industryPlaceholder.push('Airlines')
    if(this.industry.includes(3)) this.industryPlaceholder.push('Agriculture')
    if(this.industry.includes(4)) this.industryPlaceholder.push('Advertising/Public Relations')
    if(this.industry.includes(5)) this.industryPlaceholder.push('Banking and Finance')
    if(this.industry.includes(6)) this.industryPlaceholder.push('Construction')
    if(this.industry.includes(7)) this.industryPlaceholder.push('Education')
    if(this.industry.includes(8)) this.industryPlaceholder.push('Energy')
    if(this.industry.includes(9)) this.industryPlaceholder.push('Entertainment')
    if(this.industry.includes(10)) this.industryPlaceholder.push('Event Management')
    if(this.industry.includes(11)) this.industryPlaceholder.push('Food & Beverage')
    if(this.industry.includes(12)) this.industryPlaceholder.push('Government service')
    if(this.industry.includes(13)) this.industryPlaceholder.push('Human Resource')
    if(this.industry.includes(14)) this.industryPlaceholder.push('Healthcare')
    if(this.industry.includes(15)) this.industryPlaceholder.push('Hospitality')
    if(this.industry.includes(16)) this.industryPlaceholder.push('Insurance')
    if(this.industry.includes(17)) this.industryPlaceholder.push('Information Technology')
    if(this.industry.includes(18)) this.industryPlaceholder.push('Lawyers / Law Firms')
    if(this.industry.includes(19)) this.industryPlaceholder.push('Logistics')
    if(this.industry.includes(20)) this.industryPlaceholder.push('Media')
    if(this.industry.includes(21)) this.industryPlaceholder.push('Oil & Gas')
    if(this.industry.includes(22)) this.industryPlaceholder.push('Pharma')
    if(this.industry.includes(23)) this.industryPlaceholder.push('Real Estate')
    if(this.industry.includes(24)) this.industryPlaceholder.push('Retail')
    if(this.industry.includes(25)) this.industryPlaceholder.push('Sports')
    if(this.industry.includes(26)) this.industryPlaceholder.push('Telecom')
    if(this.industry.includes(27)) this.industryPlaceholder.push('Transportation')
    if(this.industry.includes(28)) this.industryPlaceholder.push('Travel & Tourism')
    if(this.industry.includes(29)) this.industryPlaceholder.push('Others')
    this.industryPlaceholder = this.industryPlaceholder.toString().split(',').join(', ')
  }

  public getEmpRange(){
    let id = this.empRangePopup
    if(id == 1){
      this.empRangePlaceholderPopup = "1-50"
      this.empRange = "1-50"
      this.empRangePopup = 1
    }
    if(id == 2){
      this.empRangePlaceholderPopup = "51-100"
      this.empRange = "51-100"
      this.empRangePopup = 2
    }
    if(id == 3){
      this.empRangePlaceholderPopup = "101-500"
      this.empRange = "101-500"
      this.empRangePopup = 3
    }
    if(id == 4){
      this.empRangePopup = 4
      this.empRangePlaceholderPopup = "501-1000"
      this.empRange = "501-1000"
    }
    if(id == 5){
      this.empRangePlaceholderPopup = "1001-5000"
      this.empRange = "1001-5000"
      this.empRangePopup = 5
    }
    if(id == 6){
      this.empRangePlaceholderPopup = "5001-10000"
      this.empRange = "5001-10000"
      this.empRangePopup = 6
    }
    if(id == 7){
      this.empRangePlaceholderPopup = ">10000"
      this.empRange = ">10000"
      this.empRangePopup = 7
    }
    this.cd.detectChanges()
  }

  public getMonthsRemaining(){
    let contEndDate = new Date(this.client.contractEndDate.replace(/\s/g, "T"))
    let months = (contEndDate.getFullYear() - new Date().getFullYear()) * 12;
    months += contEndDate.getMonth() - new Date().getMonth();
    if(contEndDate.getDate() > new Date().getDate()) months++
    this.monthsRemaining = months <= 0 ? 0 : months;
    if(contEndDate.getTime() < new Date().getTime()) 
    this.activeStatus = true
    else 
    this.activeStatus = false
    console.log(this.monthsRemaining)
    this.tempMonthsRemaining = this.monthsRemaining
  }

  public getTotalMonths(){
    let contEndDate = new Date(this.client.contractEndDate.replace(/\s/g, "T"))
    let contStartDate = new Date(this.client.contractStartDate.replace(/\s/g, "T"))
    let months = (contEndDate.getFullYear() - contStartDate.getFullYear()) * 12;
    months += contEndDate.getMonth() - contStartDate.getMonth();
    if(contEndDate.getDate() > contStartDate.getDate()) months++
    this.totalMonths = months <= 0 ? 0 : months;
    this.tempTotalMonths = this.totalMonths
    console.log(this.totalMonths)
  }


  public getTempMonthsRemaining(){
    let contEndDate = new Date(this.contractEndDate.replace(/\s/g, "T"))
    let months = (contEndDate.getFullYear() - new Date().getFullYear()) * 12;
    months += contEndDate.getMonth() - new Date().getMonth();
    if(contEndDate.getDate() > new Date().getDate()) months++
    this.monthsRemaining = months <= 0 ? 0 : months;
    this.tempMonthsRemaining = months <= 0 ? 0 : months;
    console.log(this.tempMonthsRemaining)
    this.cd.detectChanges()
  }

  public getTempTotalMonths(){
    let contEndDate = new Date(this.contractEndDate.replace(/\s/g, "T"))
    let contStartDate = new Date(this.contractStartDate.replace(/\s/g, "T"))
    let months = (contEndDate.getFullYear() - contStartDate.getFullYear()) * 12;
    console.log(months)
    months += contEndDate.getMonth() - contStartDate.getMonth();
    console.log(months)
    if(contEndDate.getDate() > contStartDate.getDate()) months++
    console.log(months)
    this.tempTotalMonths = months <= 0 ? 0 : months;
    console.log(this.tempTotalMonths)
    this.cd.detectChanges()
  }


  public navigateToClientScreen(){
    this._router.navigate(['/client-screen' ])
  }

  public updateBusinessDetails(){
    console.log(this.businessDetails)
    this._client.updateContractDates(this.clientUUID, JSON.stringify(this.businessDetails)).subscribe(data => {
      console.log(data)
      this.getClientInfo()
    })
  }

  public showUpdateContractDatePopup(){
    this.updateContractDatePopup = true
    this.loadScripts()
  }

  public closeUpdateContractDatePopup(){
    this.updateContractDatePopup = false
  }


  public toggleClientPortalAccessPopup(){
    this.clientPortalAccess = !this.clientPortalAccess
    setTimeout(() => {
      if(this.clientPortalAccess){
        this.clientPortalAccessPopup = true
      }
      else{
        this.sendCancel()
      }
    }, 10);

  }

  public closeClientPortalAccessPopup(){
    this.clientPortalAccessPopup = false
  }

  public toggleHippaPopup(){
    this.hippa = !this.hippa
    if(this.hippa){
      this.hippaPopup = true
    }
    else{
      this.closeHippaPopup()
    }
  }

  public closeHippaPopup(){
    this.hippaPopup = false
    this.hippa = false
  }

  public getBusinessDetails(){
    this._accessSettings.getBusiness(this.clientUUID).subscribe(data=>{
      console.log(data.result)
      if(data.result.length < 1){
        this.selectTab('access')
        this.newClient = true
      }
      else {
        this.selectTab(localStorage.getItem("clientDetailsTab") ? localStorage.getItem("clientDetailsTab") : 'client')
        this.accessSettingsEmail = data.result.email
        if(data.result.hippa == 1)
        this.hippa = true
        else
        this.hippa = false
        let currentDate = new Date();
        if(new Date(data.result.contractEndDate) < currentDate){
          this.kingExpired = true
        }
        else{
          this.kingExpired = false
        }
        if(data.result.programFeatures){
          this.programFeatures = JSON.parse(data.result.programFeatures)
        }

      }
    });
  }

  public sendRequestToEnableHippa(){
    let obj=
    {
      "hippa": this.hippa ? '1' : '0',
      "email": this.accessSettingsEmail
    }
    this._accessSettings.updateBusiness(this.clientUUID, obj).subscribe(data=>{
      if(data.success){
        this.hippa = true
        this.hippaPopup = false
      }
    });
  }

  public customCheck(){
    this.customBool = true
    this.contractBool = false
    console.log(this.customBool)
    console.log(this.contractBool)
    this.cd.detectChanges()
  }

  public contractCheck(){
    this.customBool = false
    this.contractBool = true
    console.log(this.customBool)
    console.log(this.contractBool)
    this.cd.detectChanges()
  }

  onStartInput(sdate) {
    let oldDate = new Date(sdate)
    // let newDate = ((oldDate.getMonth() + 1) + "/" +  oldDate.getDate() + "/" +  oldDate.getFullYear()).toString()
    let newDate = (('0' + (oldDate.getMonth() + 1)).slice(-2) + "/" +  ('0' + oldDate.getDate()).slice(-2) + "/" +  oldDate.getFullYear()).toString()
    newDate = newDate.replace(/\//g, "-");
    let sdateArray = newDate.split("-");
    sdate = sdateArray[2] + "-" + sdateArray[0] + "-" + sdateArray[1];
    this.contractStartDate = sdate + " 00:00:00.0"
    this.getTempMonthsRemaining()
    this.getTempTotalMonths()
    this.setMinMaxDate()
    console.log(this.contractStartDate)
  }

  onEndInput(edate) {
    let oldDate = new Date(edate)
    // let newDate = ((oldDate.getMonth() + 1) + "/" +  oldDate.getDate() + "/" +  oldDate.getFullYear()).toString()
    let newDate = (('0' + (oldDate.getMonth() + 1)).slice(-2) + "/" +  ('0' + oldDate.getDate()).slice(-2) + "/" +  oldDate.getFullYear()).toString()
    newDate = newDate.replace(/\//g, "-");
    let sdateArray = newDate.split("-");
    edate = sdateArray[2] + "-" + sdateArray[0] + "-" + sdateArray[1];
    this.contractEndDate = edate + " 00:00:00.0"
    this.getTempMonthsRemaining()
    this.getTempTotalMonths()
    this.setMinMaxDate()
    console.log(this.contractEndDate)
  }

  onCSStartInput(sdate) {
    $("#end-date").datepicker('option', 'minDate', new Date(sdate));
    let oldDate = new Date(sdate)
    let newDate = ((oldDate.getMonth() + 1) + "/" +  oldDate.getDate() + "/" +  oldDate.getFullYear()).toString()
    newDate = newDate.replace(/\//g, "-");
    let sdateArray = sdate.split("-");
    sdate = sdateArray[2] + "-" + sdateArray[0] + "-" + sdateArray[1];
    console.log(sdate);
    this.csStartDate = sdate + " 00:00:00.0"
    console.log(this.csStartDate)
  }

  onCSEndInput(edate) {
    let oldDate = new Date(edate)
    let newDate = ((oldDate.getMonth() + 1) + "/" +  oldDate.getDate() + "/" +  oldDate.getFullYear()).toString()
    newDate = newDate.replace(/\//g, "-");
    let sdateArray = newDate.split("-");
    edate = sdateArray[2] + "-" + sdateArray[0] + "-" + sdateArray[1];
    this.csEndDate = edate + " 00:00:00.0"
    console.log(this.csEndDate)
  }

  loadScripts() {
    let self = this;
    let startMinDate = new Date(this.tempStartDate)
    let endMinDate = new Date(this.tempEndMinDate)
    let endMaxDate = new Date(this.tempEndMaxDate)
    $(document).ready(function () {
      $(".start-date").datepicker({
        minDate: startMinDate,
        dateFormat: 'M dd yy',
        onSelect: function (dateText, inst) {
          var dateAsString = dateText; //the first parameter of this function
          var dateAsObject = $(this).datepicker('getDate'); //the getDate method
          //$("#end-date").datepicker('option', 'minDate', $(this).datepicker('getDate') || new Date());
          self.onStartInput(dateAsString);
        }
      });
      $(".end-date").datepicker({
        // minDate: $(".start-date").datepicker('getDate') || new Date(),
        minDate: endMinDate,
        maxDate: endMaxDate,
        dateFormat: 'M dd yy',
        onSelect: function (dateText, inst) {
          var dateAsString = dateText; //the first parameter of this function
          var dateAsObject = $(this).datepicker('getDate'); //the getDate method
          self.onEndInput(dateAsString);
        }
      });
      $(".cs-start-date").datepicker({
        // minDate: 0,
        dateFormat: 'M dd yy',
        onSelect: function (dateText, inst) {
          var dateAsString = dateText; //the first parameter of this function
          var dateAsObject = $(this).datepicker('getDate'); //the getDate method
          $("#end-date").datepicker('option', 'minDate', $(this).datepicker('getDate') || 0);
          self.onCSStartInput(dateAsString);
        }
      });
      $(".cs-end-date").datepicker({
        // minDate: $(".cs-start-date").datepicker('getDate') || 0,
        dateFormat: 'M dd yy',
        onSelect: function (dateText, inst) {
          var dateAsString = dateText; //the first parameter of this function
          var dateAsObject = $(this).datepicker('getDate'); //the getDate method
          self.onCSEndInput(dateAsString);
        }
      });
    });
    $(document).mouseup(function (e){
      var div = document.getElementById('industryMenu');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $('#industryMenu').hide(200);
          }
        }
    });
    $(document).mouseup(function (e){
      var div = document.getElementById('empRange');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $("#empRange").hide(200);
          }
        }
    });
  }

  public saveContract(){
    this.businessDetails['businessUUID'] = this.clientUUID
    if(this.contractStartDate) this.businessDetails['contractStartDate'] = this.contractStartDate
    if(this.contractEndDate) this.businessDetails['contractEndDate'] = this.contractEndDate
    this.updateBusinessDetails()
    this.updateContractDatePopup = false
    this.successfullyUpdated = true
    setTimeout(()=>
      this.successfullyUpdated = false,2000);

  }

  public removeSuccessfullyUpdated(){
    this.successfullyUpdated = false
  }


  public sendCancel(){
    let obj = {}
    obj["businessUUID"] = this.clientUUID
    obj['csAccessFlag'] = '0'
    this._client.requestAccess(this.clientUUID, obj).subscribe(data=>{
      console.log(data)
      // this.getClientInfo()
    })
  }

  public sendRequest(){
    let obj = {}
    obj["businessUUID"] = this.clientUUID
    obj['csAccessFlag'] = '1'
    obj['fromDate'] = this.customBool ? this.csStartDate : this.contractStartDate
    obj['toDate'] = this.customBool ? this.csEndDate : this.contractEndDate
    obj['host'] = 'localhost'
    obj['reason'] = this.tempReason
    obj['timezone'] = this.location
    this._client.requestAccess(this.clientUUID, obj).subscribe(data=>{
      console.log(data)
      if(this.clientPortalAccessPopup) {
        this.clientPortalAccess = true
      }
      this.clientPortalAccessPopup = false
      this.successfullyUpdated = true
      setTimeout(()=>
      this.successfullyUpdated = false,2000);
      this.getClientInfo()
    })
  }


  public showEditClient(){
    this.editClient = true
    this.country = this.client.countryId
    this.companyName = this.client.businessName
    this.empRangePopup = this.client.employeesRange
    this.getEmpRange()
    this.getIndustry()
    this.loadScripts()
  }


  public hideEditClient(){
    this.editClient = false
    this.companyNameError = false
    this.savePressed = false
    this.industryPlaceholderError = false
    this.empRangePlaceholderError = false
  }


  public selectRange(id){
    setTimeout(() => {
      if(id == 0){
        this.empRangePlaceholderPopup = "1-50"
        this.empRangePopup = 1
      }
      if(id == 1){
        this.empRangePlaceholderPopup = "51-100"
        this.empRangePopup = 2
      }
      if(id == 2){
        this.empRangePlaceholderPopup = "101-500"
        this.empRangePopup = 3
      }
      if(id == 3){
        this.empRangePopup = 4
        this.empRangePlaceholderPopup = "501-1000"
      }
      if(id == 4){
        this.empRangePlaceholderPopup = "1001-5000"
        this.empRangePopup = 5
      }
      if(id == 5){
        this.empRangePlaceholderPopup = "5001-10000"
        this.empRangePopup = 6
      }
      if(id == 6){
        this.empRangePlaceholderPopup = ">10000"
        this.empRangePopup = 7
      }
      document.getElementById("empRange").style.display = "none"
      this.cd.detectChanges()
    }, 5);
  }


  public runSaveChecks(){
    if(this.savePressed){
      console.log("Company name = " + this.companyName)
      console.log("industryPlaceholder = " + this.industryPlaceholder)
      console.log("industry = " + this.industry)
      console.log("empRangePlaceholder = " + this.empRangePlaceholderPopup)
      this.companyNameError = false
      this.industryPlaceholderError = false
      this.empRangePlaceholderError = false
      if(this.companyName.trim().length == 0){
        this.companyNameError = true
      }
      if(this.industry.length == 0){
        this.industryPlaceholderError = true
      }
      if(this.empRangePlaceholderPopup.length == 0){
        this.empRangePlaceholderError = true
      }
      if(this.industryPlaceholderError || this.empRangePlaceholderError || this.companyNameError){
        this.errorCheck = false
      }
      else this.errorCheck = true
      console.log(this.errorCheck)

    }
  }

  public setMinMaxDate(){
    let sdateArray1 = this.contractStartDate.split("-");
    let sdateArray2 = sdateArray1[2].split(" ")
    this.tempEndMinDate = sdateArray1[1] + "/" + (parseInt(sdateArray2[0])+ 1) + "/" + sdateArray1[0];
    this.tempEndMaxDate = sdateArray1[1] + "/" + sdateArray2[0] + "/" + (parseInt(sdateArray1[0]) + 20);
    this.tempStartDate = sdateArray1[1] + "/" + sdateArray2[0] + "/" + (parseInt(sdateArray1[0]) - 5);
    $("#start-date").datepicker('option', 'minDate', new Date(this.tempStartDate));
    $("#end-date").datepicker('option', 'minDate', new Date(this.tempEndMinDate));
    $("#end-date").datepicker('option', 'maxDate', new Date(this.tempEndMaxDate));
  }


  public selectIndustry(industry){
    if(this.industryPlaceholder.length > 0) this.industryPlaceholder = this.industryPlaceholder.split(', ')
    else this.industryPlaceholder = []
    let number
    if(industry == "Automotive") number = 1
    if(industry == "Airlines") number = 2
    if(industry == "Agriculture") number = 3
    if(industry == "Advertising/Public Relations") number =4
    if(industry == "Banking and Finance") number =5
    if(industry == "Construction") number =6
    if(industry == "Education") number =7
    if(industry == "Energy") number =8
    if(industry == "Entertainment") number =9
    if(industry == "Event Management") number =10
    if(industry == "Food & Beverage") number =11
    if(industry == "Government service") number =12
    if(industry == "Human Resource") number =13
    if(industry == "Healthcare") number =14
    if(industry == "Hospitality") number =15
    if(industry == "Insurance") number =16
    if(industry == "Information Technology") number =17
    if(industry == "Lawyers / Law Firms") number =18
    if(industry == "Logistics") number =19
    if(industry == "Media") number =20
    if(industry == "Oil & Gas") number =21
    if(industry == "Pharma") number =22
    if(industry == "Real Estate") number =23
    if(industry == "Retail") number =24
    if(industry == "Sports") number =25
    if(industry == "Telecom") number =26
    if(industry == "Transportation") number =27
    if(industry == "Travel & Tourism") number =28
    if(industry == "Others") number =29

    setTimeout(() => {
      if(this.industryPlaceholder.includes(industry)){
        this.industryPlaceholder.splice(this.industryPlaceholder.indexOf(industry), 1)
      }
      else {
        this.industryPlaceholder.push(industry)
      }
      this.industryPlaceholder = this.industryPlaceholder.join(', ')
      if(this.industry.includes(number)){
        this.industry.splice(this.industry.indexOf(number), 1)
      }
      else this.industry.push(number)
      this.runSaveChecks()
      this.cd.detectChanges()
    }, 10);
  }


  public displaySubDropDownMenu(id){
    let x = document.getElementById(id)
    x.style.display = "block"
    if(id == "industryMenu") x.scrollIntoView(true)
  }


  public saveDetails(){
    this.savePressed = true
    this.runSaveChecks()

    if(this.errorCheck){

      this.businessDetails = {
        "businessUUID":this.client.businessUUID,
        "businessName":this.companyName,
        "noOfEmployees":this.empRangePopup,
        "industry":this.industry,
        "country":this.country,
        }
      this._client.updateClient(JSON.stringify(this.businessDetails), this.client.businessUUID).subscribe(data=>{
        console.log(data)
        this.getClientInfo()
        this.editClientMessage = true
        // this.editClient = false
        this.hideEditClient()
        this.successfullyUpdated = true
        setTimeout(() => {
          this.editClientMessage = false
          this.successfullyUpdated = false
        }, 2000);
      })
    }
  }

  public selectCountry(val){
    this.country = val
    console.log(this.country)
    console.log(this.countryPlaceholder)
  }

  public saveAccessSettings(){
    this.disableUpdateButton = true
    this.accessSettingsComponent.createObjectandSave()
  }

  public enableUpdateButton(){
    this.disableUpdateButton = false
  }


  public disableSaveButton(val){
    this.saveButtonDisabled = val
  }

  public cancel(){
    this.accessSettingsComponent.cancel()
  }

  public setPristine(val){
    this.pristine = val
  }

  public stopPageChange(val){
    this.pageChangePopup = true
    this.pageRoute = val
  }

  public closeLeavePage(){
    this.pageChangePopup = false
    this.pageRoute = ""
  }

  public setEdit(){
    this.newClient = false
    this.cd.detectChanges()
  }

  public leavePage(){
    this.pageChangePopup = false
    localStorage.removeItem("accessSettingsTab")
    if(this.newClient){
      this._client.deleteClient(this.clientUUID).subscribe(data => {
        console.log(data)
        this._router.navigate(['/client-screen' ])
      })
    }
    else{
      if(this.pageRoute == "client-screen"){
        this._router.navigate(['/client-screen' ])
      }
      if(this.pageRoute ==  'client'){
        this.activeTabs = ['active', '', '']
        this.currentTab = this.pageRoute
      }
      if(this.pageRoute == 'king'){
        this.activeTabs = ['', 'active', '']
        this.currentTab = this.pageRoute
      }
    }
  }




  public toggleClientAccountAccess(){
    this.clientAccountAccess = !this.clientAccountAccess
    if(this.clientAccountAccess){
      this.clientAccountAccessPopup = true
    }
    else{
      this.sendRemoveClientAccountAccessEmail()
    }
  }

  public hideClientAccountAccessPopup(){
    this.clientAccountAccess = false
    this.clientAccountAccessPopup = false
  }

  public showUsernamePasswordPopup(){
    this.forgotUsernameOrPasswordPopup = true
  }

  public hideUsernamePasswordPopup(){
    this.forgotUsernameOrPasswordPopup = false
  }

  public sendClientAccountAccessEmail(){
    this.clientAccountAccessClicked = true
    this._client.enableCustomerServiceAccount(this.clientUUID).subscribe(data => {
      console.log(data)
      this.clientAccountAccessPopup = false
      this.clientAccountAccessClicked = false
    })
  }

  public sendRemoveClientAccountAccessEmail(){
    this._client.disableCustomerServiceAccount(this.clientUUID).subscribe(data => {
      console.log(data)
    })
  }

  public sendCustomerServiceAccountPasswordReset(){
    this._client.sendCustomerServiceAccountPasswordReset(this.clientUUID).subscribe(data => {
      console.log(data)
      this.forgotUsernameOrPasswordPopup = false
    })
  }

  public setDefaultDate(date){
    let id = "."+date
    if(date == "start-date")$(id).datepicker('setDate', this.startDate)
    if(date == "end-date")$(id).datepicker('setDate', this.endDate)
  }

}
