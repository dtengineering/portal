import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatetemplatesComponent } from './createtemplates.component';

describe('CreatetemplatesComponent', () => {
  let component: CreatetemplatesComponent;
  let fixture: ComponentFixture<CreatetemplatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatetemplatesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreatetemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
