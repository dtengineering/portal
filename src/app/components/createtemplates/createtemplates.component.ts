import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ClientServiceService } from 'src/app/services/client-service.service';
import { TemplateService } from 'src/app/services/template.service';
import { generalValidator, templateNameExists } from 'src/app/validations/validators';

declare var $ : any;

@Component({
  selector: 'app-createtemplates',
  templateUrl: './createtemplates.component.html',
  styleUrls: ['./createtemplates.component.css']
})
export class CreatetemplatesComponent implements OnInit {

  constructor(private _router: Router, public formBuilder: FormBuilder, private cd: ChangeDetectorRef, private _client: ClientServiceService, private templateService: TemplateService) {
    this.createTemplateSurveyForm = this.formBuilder.group({
      createTemplateName: ["", Validators.compose([Validators.minLength(1), Validators.maxLength(64), generalValidator]), templateNameExists.bind(this)],
      programId: ["", Validators.compose([Validators.required])],
    });
  }

  public selectedSurveyName: string = '';
  public createTemplateSurveyForm: FormGroup;
  public createTemplateName: any = '';
  public language: any = '';
  public selectedTemplateType: any = 'Basic';
  public defaultIndustry: Array<String> = ['Automotive', 'Airlines', 'Agriculture', 'Advertising/Public Relations', 'Banking and Finance','Construction', 'Education', 'Energy', 'Entertainment', 'Event Management', 'Food & Beverage', 'Government service', 'Human Resource', 'Healthcare', 'Hospitality', 'Insurance', 'Information Technology', 'Lawyers / Law Firms', 'Logistics', 'Media', 'Oil & Gas', 'Pharma', 'Real Estate', 'Retail', 'Sports', 'Telecom', 'Transportation', 'Travel & Tourism', 'Others']
  public industryPlaceholderError: boolean = false
  public industry: Array<number> = []
  public industryPlaceholder: any = []
  public industryValue: any = []
  public errorCheck: boolean
  public savePressed: boolean
  componentDestroyed$: Subject<boolean> = new Subject();
  public currentClients: any = []
  public selectedClient: any
  public selectedClientPlaceholder: any = 'Select your account'
  public fileName: any = ''
  public showImageError: boolean = false
  public templateImage: any = ''
  public templateDetails: any = {}
  public timezone: any = ''
  public showProgramError: boolean = false
  public programErrorMsg: any = ''
  public workflowData: any = []
  public selectedWorkflowData: any = []
  public workflowPlaceholder: any = []
  public workflowValue: any = {}
  public workFlowArr: any = []
  public workFlowFolders: any = []
  public workFlowData: any = []


  ngOnInit(): void {
    this.language = localStorage.getItem('language') ? localStorage.getItem('language') : 'en';
    this.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone
    this.getIndustry()
    this.getAllClientData()
    this.getWorkflows()
    this.loadScripts()
    this.templateService.getAllTemplate('dropthought', -1, false, this.timezone, this.language, '')
  }

  loadScripts() {
    $(document).mouseup(function (e){
      var div = document.getElementById('industryMenu');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $('#industryMenu').hide(200);
          }
        }
    });
    $(document).mouseup(function (e){
      var div = document.getElementById('accountMenu');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $('#accountMenu').hide(200);
          }
        }
    });
    $(document).mouseup(function (e){
      var div = document.getElementById('workflow-dropdown');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $('#workflow-dropdown').hide(200);
          }
        }
    });
  }

  public goToTemplates(){
    this._router.navigate(['/templates'])
  }

  public selectTemplateType(value){
    this.selectedTemplateType = value
  }

  public getIndustry(){
    this.industryPlaceholder = []
    if(this.industry.includes(1)) this.industryPlaceholder.push('Automotive')
    if(this.industry.includes(2)) this.industryPlaceholder.push('Airlines')
    if(this.industry.includes(3)) this.industryPlaceholder.push('Agriculture')
    if(this.industry.includes(4)) this.industryPlaceholder.push('Advertising/Public Relations')
    if(this.industry.includes(5)) this.industryPlaceholder.push('Banking and Finance')
    if(this.industry.includes(6)) this.industryPlaceholder.push('Construction')
    if(this.industry.includes(7)) this.industryPlaceholder.push('Education')
    if(this.industry.includes(8)) this.industryPlaceholder.push('Energy')
    if(this.industry.includes(9)) this.industryPlaceholder.push('Entertainment')
    if(this.industry.includes(10)) this.industryPlaceholder.push('Event Management')
    if(this.industry.includes(11)) this.industryPlaceholder.push('Food & Beverage')
    if(this.industry.includes(12)) this.industryPlaceholder.push('Government service')
    if(this.industry.includes(13)) this.industryPlaceholder.push('Human Resource')
    if(this.industry.includes(14)) this.industryPlaceholder.push('Healthcare')
    if(this.industry.includes(15)) this.industryPlaceholder.push('Hospitality')
    if(this.industry.includes(16)) this.industryPlaceholder.push('Insurance')
    if(this.industry.includes(17)) this.industryPlaceholder.push('Information Technology')
    if(this.industry.includes(18)) this.industryPlaceholder.push('Lawyers / Law Firms')
    if(this.industry.includes(19)) this.industryPlaceholder.push('Logistics')
    if(this.industry.includes(20)) this.industryPlaceholder.push('Media')
    if(this.industry.includes(21)) this.industryPlaceholder.push('Oil & Gas')
    if(this.industry.includes(22)) this.industryPlaceholder.push('Pharma')
    if(this.industry.includes(23)) this.industryPlaceholder.push('Real Estate')
    if(this.industry.includes(24)) this.industryPlaceholder.push('Retail')
    if(this.industry.includes(25)) this.industryPlaceholder.push('Sports')
    if(this.industry.includes(26)) this.industryPlaceholder.push('Telecom')
    if(this.industry.includes(27)) this.industryPlaceholder.push('Transportation')
    if(this.industry.includes(28)) this.industryPlaceholder.push('Travel & Tourism')
    if(this.industry.includes(29)) this.industryPlaceholder.push('Others')
    this.industryPlaceholder = this.industryPlaceholder.toString().split(',').join(', ')
  }

  public selectIndustry(industry){
    if(this.industryPlaceholder.length > 0) this.industryPlaceholder = this.industryPlaceholder.split(', ')
    else this.industryPlaceholder = []
    this.industryValue = []
    let number
    if(industry == "Automotive") number = "Automotive"
    if(industry == "Airlines") number = "Airlines"
    if(industry == "Agriculture") number = "Agriculture"
    if(industry == "Advertising/Public Relations") number = "Advertising/Public Relations"
    if(industry == "Banking and Finance") number = "Banking and Finance"
    if(industry == "Construction") number = "Construction"
    if(industry == "Education") number = "Education"
    if(industry == "Energy") number = "Energy"
    if(industry == "Entertainment") number = "Entertainment"
    if(industry == "Event Management") number = "Event Management"
    if(industry == "Food & Beverage") number = "Food & Beverage"
    if(industry == "Government service") number = "Government service"
    if(industry == "Human Resource") number = "Human Resource"
    if(industry == "Healthcare") number = "Healthcare"
    if(industry == "Hospitality") number = "Hospitality"
    if(industry == "Insurance") number = "Insurance"
    if(industry == "Information Technology") number = "Information Technology"
    if(industry == "Lawyers / Law Firms") number = "Lawyers / Law Firms"
    if(industry == "Logistics") number = "Logistics"
    if(industry == "Media") number = "Media"
    if(industry == "Oil & Gas") number = "Oil & Gas"
    if(industry == "Pharma") number = "Pharma"
    if(industry == "Real Estate") number = "Real Estate"
    if(industry == "Retail") number = "Retail"
    if(industry == "Sports") number = "Sports"
    if(industry == "Telecom") number = "Telecom"
    if(industry == "Transportation") number = "Transportation"
    if(industry == "Travel & Tourism") number = "Travel & Tourism"
    if(industry == "Others") number = "Others"

    setTimeout(() => {
      if(this.industryPlaceholder.includes(industry)){
        this.industryPlaceholder.splice(this.industryPlaceholder.indexOf(industry), 1)
      }
      else {
        this.industryPlaceholder.push(industry)
      }
      this.industryPlaceholder = this.industryPlaceholder.join(', ')
      if(this.industry.includes(number)){
        this.industry.splice(this.industry.indexOf(number), 1)

      }
      else this.industry.push(number)

      for(let i=0; i<this.industry.length; i++){
        let industryVal = {
          name: this.industry[i],
          mode: 1
        }
        this.industryValue.push(industryVal)
      }
      this.cd.detectChanges()
    }, 10);
  }

  public uploadTemplateImage(event){
    let file = event.target.files[0];
    // File size should be less than 1 mb
    this.fileName = file.name;
    if (file && file.size <= 1000000) {
      // File type should be .png or .jpg
      if (file.type == 'image/png' || file.type == 'image/jpg') {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (event) => {
          let url = event.target.result;
          this.templateImage = url;
        }
        this.showImageError = false;
      } else {
        this.showImageError = true;
      }
    } else {
      this.showImageError = true;
    }
  }

  public getWorkflows(){
    this.workflowPlaceholder = []
    let businessUUID = JSON.parse(localStorage.getItem('planName'))['businessUUID']
    this.templateService.getTemplateWorkflow(businessUUID).pipe(takeUntil(this.componentDestroyed$))
    .subscribe(
      data => {
        this.workflowData = data?.result
        for(let i=0; i<this.workflowData.length; i++){
          this.workFlowFolders.push(this.workflowData[i].folder_id)
          if(this.selectedWorkflowData.includes(this.workflowData[i].name))this.workflowPlaceholder.push(this.workflowData[i].name)
          this.workflowPlaceholder = this.workflowPlaceholder.toString().split(',').join(', ')
        }
      }
    )
  }

  public selectWorkflows(workflow){
    if(this.workflowPlaceholder.length > 0) this.workflowPlaceholder = this.workflowPlaceholder.split(', ')
      else this.workflowPlaceholder = []
      let number, workFlowVal
      for(let i = 0; i < this.workflowData.length; i++){
        if(workflow.name == this.workflowData[i].name){
          number = this.workflowData[i].name
          workFlowVal = this.workflowData[i]
        }
      }
  
      setTimeout(() => {
        if(this.workflowPlaceholder.includes(workflow.name)){
          this.workflowPlaceholder.splice(this.workflowPlaceholder.indexOf(workflow.name), 1)
        }
        else {
          this.workflowPlaceholder.push(workflow.name)
        }
        this.workflowPlaceholder = this.workflowPlaceholder.join(', ')
        if(this.selectedWorkflowData?.includes(number)){
          this.selectedWorkflowData.splice(this.selectedWorkflowData.indexOf(number), 1)
          this.workFlowArr.splice(this.workFlowArr.indexOf(workFlowVal), 1)
        }
        else{
          this.selectedWorkflowData.push(number)
          this.workFlowArr.push(workFlowVal)
        }
        this.workflowValue = {}
        let arr: any = []
        this.workFlowData = []
        for(let i=0; i<this.workFlowFolders.length; i++){
          for(let j=0; j<this.workFlowArr.length; j++){
            if(this.workFlowFolders[i] == this.workFlowArr[j].folder_id){
              let folder: any = this.workFlowArr[j].folder_id
              arr.push(this.workFlowArr[j].name)
              this.workflowValue[folder] = arr
            }
          }
          arr = []
        }
        let keys = Object.keys(this.workflowValue)
        let values = Object.values(this.workflowValue)
        for(let i=0; i<keys.length; i++){
          this.workFlowData.push({folderId: keys[i], recipes: values[i]})
        }
        this.cd.detectChanges()
      }, 10);
  }

  public displaySubDropDownMenu(id){
    let x = document.getElementById(id)
    x.style.display = "block"
    if(id == "industryMenu") x.scrollIntoView(true)
    if(id == "accountMenu") x.scrollIntoView(true)
    if(id == "workflow-dropdown") x.scrollIntoView(true)
  }

  public getAllClientData(){
    this.currentClients = []
    let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    this._client.getAllClientDetails(timezone, '').pipe(takeUntil(this.componentDestroyed$))
    .subscribe(
      data => {
        for (let index = 0; index < data.result.length; index++) {
          const client = data.result[index];
          let expDate = new Date(client['contractEndDate'])
          if(expDate.getTime() > new Date().getTime()) this.currentClients.push(client)
        }
      }
    )
  }

  public selectAccount(client){
    let clientData = client
    this.selectedClient = clientData.businessUUID
    this.selectedClientPlaceholder = clientData.kingName
  }

  public saveTemplate(){
    this.templateDetails = {
      "language": this.language,
      "type": "dropthought",
      "businessUUID": this.selectedClient,
      "timezone": this.timezone,
      "templateName": this.createTemplateSurveyForm.value.createTemplateName,
      "advanced": this.selectedTemplateType == 'Advanced' ? true : false,
      "templateImage": this.templateImage,
      "industry": this.industryValue,
      "workflows": this.workFlowData
    }
    this.templateService.createTemplate(this.createTemplateSurveyForm.value.programId, JSON.stringify(this.templateDetails)).pipe(takeUntil(this.componentDestroyed$))
    .subscribe(data =>{
      console.log(data)
      if(data.success){
        this.showProgramError = false
        this._router.navigate(['/templates'])
      }
      else{
        this.showProgramError = true
        if(data.message == "Survey not found"){
          this.programErrorMsg = "Please enter a valid survey ID."
        }
        if(data.message == "This advanced template is missing Filters, Triggers, TA categories"){
          this.programErrorMsg = "This advanced template is missing Filters, Triggers, TA categories"
        }
      }
    })
  }

  public removeError(){
    if((this.createTemplateSurveyForm.controls.programId && this.createTemplateSurveyForm.controls.programId.value.length == 0) || this.createTemplateSurveyForm.controls.programId.dirty){
      this.showProgramError = false
      this.programErrorMsg = ''
    }
  }

  public cancelTemplate(){
    this.createTemplateSurveyForm.reset()
    this.industry = []
    this.templateImage = ''
    this.fileName = ''
    this.selectedTemplateType = 'Basic'
    this._router.navigate(['/templates'])
  }

}
