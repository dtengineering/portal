import { Component, OnInit, ViewChild,ChangeDetectorRef, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ClientServiceService } from '../../services/client-service.service'
import { ClientCardComponent } from './client-card/client-card.component'
import { AccessSettingsService } from '../../services/access-settings.service'
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { fromEvent } from 'rxjs';

declare const $ : any
@Component({
  selector: 'app-client-screen',
  templateUrl: './client-screen.component.html',
  styleUrls: ['./client-screen.component.css']
})
export class ClientScreenComponent implements OnInit {
  @ViewChild('clientSearchInput', { static: true }) clientSearchInput: ElementRef;

  public clients: Array<Object> = []
  public currentClients: Array<Object> = []
  public pastClients: Array<Object> = []
  public pastClient: boolean

  public savePressed: boolean
  public addClient: boolean
  public companyName: string = ""
  public empRangePlaceholder: string = ""
  public empRange: number
  public industryPlaceholder: any = []
  public industry: Array<string> = []
  public contractStartDate: string = ""
  public contractEndDate: string = ""

  public businessDetails: any = {}

  public kingName: string = ""
  public kingEmail: string = ""
  public kingMobile: string = ""
  public kingTitle: string = ""



  public companyNameError: boolean
  public contractStartDateError: boolean
  public contractEndDateError: boolean
  public industryPlaceholderError: boolean
  public empRangePlaceholderError: boolean
  public kingNameError: boolean
  public kingEmailError: boolean
  public kingMobileError: boolean
  public kingTitleError: boolean
  public errorCheck: boolean

  public defaultRange: Array<String> = ['1-50', '51-100', '101-500', '501-1000', '1001-5000', '5001-10000', '>10000']
  public defaultIndustry: Array<String> = ['Automotive', 'Airlines', 'Agriculture', 'Advertising/Public Relations', 'Banking and Finance','Construction', 'Education', 'Energy', 'Entertainment', 'Event Management', 'Food & Beverage', 'Government service', 'Human Resource', 'Healthcare', 'Hospitality', 'Insurance', 'Information Technology', 'Lawyers / Law Firms', 'Logistics', 'Media', 'Oil & Gas', 'Pharma', 'Real Estate', 'Retail', 'Sports', 'Telecom', 'Transportation', 'Travel & Tourism', 'Others']
  public countryPlaceholder: Array<string> = ["India", "Oman", "United Arab Emirates", "United States"]
  public country: number
  public countryError: boolean
 
  
  public kingEmailFormatError: boolean
  public clientsReceivedFromAPI: boolean = false
  public showErrorMessage: string = ''
  public disableButton: boolean = false

  public oldClientForTesting: Object = {
    businessAddress: "",
businessEmail: "venkat@yopmail.com",
businessId: 30,
businessName: "Atest",
businessPhone: null,
businessTimezone: "Asia/Calcutta",
businessTypeId: 1,
businessUUID: "1f987cfc-becd-4202-827e-29f977f97097",
contractEndDate: "2019-06-19 04:54:36.0",
contractStartDate: "2018-06-19 04:54:36.0",
createdTime: "2020-06-19 10:24:36.0",
csAccess: "0",
industry: null,
kingUserName: "venkat@yopmail.com",
lastLoginTime: "2020-06-19 10:42:33.0",
modifiedTime: "2020-06-19 10:24:36.0",
reason: null,
state: "1",
status: "Default"
  }
  @ViewChild(ClientCardComponent) clientCard: ClientCardComponent



  public successfullyCreated: boolean
  public tempContractDateForPopup: Date
  public dateNow: Date = new Date()

  public hippaData: boolean = false
  public searchByClientKingNameEmail: any = ""
  private searchValueSubscription: Subscription;
  componentDestroyed$: Subject<boolean> = new Subject();

  constructor(private _client: ClientServiceService, private _cd: ChangeDetectorRef, private _router: Router, private _accessSettings: AccessSettingsService) { }

  ngOnInit() {
    if(localStorage.getItem("saveSuccess")){
      this.dateNow.setHours(0,0,0,0);
      this.tempContractDateForPopup = new Date(localStorage.getItem("saveSuccess"))
      this.tempContractDateForPopup.setHours(0,0,0,0);
      this.showSaveSuccessPopup()
    }
    this.getAllClientData()
    this.searchClient();
    this.loadScripts()
  }


  ngDoCheck(){
    if(this.addClient){
      this.loadScripts()
    }
  }

  public getPage(){
    let page = localStorage.getItem("page")
    if(page == "past"){
      this.showPastClients()
    }
    else {
      this.showCurrentClients()
    }
  }

  //Function to search the clients
  public searchClient(){
    fromEvent(this.clientSearchInput.nativeElement, 'keyup').pipe(
      // get value
      map((event: any) => {
        return event.target.value;
      })
      // // if character length greater then 2
      // , filter(res => res.length > 2)

      // Time in milliseconds between key events
      , debounceTime(1000)

      // If previous query is diffent from current   
      , distinctUntilChanged()

      // subscription for response
    ).subscribe((text: string) => {
      this.getAllClientData();
    });
  }

  public getAllClientData(){
    this.currentClients = []
    this.pastClients = []
    let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    this._client.getAllClientDetails(timezone, this.searchByClientKingNameEmail).pipe(takeUntil(this.componentDestroyed$))
    .subscribe(
      data => {
        console.log(data)
        this.clients = data.result
        this.clientsReceivedFromAPI = true
        localStorage.setItem("planName", JSON.stringify(this.clients[0]))
        // this.clients.push(this.oldClientForTesting)
        for (let index = 0; index < this.clients.length; index++) {
          const client = this.clients[index];
          let expDate = new Date(client['contractEndDate'])
          if(expDate.getTime() < new Date().getTime()) this.pastClients.push(client)
          else this.currentClients.push(client)
        }
        this.getPage()
      }
    )
  }

  public showPastClients(){
    localStorage.setItem("page", "past")
    this.clients = this.pastClients
    this.pastClient = true
  }


  public showCurrentClients(){
    localStorage.setItem("page", "current")
    this.clients = this.currentClients
    this.pastClient = false
  }

  public showClientDetailPopup(){
      this.addClient = true
      this.loadScripts()
  }
  
  public saveDetails(){
    this.savePressed = true
    this.runSaveChecks()
    
    if(this.errorCheck){
      let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
      this.businessDetails = {
        "businessName":this.companyName,
        "email":this.kingEmail,
        "fullName":this.kingName,
        "noOfEmployees":this.empRange,
        "phone":this.kingMobile,
        "contractStartDate":this.contractStartDate,
        "contractEndDate":this.contractEndDate,
        "industry":this.industry,
        "title":this.kingTitle,
        "country":this.country,
        "businessTimezone":timezone,
        "hippa": this.hippaData ? 1 : 0
        }
      this.disableButton = true
      let input = JSON.stringify(this.businessDetails)
      this._client.createNewClient(input).subscribe(data=>{
        console.log(data)
        this.disableButton = false
        if(data.error){
          this.showErrorMessage = data.error
        }
        else if(data.result[0]){
          this.showErrorMessage = ""
          localStorage.setItem("newClient", "1")
          localStorage.setItem('hippaStatus', this.hippaData ? "1" : "0")
          this._router.navigate(['/client-details/' + data.result[0].businessUUID])
        } 
      })
    }
  }

  public runSaveChecks(){
    if(this.savePressed){
      console.log("Company name = " + this.companyName)
      console.log("contractStartDate = " + this.contractStartDate)
      console.log("contractEndDate = " + this.contractEndDate)
      console.log("industryPlaceholder = " + this.industryPlaceholder)
      console.log("empRangePlaceholder = " + this.empRangePlaceholder)
      console.log("kingName = " + this.kingName)
      this.companyNameError = false
      this.contractStartDateError = false
      this.contractEndDateError = false
      this.industryPlaceholderError = false
      this.empRangePlaceholderError = false
      this.kingNameError = false
      this.kingEmailError = false
      this.kingMobileError = false
      this.kingTitleError = false
      this.kingEmailFormatError = false
      this.countryError = false
      if(this.companyName.trim().length == 0){
        this.companyNameError = true
      }
      if(this.contractEndDate.trim().length == 0 || this.contractStartDate.trim().length == 0){
        this.contractStartDateError = true
      }
      if(this.contractStartDate.length == 0){
        this.contractStartDateError = true
      }
      if(this.contractEndDate.length == 0){
        this.contractEndDateError = true
      }
      if(this.industry.length == 0){
        this.industryPlaceholderError = true
      }
      if(!this.country){
        this.countryError = true
      }
      if(this.empRangePlaceholder.length == 0){
        this.empRangePlaceholderError = true
      }
      if(this.kingName.trim().length == 0){
        this.kingNameError = true
      }
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(this.kingEmail.trim().length == 0 ){
        this.kingEmailError = true
      }
      if(this.businessDetails.email != this.kingEmail){
        this.showErrorMessage = ""
      }
      if(!re.test(String(this.kingEmail).toLowerCase())){
        this.kingEmailFormatError = true
      }
      const me = /^[0-9*#+]+$/
      // const me = /^[0-9? =.*@#$%^&*~!;:\/{}"'_(){}|\[\],-]+$/
      if(this.kingMobile.trim().length == 0 || !me.test(String(this.kingMobile).toLowerCase())){
        this.kingMobileError = true
      }
      if(this.kingTitle.trim().length == 0){
        this.kingTitleError = true
      }
      if(this.contractStartDateError || this.kingEmailFormatError || this.contractStartDateError || this.contractEndDateError || this.industryPlaceholderError || this.empRangePlaceholderError || this.kingNameError || this.kingEmailError || this.kingMobileError || this.kingTitleError){
        this.errorCheck = false
      }
      else this.errorCheck = true
      console.log(this.errorCheck)

      this._cd.detectChanges()
    }
  }

  public displaySubDropDownMenu(id){
    let x = document.getElementById(id)
    x.style.display = "block"
    if(id == "industryMenu") x.scrollIntoView(true)
  }

  public selectRange(id){
    setTimeout(() => {
      if(id == 0){
        this.empRangePlaceholder = "1-50"
        this.empRange = 1
      }
      if(id == 1){
        this.empRangePlaceholder = "51-100"
        this.empRange = 2
      }
      if(id == 2){
        this.empRangePlaceholder = "101-500"
        this.empRange = 3
      }
      if(id == 3){
        this.empRange = 4
        this.empRangePlaceholder = "501-1000"
      }
      if(id == 4){
        this.empRangePlaceholder = "1001-5000"
        this.empRange = 5
      }
      if(id == 5){
        this.empRangePlaceholder = "5001-10000"
        this.empRange = 6
      }
      if(id == 6){
        this.empRangePlaceholder = ">10000"
        this.empRange = 7
      }
      this.runSaveChecks()
      document.getElementById("empRange").style.display = "none"
      this._cd.detectChanges()
    }, 5);
  }



  public selectIndustry(industry){
    let number
    if(industry == "Automotive") number = 1
    if(industry == "Airlines") number = 2
    if(industry == "Agriculture") number = 3
    if(industry == "Advertising/Public Relations") number =4
    if(industry == "Banking and Finance") number =5
    if(industry == "Construction") number =6
    if(industry == "Education") number =7
    if(industry == "Energy") number =8
    if(industry == "Entertainment") number =9
    if(industry == "Event Management") number =10
    if(industry == "Food & Beverage") number =11
    if(industry == "Government service") number =12
    if(industry == "Human Resource") number =13
    if(industry == "Healthcare") number =14
    if(industry == "Hospitality") number =15
    if(industry == "Insurance") number =16
    if(industry == "Information Technology") number =17
    if(industry == "Lawyers / Law Firms") number =18
    if(industry == "Logistics") number =19
    if(industry == "Media") number =20
    if(industry == "Oil & Gas") number =21
    if(industry == "Pharma") number =22
    if(industry == "Real Estate") number =23
    if(industry == "Retail") number =24
    if(industry == "Sports") number =25
    if(industry == "Telecom") number =26
    if(industry == "Transportation") number =27
    if(industry == "Travel & Tourism") number =28
    if(industry == "Others") number =29

    setTimeout(() => {
      if(this.industryPlaceholder.length > 0) this.industryPlaceholder = this.industryPlaceholder.split(',')
      if(this.industryPlaceholder.includes(industry)){
        this.industryPlaceholder.splice(this.industryPlaceholder.indexOf(industry), 1)
      }
      else this.industryPlaceholder.push(industry)
      this.industryPlaceholder = this.industryPlaceholder.toString()
      if(this.industry.includes(number)){
        this.industry.splice(this.industry.indexOf(number), 1)
      }
      else this.industry.push(number)
      this.runSaveChecks()
      this._cd.detectChanges()
    }, 5);
  }



  loadScripts() {
    let self = this;
    $(document).ready(function () {
      $(".start-date").datepicker({
        // minDate: this.contractStartDate,
        minDate: "-5Y",
        dateFormat: 'M dd yy',
        onSelect: function (dateText, inst) {
          var dateAsString = dateText; //the first parameter of this function
          var dateAsObject = $(this).datepicker('getDate'); //the getDate method
          let date2 = $(this).datepicker('getDate');
          var nextDayDate = new Date($(this).datepicker('getDate'));
          console.log(date2.getDate())
          nextDayDate.setDate(date2.getDate() + 1);
          $("#end-date").datepicker('option', 'minDate', nextDayDate);
          self.onStartInput(dateAsString);
        }
      });
      $(".end-date").datepicker({
        maxDate: "+20Y",
        dateFormat: 'M dd yy',
        onSelect: function (dateText, inst) {
          var dateAsString = dateText;
          var dateAsObject = $(this).datepicker('getDate');
          self.onEndInput(dateAsString);
        }
      });
    });
    $(document).mouseup(function (e){
      var div = document.getElementById('industryMenu');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $('#industryMenu').hide(200);
          }
        }
    });
    $(document).mouseup(function (e){
      var div = document.getElementById('empRange');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $("#empRange").hide(200);
          }
        }
    });
  }  

  onStartInput(sdate) {
    let oldDate = new Date(sdate)
    let newDate = ((oldDate.getMonth() + 1) + "/" +  oldDate.getDate() + "/" +  oldDate.getFullYear()).toString()
    newDate = newDate.replace(/\//g, "-");
    let sdateArray = newDate.split("-");
    sdate = sdateArray[2] + "-" + sdateArray[0] + "-" + sdateArray[1];
    console.log(sdate);
    this.contractStartDate = sdate + " 00:00:00.0"
    this.businessDetails['contractStartDate'] = this.contractStartDate
    this._cd.detectChanges()
    this.runSaveChecks()
  }

  onEndInput(edate) {
    let oldDate = new Date(edate)
    let newDate = ((oldDate.getMonth() + 1) + "/" +  oldDate.getDate() + "/" +  oldDate.getFullYear()).toString()
    newDate = newDate.replace(/\//g, "-");
    let sdateArray = newDate.split("-");
    edate = sdateArray[2] + "-" + sdateArray[0] + "-" + sdateArray[1];
    console.log(edate);
    this.contractEndDate = edate + " 00:00:00.0"
    this.businessDetails['contractEndDate'] = this.contractEndDate
    this._cd.detectChanges()
    this.runSaveChecks()
  }


  public closeNewClient(){
    this.kingEmail = ""
    this.kingMobile = ""
    this.kingName = ""
    this.kingTitle = ""
    this.companyName = ""
    this.contractEndDate = ""
    this.contractStartDate = ""
    this.industry = []
    this.industryPlaceholder = []
    this.empRange = null
    this.empRangePlaceholder = ''
    this.kingEmailFormatError = false
    this.savePressed = false
    this.companyNameError = false
    this.contractStartDateError = false
    this.industryPlaceholderError = false
    this.empRangePlaceholderError = false
    this.kingEmailError = false
    this.kingMobileError = false
    this.kingNameError = false
    this.kingTitleError = false
    this.addClient=false
    this.country = null
    this.countryError = false
  }

  public selectCountry(val){
    this.country = val
    this.runSaveChecks()
    console.log(this.country)
    console.log(this.countryPlaceholder)
  }


  public showSaveSuccessPopup(){
    localStorage.removeItem("saveSuccess")
    this.successfullyCreated = true
    setTimeout(() => {
      this.successfullyCreated = false
    }, 2000);
    
  }


  public removeSuccessfullyUpdated(){
    this.successfullyCreated = false
  }

  public convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/')
  }

  public setHippaStatus(hippa){
    this.hippaData = hippa ? true : false
    console.log(this.hippaData)
  }

  ngOnDestroy(){
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.unsubscribe();
    this.searchValueSubscription?.unsubscribe()
  }
}
