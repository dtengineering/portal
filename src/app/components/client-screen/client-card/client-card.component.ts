import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

import { ClientServiceService } from '../../../services/client-service.service'
import { Observable } from 'rxjs';

declare const $ : any
@Component({
  selector: 'app-client-card',
  templateUrl: './client-card.component.html',
  styleUrls: ['./client-card.component.css']
})
export class ClientCardComponent implements OnInit {

  @Input() clients;
  @Input() pastClients;
  @Output() getAllClients = new EventEmitter<string>();
  
  public showDropDownMenu: boolean = false
  public contractPopup: boolean = false
  public contractStartDate: string
  public contractEndDate: string
  public client: any
  public clientUUID: string
  public industry: Array<number> = [];
  public industryPlaceholder: Array<string> = []
  public businessDetails: Object = {}

  public empRange: string
  public empRangePlaceholder: string
  public successfullyUpdated: boolean = false



  public kingName: string
  public kingEmail: string
  public kingMobile: string
  public kingTitle: string
  public companyName: string

  constructor(private _router: Router, private _client: ClientServiceService, private _cd: ChangeDetectorRef) { }

  ngOnInit() {
    console.log(this.clients)
    this.loadScripts()
  }


  ngDoCheck(){
    if(this.contractPopup){
      this.loadScripts()
    }
  }


  public getClientInfo(){
    let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    this._client.getClientDetails(this.clientUUID, timezone).subscribe( data => {
      this.client = data.result[0]
      this.companyName = this.client['businessName']
      this.contractStartDate = new Date(this.client['contractStartDate']).toDateString().split(' ').slice(1).join(' ')
      this.contractEndDate = new Date(this.client['contractEndDate']).toDateString().split(' ').slice(1).join(' ')
      this.businessDetails['contractStartDate'] = this.client['contractStartDate']
      this.businessDetails['contractEndDate'] = this.client['contractEndDate']
      this.businessDetails['contractStartDate'] = this.client['contractStartDate']
      this.kingName = this.client['kingName']
      this.kingEmail = this.client['kingEmail']
      this.kingMobile = this.client['kingMobile']
      this.kingTitle = this.client['kingTitle']
      this.kingTitle = this.client['kingTitle']
      this.getEmpRange()
      this.industry = []
      if(this.client['industry']) this.industry = JSON.parse(this.client['industry'])
      this.getIndustry()
    })
  }

  public getEmpRange(){
    this.empRange = this.client['employeesRange']
    if(this.client['employeesRange'] == 1){
      this.empRangePlaceholder = "50-100"
    }
    if(this.client['employeesRange'] == 2){
      this.empRangePlaceholder = "100-500"
    }
    if(this.client['employeesRange'] == 3){
      this.empRangePlaceholder = "500-1000"
    }
    if(this.client['employeesRange'] == 4){
      this.empRangePlaceholder = "1000-5000"
    }
    // if(this.client['employeesRange'] == 5){
    //   this.empRangePlaceholder = "5000-10000"
    // }


  }
  
  navigateToClientDetails(client, clientUUID){
    localStorage.setItem("planName", JSON.stringify(client))
    setTimeout(() => {
      this._router.navigate(['/client-details/' + clientUUID ]);      
    }, 10);
  }

  public displaySubDropDownMenu(id){
    id = document.getElementById(id)
    id.style.display = "block"
  }

  public selectRange(id){
    this.empRange = id
    setTimeout(() => {
      if(id == 1){
        this.empRangePlaceholder = "50-100"
      }
      if(id == 2){
        this.empRangePlaceholder = "100-500"
      }
      if(id == 3){
        this.empRangePlaceholder = "500-1000"
      }
      if(id == 4){
        this.empRangePlaceholder = "1000-5000"
      }
      // if(id == 5){
      //   this.empRangePlaceholder = "5000-10000"
      // }
      document.getElementById("empRange").style.display = "none"
    }, 5);
  }

  public getIndustry(){
    this.industryPlaceholder = []
    if(this.industry.includes(1)){
      this.industryPlaceholder.push("Healthcare")
    }
    if(this.industry.includes(2)){
      this.industryPlaceholder.push("Retail")
    }
    if(this.industry.includes(3)){
      this.industryPlaceholder.push("Automotive")
    }
    if(this.industry.includes(4)){
      this.industryPlaceholder.push("Pharma")
    }
  }


  public selectIndustry(industry){
    this.industryPlaceholder = []
    setTimeout(() => {
      console.log(this.industry)
      if(this.industry.includes(industry)){
        this.industry.splice(this.industry.indexOf(industry), 1)
      }
      else this.industry.push(industry)
      
      if(this.industry.includes(1)){
        this.industryPlaceholder.push("Healthcare")
      }
      if(this.industry.includes(2)){
        this.industryPlaceholder.push("Retail")
      }
      if(this.industry.includes(3)){
        this.industryPlaceholder.push("Automotive")
      }
      if(this.industry.includes(4)){
        this.industryPlaceholder.push("Pharma")
      }
      this._cd.detectChanges()
      
    }, 5);
  }

  public showRenewContractPopup(businessId){
    this.contractPopup = true
    this.clientUUID = businessId
    this.getClientInfo()
  }

  public hideRenewContractPopup(){
    this.contractPopup = false
  }


  onStartInput(sdate) {
    $("#end-date").datepicker('option', 'minDate', new Date(sdate));
    sdate = sdate.replace(/\//g, "-");
    let sdateArray = sdate.split("-");
    sdate = sdateArray[2] + "-" + sdateArray[0] + "-" + sdateArray[1];
    console.log(sdate);
    this.contractStartDate = sdate + " 00:00:00.0"
    this.businessDetails['contractStartDate'] = this.contractStartDate
  }

  onEndInput(edate) {
   // $("#end-date").datepicker('option', 'minDate', new Date(this.surveyData.surveyStartDate));
    edate = edate.replace(/\//g, "-");
    let sdateArray = edate.split("-");
    edate = sdateArray[2] + "-" + sdateArray[0] + "-" + sdateArray[1];
    console.log(edate);
    this.contractEndDate = edate + " 00:00:00.0"
    this.businessDetails['contractEndDate'] = this.contractEndDate
  }

  loadScripts() {
    let self = this;
    $(document).ready(function () {
      $(".start-date").datepicker({
        // minDate: this.contractStartDate,
        onSelect: function (dateText, inst) {
          //this.console.consoleLogPrint("picking stast date");
          var dateAsString = dateText; //the first parameter of this function
          var dateAsObject = $(this).datepicker('getDate'); //the getDate method
          //var formattedDate = $(this).datepicker.formatDate("mm-dd-yyyy",dateAsObject);
          $("#end-date").datepicker('option', 'minDate', $(this).datepicker('getDate') || new Date($.now()));
          //this.console.consoleLogPrint(dateAsObject);
          self.onStartInput(dateAsString);
        }
      });
      $(".end-date").datepicker({
        onSelect: function (dateText, inst) {
          var dateAsString = dateText; //the first parameter of this function
          var dateAsObject = $(this).datepicker('getDate'); //the getDate method
          //var formattedDate = $(this).datepicker.formatDate("mm-dd-yyyy",dateAsObject);
          //this.console.consoleLogPrint(dateAsObject);
          self.onEndInput(dateAsString);
        }
      });
    });
    $(document).mouseup(function (e){
      var div = document.getElementById('industryMenu');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $('#industryMenu').hide(200);
          }
        }
    });
    $(document).mouseup(function (e){
      var div = document.getElementById('empRange');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $("#empRange").hide(200);
          }
        }
    });
  }  

  public updateBusinessDetails(){
    this.businessDetails['businessUUID'] = this.clientUUID
    this.businessDetails['kingName'] = this.kingName
    this.businessDetails['kingEmail'] = this.kingEmail
    this.businessDetails['kingMobile'] = this.kingMobile
    this.businessDetails['kingTitle'] = this.kingTitle
    this.businessDetails['industry'] = this.industry
    this.businessDetails['noOfEmployees'] = this.empRange
    this.businessDetails['businessName'] = this.companyName
    console.log(this.businessDetails)
    this._client.updateContractDates(this.clientUUID, JSON.stringify(this.businessDetails)).subscribe(data => {
      console.log(data)
      this.getAllClients.emit("x")
      // window.location.reload()
    })
  }

  public saveDetails(){
    // if(this.contractStartDate) this.businessDetails['contractStartDate'] = this.contractStartDate
    // if(this.contractEndDate) this.businessDetails['contractEndDate'] = this.contractEndDate
    this.updateBusinessDetails()
    this.contractPopup = false
    this.successfullyUpdated = true
    setTimeout(()=> 
      this.successfullyUpdated = false,2000);
  }
  
  public removeSuccessfullyUpdated(){
    this.successfullyUpdated = false
  }

  public updateCompanyName(val){
    this.companyName = val
  }
}
