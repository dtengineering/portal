import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameplansComponent } from './gameplans.component';

describe('GameplansComponent', () => {
  let component: GameplansComponent;
  let fixture: ComponentFixture<GameplansComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameplansComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GameplansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
