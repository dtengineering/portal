import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exploregameplan',
  templateUrl: './exploregameplan.component.html',
  styleUrls: ['./exploregameplan.component.css']
})
export class ExploregameplanComponent implements OnInit {

  constructor(private _router: Router) { }

  public selectedGamePlan: any
  public language: any

  ngOnInit(): void {
    this.language = localStorage.getItem('language') ? localStorage.getItem('language') : 'en'
    let gamePlan = localStorage.getItem("selectedGameplan")
    this.selectedGamePlan = JSON.parse(gamePlan)
    console.log(this.selectedGamePlan)
  }

  public goToGameplansPage(){
    this._router.navigate(['/gameplans'])
  }

  public goToTemplatePreview(info){
    console.log(info)
    localStorage.setItem('language', this.language)
    localStorage.setItem('pageType', 'gameplan')
    this._router.navigate(['/template-preview/' + info.templateId])
  }

}
