import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploregameplanComponent } from './exploregameplan.component';

describe('ExploregameplanComponent', () => {
  let component: ExploregameplanComponent;
  let fixture: ComponentFixture<ExploregameplanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExploregameplanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExploregameplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
