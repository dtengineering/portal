import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameplanService } from 'src/app/services/gameplan.service';

@Component({
  selector: 'app-gameplans',
  templateUrl: './gameplans.component.html',
  styleUrls: ['./gameplans.component.css']
})
export class GameplansComponent implements OnInit {

  constructor(private _router: Router, private gameplanService: GameplanService) { }

  public timezone: any
  public language: any
  public gameplansList: any
  public selectedGameplan: any
  public activeTabs: any = []
  public gameplanStatus: any
  public gameplansListLive: any = []
  public gameplansListDraft: any = []
  public maxLength: any = 125

  ngOnInit(): void {
    this.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone
    this.language = localStorage.getItem('language');
    this.getAllGameplans()
  }

  public getAllGameplans(){
    let userId = 0;
    this.gameplanService.getAllGameplans(userId).subscribe(data => {
      if(data) {
        let gameplans = data?.result ? data?.result : [];
        this.gameplansListLive = gameplans.filter(gameplan => gameplan.state == 1);
        this.gameplansListDraft = gameplans.filter(gameplan => gameplan.state == 0);
        this.selectTab('livegame');
        console.log(this.gameplansListLive);
        console.log(this.gameplansListDraft);
      } else {
        this.gameplansListLive = [];
        this.gameplansListDraft = [];
      }
    });
  }

  public goToCreateGameplan(){
    localStorage.setItem("gameplanStatus", "create");
    this._router.navigate(['/create-gameplan'])
  }

  public exploreGamePlan(gameplan){
    console.log(gameplan);
    this.selectedGameplan = gameplan;
    localStorage.setItem("selectedGameplan", JSON.stringify(this.selectedGameplan));
    this._router.navigate(['/explore-gameplan'])
  }

  public selectTab(selectedTab) {
    switch (selectedTab) {
      case 'livegame':
        this.activeTabs = ['active', '']
        this.gameplansList = this.gameplansListLive
        break
      case 'drafts':
        this.activeTabs = ['', 'active']
        this.gameplansList = this.gameplansListDraft
        break
    }
  }

}
