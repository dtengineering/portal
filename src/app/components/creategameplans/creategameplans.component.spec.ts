import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreategameplansComponent } from './creategameplans.component';

describe('CreategameplansComponent', () => {
  let component: CreategameplansComponent;
  let fixture: ComponentFixture<CreategameplansComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreategameplansComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreategameplansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
