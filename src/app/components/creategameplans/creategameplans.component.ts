import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GameplanService } from 'src/app/services/gameplan.service';
import { TemplateService } from 'src/app/services/template.service';
import { generalValidator, gameplanNameExists } from 'src/app/validations/validators';

declare var $ : any;

@Component({
  selector: 'app-creategameplans',
  templateUrl: './creategameplans.component.html',
  styleUrls: ['./creategameplans.component.css']
})
export class CreategameplansComponent implements OnInit {

  constructor(private _router: Router, public formBuilder: FormBuilder, private gameplanService: GameplanService, private cd: ChangeDetectorRef, private _templateService: TemplateService) { 
    this.createGameplanSurveyForm = this.formBuilder.group({
      createGameplanName: ["", Validators.compose([Validators.minLength(1), Validators.maxLength(64), generalValidator]), gameplanNameExists.bind(this)],
      shortDescription: ["", Validators.compose([Validators.minLength(1), Validators.maxLength(2000)])],
      objectiveValue: ["", Validators.compose([Validators.minLength(1), Validators.maxLength(2000)])],
      outcomeValue: ["", Validators.compose([Validators.minLength(1), Validators.maxLength(2000)])]
    });
  }
  
  componentDestroyed$: Subject<boolean> = new Subject();
  public gameplanStatus: any
  public createGameplanSurveyForm: FormGroup;
  public createGameplanName: any = '';
  public defaultIndustry: Array<String> = ['Automotive', 'Airlines', 'Agriculture', 'Advertising/Public Relations', 'Banking and Finance','Construction', 'Education', 'Energy', 'Entertainment', 'Event Management', 'Food & Beverage', 'Government service', 'Human Resource', 'Healthcare', 'Hospitality', 'Insurance', 'Information Technology', 'Lawyers / Law Firms', 'Logistics', 'Media', 'Oil & Gas', 'Pharma', 'Real Estate', 'Retail', 'Sports', 'Telecom', 'Transportation', 'Travel & Tourism', 'Others']
  public industryPlaceholderError: boolean = false
  public industry: Array<number> = []
  public industryPlaceholder: any = []
  public industryValue: any = []
  public selectedSurveyName: string = '';
  public keyFeaturesList: any = []
  public selectedKeyFeature: any = []
  public keyFeaturePlaceholder: any = []
  public addAnotherCount: number = 1
  public templatesList: any = []
  public timezone: any
  public language: any
  public selectedTemplate: any
  public shortDescription: string = '';
  public objectiveValue: string = '';
  public outcomeValue: string = '';
  public templateNames: any = []
  public templatesDropdown: any = []
  public templateIds: any = []
  public templateType: any = []
  public gameplanSuccess: boolean = false

  //tinyMCEConfiguration
  public tinyMCEConfig: any = {
    height: 350,
    menubar: false,
    statusbar: false,
    setup: function(ed) {
      ed.on("keydown", function(e) {
        //define local variables
        var tinylen;
        var maxlength = parseInt($("#" + (ed.id)).attr("maxlength"));
  
        //grabbing the length of the curent editors content
        tinylen = ed.getContent().replace(/(<([^>]+)>)/ig,"").length;
        //if the user has exceeded the max turn the path bar red.
        if (tinylen > maxlength && e.keyCode != 8 && e.keyCode != 46) { // -1 to account for the \n character
          // prevent insertion of typed character
          e.preventDefault();
          e.stopPropagation();
          return false;
        } 
      });
    },
  plugins: 
    'lists link code'
    // 'advlist autolink lists link image charmap print preview anchor',
    // 'visualblocks code fullscreen',
    // 'insertdatetime media table paste code wordcount'
  ,
  toolbar:
    'undo redo | alignleft aligncenter alignright alignjustify | \
    bold italic underline strikethrough | bullist numlist link hr | \
    fontfamily fontsize forecolor backcolor | code'
};

  ngOnInit(): void {
    this.gameplanStatus = localStorage.getItem('gameplanStatus')
    this.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone
    this.language = localStorage.getItem('language') ? localStorage.getItem('language') : 'en';
    this.getIndustry()
    this.getKeyFeatures()
    this.getAllTemplates()
    this.loadScripts()
    this.templatesDropdown.push(this.addAnotherCount)
  }

  ngOnDestroy(): void {
    localStorage.removeItem('gameplanStatus')
  }

  
  loadScripts() {
    $(document).mouseup(function (e){
      var div = document.getElementById('industryMenu');
        var topMenuDiv = $(div);
        if(div){
          if (!topMenuDiv.is(e.target) && topMenuDiv.has(e.target).length === 0){
            $('#industryMenu').hide(200);
          }
        }
    });
    $(document).mouseup(function (e){
      var menuDiv: any = document.querySelectorAll('[id^="showtemplateDropdownMenu_"]')
        if (menuDiv && menuDiv.length > 0) {
          for (let i of menuDiv) {
            var container = $(i);
            if (!container.is(e.target) && container.has(e.target).length === 0) {
              if (i.style.display === "block") {
                i.style.display = "none"
                container.fadeOut();
              }
            }
          }
        }
    });
  }

  public getIndustry(){
    this.industryPlaceholder = []
    if(this.industry.includes(1)) this.industryPlaceholder.push('Automotive')
    if(this.industry.includes(2)) this.industryPlaceholder.push('Airlines')
    if(this.industry.includes(3)) this.industryPlaceholder.push('Agriculture')
    if(this.industry.includes(4)) this.industryPlaceholder.push('Advertising/Public Relations')
    if(this.industry.includes(5)) this.industryPlaceholder.push('Banking and Finance')
    if(this.industry.includes(6)) this.industryPlaceholder.push('Construction')
    if(this.industry.includes(7)) this.industryPlaceholder.push('Education')
    if(this.industry.includes(8)) this.industryPlaceholder.push('Energy')
    if(this.industry.includes(9)) this.industryPlaceholder.push('Entertainment')
    if(this.industry.includes(10)) this.industryPlaceholder.push('Event Management')
    if(this.industry.includes(11)) this.industryPlaceholder.push('Food & Beverage')
    if(this.industry.includes(12)) this.industryPlaceholder.push('Government service')
    if(this.industry.includes(13)) this.industryPlaceholder.push('Human Resource')
    if(this.industry.includes(14)) this.industryPlaceholder.push('Healthcare')
    if(this.industry.includes(15)) this.industryPlaceholder.push('Hospitality')
    if(this.industry.includes(16)) this.industryPlaceholder.push('Insurance')
    if(this.industry.includes(17)) this.industryPlaceholder.push('Information Technology')
    if(this.industry.includes(18)) this.industryPlaceholder.push('Lawyers / Law Firms')
    if(this.industry.includes(19)) this.industryPlaceholder.push('Logistics')
    if(this.industry.includes(20)) this.industryPlaceholder.push('Media')
    if(this.industry.includes(21)) this.industryPlaceholder.push('Oil & Gas')
    if(this.industry.includes(22)) this.industryPlaceholder.push('Pharma')
    if(this.industry.includes(23)) this.industryPlaceholder.push('Real Estate')
    if(this.industry.includes(24)) this.industryPlaceholder.push('Retail')
    if(this.industry.includes(25)) this.industryPlaceholder.push('Sports')
    if(this.industry.includes(26)) this.industryPlaceholder.push('Telecom')
    if(this.industry.includes(27)) this.industryPlaceholder.push('Transportation')
    if(this.industry.includes(28)) this.industryPlaceholder.push('Travel & Tourism')
    if(this.industry.includes(29)) this.industryPlaceholder.push('Others')
    this.industryPlaceholder = this.industryPlaceholder.toString().split(',').join(', ')
  }

  public selectIndustry(industry){
    if(this.industryPlaceholder.length > 0) this.industryPlaceholder = this.industryPlaceholder.split(', ')
    else this.industryPlaceholder = []
    this.industryValue = []
    let number
    if(industry == "Automotive") number = "Automotive"
    if(industry == "Airlines") number = "Airlines"
    if(industry == "Agriculture") number = "Agriculture"
    if(industry == "Advertising/Public Relations") number = "Advertising/Public Relations"
    if(industry == "Banking and Finance") number = "Banking and Finance"
    if(industry == "Construction") number = "Construction"
    if(industry == "Education") number = "Education"
    if(industry == "Energy") number = "Energy"
    if(industry == "Entertainment") number = "Entertainment"
    if(industry == "Event Management") number = "Event Management"
    if(industry == "Food & Beverage") number = "Food & Beverage"
    if(industry == "Government service") number = "Government service"
    if(industry == "Human Resource") number = "Human Resource"
    if(industry == "Healthcare") number = "Healthcare"
    if(industry == "Hospitality") number = "Hospitality"
    if(industry == "Insurance") number = "Insurance"
    if(industry == "Information Technology") number = "Information Technology"
    if(industry == "Lawyers / Law Firms") number = "Lawyers / Law Firms"
    if(industry == "Logistics") number = "Logistics"
    if(industry == "Media") number = "Media"
    if(industry == "Oil & Gas") number = "Oil & Gas"
    if(industry == "Pharma") number = "Pharma"
    if(industry == "Real Estate") number = "Real Estate"
    if(industry == "Retail") number = "Retail"
    if(industry == "Sports") number = "Sports"
    if(industry == "Telecom") number = "Telecom"
    if(industry == "Transportation") number = "Transportation"
    if(industry == "Travel & Tourism") number = "Travel & Tourism"
    if(industry == "Others") number = "Others"

    setTimeout(() => {
      if(this.industryPlaceholder.includes(industry)){
        this.industryPlaceholder.splice(this.industryPlaceholder.indexOf(industry), 1)
      }
      else {
        this.industryPlaceholder.push(industry)
      }
      this.industryPlaceholder = this.industryPlaceholder.join(', ')
      if(this.industry.includes(number)){
        this.industry.splice(this.industry.indexOf(number), 1)

      }
      else this.industry.push(number)
      this.cd.detectChanges()
    }, 10);
  }

  public displaySubDropDownMenu(id, count?){
    if(count != undefined){
      let y = document.getElementById("showtemplateDropdownMenu_" + count)
      y.style.display = "block"
      y.scrollIntoView({behavior: 'smooth', block: 'nearest', inline: 'start'})
    }
    else{
      let x = document.getElementById(id)
      x.style.display = "block"
      if(id == "industryMenu") x.scrollIntoView({behavior: 'smooth', block: 'nearest', inline: 'start'})
    }
  }

  public getKeyFeatures(){
    this.keyFeaturePlaceholder = []
    this.gameplanService.getKeyFeatures().subscribe(data => {
      if(data) {
        this.keyFeaturesList = data?.keyFeatures ? data?.keyFeatures : [];
      }
      else{
        this.keyFeaturesList = []
      }
    });
    for(let i = 0; i < this.keyFeaturesList.length; i++){
      if(this.selectedKeyFeature.includes(this.keyFeaturesList[i])) this.keyFeaturePlaceholder.push(this.keyFeaturesList[i])
    }
    this.keyFeaturePlaceholder = this.keyFeaturePlaceholder.toString().split(',').join(', ')
  }

  public selectKeyFeatures(keyFeature){
    if(this.keyFeaturePlaceholder.length > 0) this.keyFeaturePlaceholder = this.keyFeaturePlaceholder.split(', ')
    else this.keyFeaturePlaceholder = []
    let number
    for(let i = 0; i < this.keyFeaturesList.length; i++){
      if(keyFeature == this.keyFeaturesList[i]) number = this.keyFeaturesList[i]
    }

    setTimeout(() => {
      if(this.keyFeaturePlaceholder.includes(keyFeature)){
        this.keyFeaturePlaceholder.splice(this.keyFeaturePlaceholder.indexOf(keyFeature), 1)
      }
      else {
        this.keyFeaturePlaceholder.push(keyFeature)
      }
      this.keyFeaturePlaceholder = this.keyFeaturePlaceholder.join(', ')
      if(this.selectedKeyFeature.includes(number)){
        this.selectedKeyFeature.splice(this.selectedKeyFeature.indexOf(number), 1)

      }
      else this.selectedKeyFeature.push(number)

      this.cd.detectChanges()
    }, 10);
  }

  public addAnotherCondition(){
    this.addAnotherCount = this.addAnotherCount + 1
    this.templatesDropdown.push(this.addAnotherCount)
    this.templateNames.push('')
    this.templateIds.push('')
    this.templateType.push('')
  }

  public removeTemplateDropdown(count){
    this.templatesDropdown.splice(this.templatesDropdown.indexOf(count), 1)
    this.templateNames.splice(this.templateNames.indexOf(this.templateNames[count]), 1)
    this.templateIds.splice(this.templateIds.indexOf(this.templateIds[count]), 1)
    this.templateType.splice(this.templateType.indexOf(this.templateType[count]), 1)
    this.addAnotherCount = this.addAnotherCount - 1
  }

  public getAllTemplates(){
    this._templateService.getAllTemplate('dropthought', -1, false, this.timezone, this.language, '').subscribe(data => {
      if(data) {
        this.templatesList = data?.result ? data?.result : [];
      } else {
        this.templatesList = [];
      }
    });
  }

  public selectTemplate(count,templates){
    this.selectedTemplate = templates.templateName
    this.templateNames[count] = this.selectedTemplate
    this.templateIds[count] = templates.templateUUID
    this.templateType[count] = 0
  }

  public onPaste(event, maxLimit) {
    if(event){
      const contentLength = event.editor.getContent().replace(/(<([^>]+)>)/ig,"").length;
      const clipboardData = event.event.clipboardData;
      const pastedContent = clipboardData.getData('text/plain');
      const pastedLength = pastedContent.length;
      if (contentLength + pastedLength > maxLimit) {
        // Prevent paste if content length exceeds max limit
        event.event.preventDefault();
      }
    }
  }

  public removeHTMLTags(html) {
    var regX = /(<([^>]+)>)/ig;
    let text = html.replace(regX, "")     
    return text.length - 1; // -1 to account for the \n character
  }

  public saveGameplan(value){
    let state, gameplanData;
    if(value == 'draft'){
      state = 0
    }
    if(value == 'public'){
      state = 1
    }
    let outcomeVal = []
    outcomeVal.push(this.createGameplanSurveyForm.value.outcomeValue)
    gameplanData= [{
      "name": this.createGameplanSurveyForm.controls.createGameplanName.value,
      "objective": this.createGameplanSurveyForm.value.objectiveValue,
      "description": this.createGameplanSurveyForm.value.shortDescription,
      "keyFeatures": this.selectedKeyFeature,
      "outcomes": outcomeVal,
      "language": this.language,
      "templateIds": this.templateIds,
      "templateNames": this.templateNames,
      "industry": this.industry,
      "templateTypes": this.templateType,
      }]
    this.gameplanService.createTemplate(0, state, (gameplanData)).pipe(takeUntil(this.componentDestroyed$))
    .subscribe(data =>{
      console.log(data)
      if(data.success) {
        console.log(data)
        if(state == 1){
          this.gameplanSuccess = true
          setTimeout(()=> {
            this.gameplanSuccess = false;
            this._router.navigate(['/gameplans'])
          },3000);
        }
        else{
          this._router.navigate(['/gameplans'])
        }
      } else {
        console.log("Error")
      }
    });

  }

  public goToGameplans(){
    this._router.navigate(['/gameplans'])
  }

}
