import { BrowserModule } from '@angular/platform-browser';
import { NgModule , Pipe, PipeTransform,} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { Configurations } from "./configurations/configurations"
import { AppComponent } from './components/app.component';
import { ClientScreenComponent } from './components/client-screen/client-screen.component';
import { ClientCardComponent } from './components/client-screen/client-card/client-card.component';
import { ClientDetailsComponent } from './components/client-details/client-details.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { GrantAccessComponent } from './components/grant-access/grant-access.component';
import { AccessSettingsComponent } from './components/client-details/access-settings/access-settings.component';
import { LoginComponent } from './components/login/login.component';
import { AccountComponent } from './components/account/account.component';
import { GrantApprovalComponent } from './components/grant-approval/grant-approval.component';
import { CustomThemeComponent } from './components/custom-theme/custom-theme.component';
import { CustomThemePreviewComponent } from './components/custom-theme-preview/custom-theme-preview.component';
import { TemplatesComponent } from './components/templates/templates.component';
import { CreatetemplatesComponent } from './components/createtemplates/createtemplates.component';
import { TemplatepreviewComponent } from './components/templates/templatepreview/templatepreview.component';
import { GameplansComponent } from './components/gameplans/gameplans.component';
import { CreategameplansComponent } from './components/creategameplans/creategameplans.component';
import { ExploregameplanComponent } from './components/gameplans/exploregameplan/exploregameplan.component';
import { EditorModule } from '@tinymce/tinymce-angular';

//pipes
import { SafeHtmlPipe } from './pipes/safeHtmlPipe.pipe';
import { TruncatePipe } from './pipes/truncatePipe.pipe';

@Pipe({
  name: "Dtdate"
})
export class DtdatePipe implements PipeTransform {
  transform(value: any): any {

    if (value) {
      const temp = value.toString().replace(' ', 'T');
        let isInvalidDate = new Date(temp).toString();
        if (isInvalidDate == 'Invalid Date') {
          return '';
        } else {
          return new Date(temp);
        }
    } else {
        return null;
    }
  }
}
@NgModule({
  declarations: [
    AppComponent,
    ClientScreenComponent,
    ClientCardComponent,
    ClientDetailsComponent,
    NavigationComponent,
    GrantAccessComponent,
    AccessSettingsComponent,
    LoginComponent,
    AccountComponent,
    GrantApprovalComponent,
    DtdatePipe,
    CustomThemeComponent,
    CustomThemePreviewComponent,
    TemplatesComponent,
    CreatetemplatesComponent,
    TemplatepreviewComponent,
    GameplansComponent,
    CreategameplansComponent,
    ExploregameplanComponent,
    SafeHtmlPipe,
    TruncatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    EditorModule
  ],
  providers: [
    Configurations
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
