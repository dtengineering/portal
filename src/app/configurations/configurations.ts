/*
Framework Imports
*/
import { Injectable, OnInit, Input } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { RouterModule, Routes, Router } from "@angular/router"
import { environment } from "../../environments/environment"

@Injectable()
export class Configurations implements OnInit {
  PROTOCOL = "http"
  API_PORT = "8080"
  API_BASE_URL = "/dtapp/api/"
  WEB_PORT = "4200"
  API_HOST = window.location.hostname
  //API_HOST = "https://test.dropthought.com"
  WEB_HOST = window.location.hostname

  PORTAL_BASE_URL = "/api/v1/portal/"
  PORTAL_API_PORT = "8088"
  TEMPLATE_BASE_URL = "/api/v1/"

  // WEB_URL = this.PROTOCOL + "://" + this.WEB_HOST + ":" + this.WEB_PORT
  // OPS_URL = this.PROTOCOL + "://" + this.WEB_HOST
   
  /****  Default - uncomment this before pushing any config file changes ****/
  API_URL =
     environment.API_URL ||
    this.PROTOCOL +
       "://" +
      this.API_HOST +
      ":" +
        this.API_PORT +
       this.API_BASE_URL 

  PORTAL_API_URL =
  environment.PORTAL_URL ||
  this.PROTOCOL +
      "://" +
    this.API_HOST +
    ":" +
     this.PORTAL_API_PORT +
    this.PORTAL_BASE_URL 

  TEMPLATE_API_URL = environment.TEMPLATE_URL ||
  this.PROTOCOL +
      "://" +
    this.API_HOST +
    ":" +
    this.TEMPLATE_BASE_URL

  // Important - comment stage, demo, test and local before pushing any config file changes
  /**** stage  ****/
  // API_URL = 'https://stage-api.dropthought.com/dtapp/api/';
  // PORTAL_API_URL = 'https://stage-api.dropthought.com/api/v1/portal/';
  // TEMPLATE_API_URL = 'https://stage-api.dropthought.com/api/v1/';

  /****  Demo  ****/
  // API_URL = 'https://api1.dropthought.com/dtapp/api/';
  // PORTAL_API_URL = 'https://api1.dropthought.com/api/v1/portal/';
  // TEMPLATE_API_URL = 'https://api1.dropthought.com/api/v1/';

  /****   Test ****/
  // API_URL = 'https://test-api.dropthought.com/dtapp/api/';
  // PORTAL_API_URL = 'https://test-api.dropthought.com/api/v1/portal/';
  // TEMPLATE_API_URL = 'https://test-api.dropthought.com/api/v1/';
  
  /****  Local - not needed ****/
  // PORTAL_API_URL = 'http://localhost:8088/api/v1/portal/';
  // API_URL = 'http://localhost:8080/dtapp/api/';
  // TEMPLATE_API_URL = 'http://localhost:8080/api/v1/';

  SLASH = "/"
  BUSINESS_URL = this.API_URL + "business"
  CHECK_EMAIL_EXISTS_URL = this.API_URL + "validate/email/"
  FORGOT_PASSWORD_URL = this.API_URL + "authenticate/resetpassword?token="
  FACEBOOK_SIGNUP_URL = this.API_URL + "signin/facebook"
  GOOGLE_SIGNUP_URL = this.API_URL + "signin/google"
  LOGIN_URL = this.PORTAL_API_URL + "user/login"
  VERIFY_ACCOUNT_URL = this.API_URL + "authenticate/verifyAccount/"
  CONFIRM_ACCOUNT_URL = this.API_URL + "authenticate/confirmAccount/"
  DROPOFF_RATE_URL = this.API_URL + "dropOffRate?"
  HAZELCAST_URL = this.API_URL + "hazelcast/imap/"
  SURVEY_URL = this.API_URL + "dtsurvey"
  EVENT_URL = this.API_URL + "event"
  DT_SURVEY_URL = this.API_URL + "survey"
  SURVEYCARD_URL = this.API_URL + "surveycard"
  EMP_RANGE_URL = this.API_URL + "emprange"
  SURVEY_FILTER = this.SURVEY_URL + "/filters"
  DUPLICATE_SURVEY_URL = this.SURVEY_URL + "/duplicate"
  USER_URL = this.API_URL + "user/"
  JUST_USER_URL = this.API_URL + "user"
  SOCIAL_LOGIN_URL = this.API_URL + "authenticate/sociallogin?access_token="
  CANCEL_SOCIAL_SIGNUP_URL = this.API_URL + "authenticate/connection/social?email="
  REFRESH_TOKEN_URL = this.API_URL + "token/renew"
  REGISTER_URL = this.API_URL + "register"
  TAG_URL = this.API_URL + "tags"
  TAG_PAGE_URL = this.API_URL + "tags/page/"
  TAG_USER_URL = this.API_URL + "tags/user/"
  TAG_FILTER = this.TAG_URL + "/filters"
  STATIC_SURVEY_LINK_URL = this.API_URL + "dtsurvey/generateToken/"
  RECIPIENT_LIST_URL = this.API_URL + "recipient/companyCode/"
  SEND_SURVEY_URL = this.API_URL + "sendSurvey"
  RECIPIENT_UPLOAD = this.API_URL + "recipient/upload"
  RECIPIENT_ID_URL = this.API_URL + "recipient/"
  SEND_EMAIL_URL = this.API_URL + "email"
  METADATA_URL = this.API_URL + "metadata"
  INITIALIZE_DEFAULT_METADATA_URL =
    this.API_URL + "metadatamap/initializedefault"
  METADATA_MAP_URL = this.API_URL + "metadatamap"
  METADATA_MAP_BUSINESS = this.METADATA_MAP_URL + "/business/"
  METADATA_MAP_TAG = this.METADATA_MAP_URL + "/tag/"
  METADATA_MAP_RECIPIENT_HEADER_URL =
    this.API_URL + "metaDataRecipientHeaderMap"
  RECIPIENT_DATA_URL = this.API_URL + "recipient/"
  RECIPIENT_DATA_ID_URL = this.API_URL + "recipientData/"
  TAG_UPLOAD_MAP_URL = this.API_URL + "taguploadmap"
  //SURVEY_RENDERING_URL = this.WEB_URL + "/dt/survey/"
  RECIPIENT_DATA_SEARCH = this.API_URL + "recipientData/search"
  DELETE_TAG_UPLOAD_MAP = this.API_URL + "taguploadmap/tagId/"
  TAGS_BY_DATE = this.TAG_URL + "/date/"
  SURVEYS_BY_DATE = this.SURVEY_URL + "/date"
  DOWNLOAD_TAG_DATA = this.API_URL + "export/excel/tag/"
  COUNTRY_URL = this.API_URL + "countries"
  CHECK_USER = this.USER_URL + "search"
  VERIFY_PASSWORD = this.API_URL + "user/verifyPassword"
  ROLE = this.API_URL + "role"
  USER_GROUP = this.API_URL + "userGroup/"
  USER_BY_BUSINESS_URL = this.API_URL + "user/business"
  SURVEY_STATE_URL = this.API_URL + "survey/state"
  SURVEY_BY_PAGE = this.API_URL + "survey/page/"
  USER_SURVEYS = this.API_URL + "survey/user"
  SURVEY_STATE_BY_PAGE = this.API_URL + "survey/state/page/"
  SURVEY_SEARCH_BY_PAGE = this.API_URL + "survey/search/page/"
  ROLEALERT = this.API_URL + "rolealert"
  ROLEALERTMODE = this.API_URL + "rolealertmode"
  UPDATE_PASSWORD_URL = this.PORTAL_API_URL + "user/resetpassword"
  FEED_URL = this.API_URL + "feed"
  FEED_BY_SURVEY_URL = this.API_URL + "feed/survey/"
  EVENT_BY_SURVEY_URL = this.API_URL + "event/survey/"
  SURVEY_REPORT_URL = this.API_URL + "feed/surveyreport/survey/"
  SURVEY_CHARTS_URL = this.API_URL + "event/surveycharts/survey/"
  SURVEY_BY_USERTOKEN_URL = this.API_URL + "usertoken/"
  EVENT_BY_USERTOKEN_URL = this.API_URL + "event/"
  DOWNLOAD_SURVEY_DATA = this.API_URL + "export/excel/dtsurvey/"
  QUESTION_REPORT_URL = this.API_URL + "feed/questionreport/"
  SURVEY_INSTANCE_URL = this.API_URL + "surveyInstance/"
  SHARE_SURVEY = this.SEND_SURVEY_URL + "/share"
  PDF_SURVEY_DATA = this.API_URL + "export/pdf/dtsurvey/"
  FEEDBACK_COUNT_SURVEY_ID = this.API_URL + "feed/feedbackcount/survey/"
  EVENT_COUNT_SURVEY_ID = this.API_URL + "event/count/survey/"
  GET_QR_BY_SURVEY = this.SURVEY_URL + "/qr/"
  UPDATE_QR_DOWNLOAD_COUNT_BY_SURVEY = this.SURVEY_URL + "/qr/track/"
  SURVEY_PROPERTY_URL = this.API_URL + "surveyProperty/"
  KEY_METRIC_BUSINESS_URL = this.API_URL + "keyMetric/business/"
  KEY_METRIC_URL = this.API_URL + "keyMetric"
  SURVEY_FILTER_URL = this.API_URL + "surveyFilter"
  USER_SURVEY_FILTER_URL = this.API_URL + "usersurveyfilter"
  USER_SURVEY_MAP_URL = this.API_URL + "usersurveymap"
  COMPANY_CODE_URL = this.API_URL + "validate/companycode/"
  CHECK_EXISTING_BUSINESS_URL = this.REGISTER_URL + "/checkBusiness/"
  VERSION_URL = this.API_URL + "version"
  USER_ALERT_SETTINGS = this.API_URL + "userAlert"
  BUSINESS_GLOBAL_SETTINGS = this.API_URL + "businesssettings"
  BUSINESS_CONFIG_POST = this.API_URL + "businessconfig"
  BUSINESS_CONFIGURATIONS = this.API_URL + "businessconfig/business/"
  USER_ALERTS_SETTINGS_BY_USER = this.USER_ALERT_SETTINGS + "/user/"
  USER_ALERTS_SETTINGS_BY_BUSINESS = this.USER_ALERT_SETTINGS + "/business/"
  GLOBAL_SETTINGS_BY_BUSINESS = this.BUSINESS_GLOBAL_SETTINGS + "/"
  GET_SCHEDULED_SURVEY_INFO = this.SEND_SURVEY_URL + "/survey/"
  GET_SCHEDULED_SURVEY_BY_UUID = this.SEND_SURVEY_URL + "/surveyuuid/"
  ACCOUNT_BY_ACCESSTOKEN = this.API_URL + "account/accesstoken/"
  GROUP_URL = this.API_URL + "group"
  GROUP_BY_BUSINESS = this.GROUP_URL + "/business/"
  SEARCH_GROUP_BY_NAME = this.GROUP_URL + "/search/"
  GROUP_BY_USER = this.USER_GROUP + "user/"
  SURVEY_QUESTION_URL = this.API_URL + "dtsurvey/"
  HELP_URL = "http://dt-help-center.s3-website.us-east-2.amazonaws.com/help"
  UPLOAD_IMAGE_URL = this.API_URL + "storage"
  DOWNLOAD_SURVEY = this.API_URL + "download"
  // INITIATE_DOWNLOAD_URL = environment.DOWNLOAD_URL + "initiate"
  // DOWNLOAD_STATUS_URL = environment.DOWNLOAD_URL + "status/"
  // DOWNLOAD_ID_URL = environment.DOWNLOAD_URL
  EULA_URL = this.API_URL + "email/eula"
  NOTIFICATION_URL = this.API_URL + "alert"
  //added for participants migration
  TAGS_URL = this.API_URL + "tag"
  TAGS_USER_URL = this.API_URL + "tag/user/"
  PARTICIPANT_GROUP_URL = this.API_URL + "participant/group"
  PARTICIPANT_URL = this.API_URL + "participant/"
  PARATICIPANT_UPLOAD = this.API_URL + "participant/upload"
  METADATA_MAPPING_URL = this.API_URL + "metadatamapping"
  PARTICIPANT_FILTER_URL = this.API_URL + "participant/filter"
  TAGS_PAGE_URL = this.API_URL + "tag/page/"

  //Survey Metric URL - added by selvi (09/09/2019)
  SURVEY_METRIC_URL = this.API_URL + "metric/surveys"

  // Program URL

  PROGRAM_URL = this.API_URL + "program"

  PROGRAMS_URL = this.API_URL + "programs"
  PROGRAMS_SUMMARY_URL = this.PROGRAMS_URL + "/summary"
  DUPLICATE_PROGRAM_URL = this.PROGRAM_URL + "/duplicate"

  // Respondents URL

  RESPONDENT_URL = this.API_URL

  // Advance Scheduling publish

  PUBLISH_PROGRAM_URL = this.API_URL + "publish/v2"
  

  // save and publish

  SAVE_PUBLISH_URL = this.API_URL + "publish/draft"
  GET_PUBLISH_URL = this.SAVE_PUBLISH_URL + "/survey"
  GET_REVIEW_URL =  this.API_URL + "publish/review/programid" 

  // Get saved publish for active survey

  GET_ACTIVE_PROGRAM_URL = this.API_URL + "publish/program"
  //PUBLISH_ACTIVE_PROGRAM_URL = this.GET_ACTIVE_PROGRAM_URL

  //ELIZA URL
  GRAPHQL_PROTOCOL = "https"
  //GRAPHQL_BASEURL = "dtv2egress-dot-airislabs-eliza.appspot.com/"
  GRAPHQL_BASEURL = "dtv2egressqa-dot-airislabs-eliza.appspot.com/"
  GRAPHQL_URL = this.GRAPHQL_PROTOCOL + "://" + this.GRAPHQL_BASEURL + "graphql"

  // Templates

  TEMPLATE_URL = this.API_URL + "template"
  TEMPLATE_PROGRAM_URL = this.TEMPLATE_URL + "/program"
  TEMPLATE_DASHBOARD_URL = this.API_URL + "templates"
  PROGRAM_TEMPLATE_URL = this.API_URL + "program"

  // Text Anlytics

  TEXT_ANALYTICS_URL = environment.TEXT_ANALYTICS_URL
  TEXT_ANALYTICS_MODEL_LIST_URL = this.TEXT_ANALYTICS_URL + "/mapping/settings/models/api/v1?labelTypeId=1"

  INDUSTRY_URL = this.PORTAL_API_URL + "industry"
  TEMPLATES_URL = this.TEMPLATE_API_URL + "program/templates"
  TEMPLATE_PROGRAMS_URL = this.TEMPLATE_API_URL + "program/"
  GAMEPLANS_URL = this.TEMPLATE_API_URL + "program/gameplans"
  GAMEPLAN_PROGRAMS_URL = this.TEMPLATE_API_URL + "program/"

  constructor(private router: Router) {
    //console.log(this.API_URL);
    //console.log(this.WEB_URL);
  }
  ngOnInit() {
    console.log("Configurations")
  }
}
