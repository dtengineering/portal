import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


// component imports
import { ClientDetailsComponent } from "./components/client-details/client-details.component"
import { ClientScreenComponent } from "./components/client-screen/client-screen.component"
import { GrantAccessComponent } from "./components/grant-access/grant-access.component"
import { LoginComponent } from "./components/login/login.component"
import { AccountComponent } from "./components/account/account.component"
import { GrantApprovalComponent } from './components/grant-approval/grant-approval.component';
import { CustomThemeComponent } from './components/custom-theme/custom-theme.component';
import { CustomThemePreviewComponent } from './components/custom-theme-preview/custom-theme-preview.component';
import { CreatetemplatesComponent } from './components/createtemplates/createtemplates.component';
import { TemplatesComponent } from './components/templates/templates.component';
import { TemplatepreviewComponent } from './components/templates/templatepreview/templatepreview.component';
import { GameplansComponent } from './components/gameplans/gameplans.component';
import { CreategameplansComponent } from './components/creategameplans/creategameplans.component';
import { ExploregameplanComponent } from './components/gameplans/exploregameplan/exploregameplan.component';

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "account", component: AccountComponent },
  { path: "client-screen", component: ClientScreenComponent },
  { path: "csaccess", component: GrantAccessComponent },
  { path: "custom-theme", component: CustomThemeComponent },
  { path: "theme-preview", component: CustomThemePreviewComponent },
  { path: "client-details/:id", component: ClientDetailsComponent },
  { path: "approve/:id1/businessid/:id2/id/:id3/:id4", component: GrantApprovalComponent },
  // { path: "approve/1/businessid/049ca6d6-6fb1-4967-a1e4-6f915ec6b20d/id/43353627-014c-4012-b20f-0e5757849a9a/Y2Jyb3duQGRyb3B0aG91Z2h0LmNvbQ==", component: GrantApprovalComponent },
  // { path: "approve/1/businessid/049ca6d6-6fb1-4967-a1e4-6f915ec6b20d/id/43353627-014c-4012-b20f-0e5757849a9a/Y2Jyb3duQGRyb3B0aG91Z2h0LmNvbQ==?utm_source=sendgrid.com&utm_medium=email&utm_campaign=website", component: GrantApprovalComponent },
  // { path: "**", redirectTo: 'login', pathMatch: 'full' },
  { path: "", component: LoginComponent },
  { path: "create-template", component: CreatetemplatesComponent},
  { path: "templates", component: TemplatesComponent},
  { path: "template-preview/:id", component: TemplatepreviewComponent},
  { path: "gameplans", component: GameplansComponent},
  { path: "create-gameplan", component: CreategameplansComponent},
  { path: "explore-gameplan", component: ExploregameplanComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}