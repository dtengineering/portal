import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx'
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {
/**
     * Subject used to send click events to other components
     */
    private paginationClick = new Subject<any>();
    public paginationClickObservable = this.paginationClick.asObservable();

    private initializePage = new Subject<any>();
    public initializePageObservable = this.initializePage.asObservable();

    private pageLoadSuccess = new Subject<any>();
    public pageLoadObservable = this.pageLoadSuccess.asObservable();

    constructor() { }

    /**
     * Function called by the pagination component to indicate page click 
     * @param pageNumber 
     */
    public sendPageNumber(pageNumber) {
        //this.console.consoleLogPrint("Page number " + pageNumber)
        this.paginationClick.next(pageNumber)
    }

    public initializePageProperties(pageData) {
        //this.console.consoleLogPrint("Initialize pagination")
        this.initializePage.next(pageData)
    }
}
