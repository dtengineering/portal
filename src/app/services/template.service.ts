import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { _throw } from 'rxjs/observable/throw';
import { Configurations } from '../configurations/configurations';


const headers = new HttpHeaders().set('Content-Type', 'application/json');
@Injectable({
  providedIn: 'root'
})


export class TemplateService {

  constructor(private http: HttpClient, private config: Configurations) { }

  /**
   * Get all templates
   */
    public getAllTemplate(templateType, pageNo, exactMatch, timezone, language, searchTerm): Observable<any> {
    return this.http.get(this.config.TEMPLATES_URL + '/summary' + '?templateType=' + templateType + '&pageNo=' + pageNo + '&exactMatch=' + exactMatch + '&timezone=' + timezone + '&language=' + language + '&search=' + searchTerm + '&userId=0', { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
   }

    /**
     * Create template
     */
    public createTemplate(programId,templateData): Observable<any> {
      return this.http.post(this.config.TEMPLATE_PROGRAMS_URL + programId + '/template', templateData, { headers : headers })
      .pipe(map(res => res),catchError((error) => { return throwError(() => error);}))
        // .map(res => res)
        // .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }

    /**
     * Get preview template details
     * 
     */
    public getpreviewTemplate(templateId): Observable<any> {
    return this.http.get(this.config.TEMPLATE_PROGRAMS_URL + 'template/'+ templateId, { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }

    /**
     * Get template workflow details
     * 
     */
    public getTemplateWorkflow(businessUUID): Observable<any> {
    return this.http.get(this.config.TEMPLATE_PROGRAMS_URL + 'integration/workflows'+ '?businessUUID=' + businessUUID , { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }

}