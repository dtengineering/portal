import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { RouterModule, Routes, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Configurations } from '../configurations/configurations';

const contentHeaders = new HttpHeaders().set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private router: Router, private http: HttpClient, private _config: Configurations) { }

  authenticateUser(loginForm, email, password, returnUrl) {
    let inputObject = {}
    inputObject["email"] = email
    inputObject["password"] = password
    let inputJson = JSON.stringify(inputObject)
    this.http.post(this._config.LOGIN_URL, inputJson, { headers: contentHeaders })
      .map(res => res).subscribe(
        data => {
          //this.console.consoleLogPrint(data);
          let status = data['success'];
          let response = data['result'];
          if (status === true) {
            sessionStorage.setItem("userUUID", data["result"]["userUUID"])
            sessionStorage.setItem("currentpassword", password)
            this.router.navigate(['/client-screen'])
          }
          else {
            this.router.navigate(['/'])
          }

        },
        error => {
          /* let message = "Invalid Login credentials";
          this.invalidLogin.next(false)
          this.openDialog(); */
          //this.console.consoleLogPrint(message);
          //loginForm.reset();
          //this.router.navigate(['../login']);

        }

      );
  }

  updateUserPassword(data): Observable<any> {
    return this.http.post(this._config.UPDATE_PASSWORD_URL, data, { headers: contentHeaders })
      .map(res => res)
      .catch((error: any) => "Error")
  }
}