import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs';
import { Observable } from 'rxjs/Rx';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Configurations } from '../configurations/configurations';


const headers = new HttpHeaders().set('Content-Type', 'application/json')
@Injectable({
  providedIn: 'root'
})
export class AccessSettingsService {

  constructor(private http: HttpClient, private config: Configurations) { }


  getBusinessResults(){
    return {
      "result": {
          "intentAnalysis": "0",
          "preferredMetric": "0",
          "qrcode": "1",
          "businessId": 1,
          "respondent": "0",
          "bitly": "0",
          "sharableLink": "1",
          "advancedSchedule": "0",
          "notification": "1",
          "businessConfigId": "4d20138f-7c72-43eb-9a5d-295f232f52ca",
          "arabic": "0",
          "segment": "1",
          "sms": "1",
          "english": "0",
          "metadataQuestion": "1",
          "webHooks": "0",
          "email": "1",
          "textAnalytics": "1",
          "contractStartDate": "2019-06-30 17:00:00.0",
          "contractEndDate": "2021-09-29 17:00:00.0",
          "mobileApp": "0",
          "trigger": "1",
          "multiSurveys": "0",
          "business_config_id": 9,
          "emotionAnalysis": "0",
          "logic": "0",
          "kiosk": "1"
      },
      "success": true
  }
  }

  getBusiness(clientUUID): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.get(this.config.PORTAL_API_URL + 'businessconfig/business/' + clientUUID, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }

   createBusiness(clientUUID, payload): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.post(this.config.PORTAL_API_URL + 'businessconfig', payload, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }

  updateBusiness(clientUUID, payload): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.put(this.config.PORTAL_API_URL + 'businessconfig/business/' + clientUUID, payload, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }


  deleteBusinessByBusinessCongigUUID(businessConfigId): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.delete(this.config.PORTAL_API_URL + 'businessconfig/' + businessConfigId, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }

   deleteBusinessByBusinessUUID(clientUUID): Observable<any> {
     //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
     return this.http.delete(this.config.PORTAL_API_URL + 'businessconfig/business/' + clientUUID, {headers : headers})
       .map(res => res)
       .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
 
    }


  public getUsageLimit(clientUUID): Observable<any> {
    let cc: any = localStorage.getItem("cc")
    return this.http.get(this.config.PORTAL_API_URL + "usage/client/" + clientUUID, {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.error || 'Server error'))
  }

  public updateTextAnalyticsData(clientUUID, payload): Observable<any> {
    return this.http.post(this.config.PORTAL_API_URL + "businessconfig/txtanalytics/" + clientUUID, payload, {headers: headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'))   
  }

  public getTextAnalyticsData(clientUUID): Observable<any> {
    return this.http.get(this.config.PORTAL_API_URL + "businessconfig/txtanalytics/" + clientUUID, {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'))   
  }
  /**
   * Get all Text Analytics Model
   */
  getListOfTextAnalyticsModels(): Observable<any> {
    return this.http.get(this.config.TEXT_ANALYTICS_MODEL_LIST_URL, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }

   /**
   * Get program list for a business
   */
  getProgramListForABusiness(clientUUID): Observable<any> {
  
    return this.http.get(this.config.PORTAL_API_URL + 'client/' + clientUUID + '/programs?state=-1&page=-1', {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }

   /**
   * Get program list for a business
   */
  getHistoricalListForABusiness(clientUUID): Observable<any> {
    return this.http.get(this.config.PORTAL_API_URL + 'client/' + clientUUID + '/programs/cs?model=historical', {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
   }

   /**
    *  Set One or All model to the Survey
    */
    setModelToSurvey(clientUUID,payload){
      return this.http.post(this.config.TEXT_ANALYTICS_URL + "/mapping/settings/api/v1/" + clientUUID, payload, {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'))   
    } 

    /**
    *  Get model applied for the client
    */

    getModelAppliedToBusiness(clientUUID){
      return this.http.get(this.config.TEXT_ANALYTICS_URL + "/mapping/settings/api/v1/" + clientUUID, {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'))   
    }

    /**
    *  Get model applied for the client
    */

    getModelAppliedToBusinessReformated(clientUUID){
      return this.http.get(this.config.PORTAL_API_URL + "businessconfig/txtanalytics/" + clientUUID, {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'))   
    }
    
    /**
     * Get advanced templates
     */

     public getAdvancedTemplates(clientUUID,templateType,sortString,pageNo,exactMatch,timezone,language,advanced,search,industry,theme): Observable<any> {
      return this.http.get(this.config.PORTAL_API_URL + 'client/' + clientUUID + "/templates?templateType=" + templateType + "&sortString=" + sortString + "&pageNo=" + pageNo + "&exactMatch=" + exactMatch + "&timezone=" + timezone + "&language=" + language + "&advanced=" + advanced + "&search=" + search + "&industry=" + industry + "&theme=" + theme, {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  
     }

     /**
      * Get industry values
      */
      getTemplateIndustry(){
        let url = this.config.INDUSTRY_URL
        return this.http.get(url, { headers: headers })
        .map(res => res)
        .catch((error: any) => Observable.throwError(error.error || 'Server error')) 
      }

      /**
      * Update templates
      */
      updateTemplates(clientUUID,payload){
        return this.http.put(this.config.PORTAL_API_URL + 'client/' + clientUUID + '/templates', payload, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      }

      /**
   * Get API KEY for a client/business
   */
  getApiKeyForClient(clientUUID): Observable<any> {
  
    return this.http.get(this.config.PORTAL_API_URL + 'client/' + clientUUID + '/key', {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }

   public getAllThemes(): Observable<any> {
    return this.http.get(this.config.PORTAL_API_URL + "theme", {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.error || 'Server error'))
  }

  public getAllThemesByClientId(clientId): Observable<any> {
    return this.http.get(`${this.config.PORTAL_API_URL}client/${clientId}/themes`, {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.error || 'Server error'))
  }

  public updateThemesByClientId(clientId, themes): Observable<any> {
    return this.http.put(`${this.config.PORTAL_API_URL}client/${clientId}/themes`, themes, {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.error || 'Server error'))
  }

  public getPortalDemoAPIKey(): Observable<any> {
    return this.http.get(this.config.PORTAL_API_URL + "demo", {headers : headers})
        .map(res => res)
        .catch((error: any) => Observable.throw(error.error || 'Server error'))
  }

  public getPortalDemoSurveyToken(surveyUUID, apiKey): Observable<any> {
    let cc: any = 'DT';
    let language: any = 'en';
    // Add Authorization token to headers
    const headers1 = new HttpHeaders().set('Content-Type', 'application/json').set('X-DT-API-KEY', apiKey);
    return this.http.get(this.config.STATIC_SURVEY_LINK_URL + 'surveyuuid/' + surveyUUID + '?cc=' + cc + '&language=' + language, { headers: headers1 })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.error || 'Server error'))
  }

  public getTemplateSurveyToken(surveyUUID): Observable<any> {
    let cc: any = 'DT';
    let language: any = 'en';
    return this.http.get(this.config.STATIC_SURVEY_LINK_URL + 'surveyuuid/' + surveyUUID + '?cc=' + cc + '&language=' + language, { headers: headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.error || 'Server error'))
  }

}
