import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { _throw } from 'rxjs/observable/throw';
import { Configurations } from '../configurations/configurations';


const headers = new HttpHeaders().set('Content-Type', 'application/json');
@Injectable({
  providedIn: 'root'
})


export class GameplanService {

  constructor(private http: HttpClient, private config: Configurations) { }

  /**
   * Get all gameplans
   */
    public getAllGameplans(userId, searchTerm?): Observable<any> {
    let url = this.config.GAMEPLANS_URL + '?userId=' + userId;
    if(searchTerm) {
      url = url + '&searchTerm=' + searchTerm;
    }
    return this.http.get(url, { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
   }

    /**
     * Create gameplan
     */
    public createTemplate(userId, state, gameplanData): Observable<any> {
      return this.http.post(this.config.GAMEPLANS_URL + '?userId=' + userId + '&state=' + state, gameplanData, { headers : headers })
      .pipe(map(res => res),catchError((error) => { return throwError(() => error);}))
        // .map(res => res)
        // .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }

    /**
     * Get preview gameplan details
     * 
     */
    public getpreviewGameplan(gamePlanId, language): Observable<any> {
    return this.http.get(this.config.GAMEPLANS_URL + '/' + gamePlanId + '?language=' + language, { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }

    /**
     * Get key features
     */
    public getKeyFeatures(): Observable<any> {
    return this.http.get(this.config.GAMEPLANS_URL + '/retrieveKeyFeatures', { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
    }

}