import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { _throw } from 'rxjs/observable/throw';
import { Configurations } from '../configurations/configurations';


const headers = new HttpHeaders().set('Content-Type', 'application/json')
@Injectable({
  providedIn: 'root'
})


export class ClientServiceService {

  constructor(private http: HttpClient, private config: Configurations) { }

  /**
   * Get all client data
   */
  getAllClientDetails(timezone, searchTerm): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.get(this.config.PORTAL_API_URL + 'client' + '?timezone=' + timezone +'&&searchTerm=' + searchTerm, { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }

  /**
   * Get specific client data
   */
  getClientDetails(clientUUID, timezone): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.get(this.config.PORTAL_API_URL + 'client/' + clientUUID + '?timezone=' + timezone, { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
   }

   createNewClient(payload): Observable<any> {
    let newClientHeaders = new HttpHeaders().set('Content-Type', 'application/json')
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.post(this.config.PORTAL_API_URL + 'client/', payload , {headers : newClientHeaders})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.error || 'Server error'))
   }

   updateClient(payload, businessId): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.put(this.config.PORTAL_API_URL + 'client/' + businessId, payload, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.error || 'Server error'))
   }


   deleteClient(businessId): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.delete(this.config.PORTAL_API_URL + 'client/' + businessId, { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.error || 'Server error'))
   }


   getBusinessDetails(clientUUID): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.get(this.config.PORTAL_API_URL + 'businessconfig/business/' + clientUUID, { headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }

   updateContractDates(clientUUID, payload): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.put(this.config.PORTAL_API_URL + 'client' + '/' + clientUUID, payload, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }


  /**
   * Get specific client data
   */
  requestAccess(clientUUID, payload): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.post(this.config.PORTAL_API_URL + 'client/csaccess', payload, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }

   /**
   * Grant access to client
   */
   grantAccess(payload): Observable<any> {
    //return this.http.get(this.config.TAG_URL + '/' + tagId + '/recipientData?cc=' + localStorage.getItem('cc'), { headers: this.Headers })
    return this.http.post(this.config.PORTAL_API_URL + 'client/grantaccess', payload, {headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
   }

   updateKingProfile(userUUID, payload): Observable<any>{
    return this.http.put(this.config.PORTAL_API_URL + 'client/user' + '/' + userUUID, payload, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
   }

   changeKingProfile(payload, businessUUID): Observable<any>{
    return this.http.put(this.config.PORTAL_API_URL + 'client/' + businessUUID +'/king', payload, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
   }

  resetPasswordOrResendActivationOld(token, activation_flag, login_flag): Observable<any>{
    let api_url = this.config.API_URL
    let host = api_url.replace('dtapp/api/', 'en') 
    return this.http.get(this.config.API_URL + 'authenticate/resetpassword?token=' + token +'&cc=DT&host=' + host + '&language=en&activation=' + activation_flag + '&login=' + login_flag, {headers : headers })
     .map(res => res)
     .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  resetPasswordOrResendActivation(payload): Observable<any>{
    let api_url = this.config.API_URL
    let host = api_url.replace('dtapp/api/', 'en') 
    return this.http.post(this.config.API_URL + 'password/reset',payload, {headers : headers })
     .map(res => res)
     .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  getAccessSettingVersion(businessuuid, pageid, timezone): Observable<any>{
    return this.http.get(this.config.PORTAL_API_URL + 'businessconfigversion/business/' + businessuuid + '?pageId=' + pageid + '&timezone=' + timezone, {headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  enableCustomerServiceAccount(businessuuid): Observable<any>{
    return this.http.post(this.config.PORTAL_API_URL + 'client/' + businessuuid + '/access', {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  disableCustomerServiceAccount(businessuuid): Observable<any>{
    return this.http.delete(this.config.PORTAL_API_URL + 'client/' + businessuuid + '/access', {headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  sendCustomerServiceAccountPasswordReset(businessuuid): Observable<any>{
    return this.http.post(this.config.PORTAL_API_URL + 'client/' + businessuuid + '/cspwdreset', {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  sendRequestApproval(obj): Observable<any>{
    return this.http.post(this.config.PORTAL_API_URL + 'textAnalytics/request', obj, {headers : headers})
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }
  
  getRequestApproval(uuid): Observable<any>{
    return this.http.get(this.config.PORTAL_API_URL + 'textAnalytics/request/client/' + uuid, {headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  approveRequest(obj): Observable<any>{
    return this.http.post(this.config.PORTAL_API_URL + 'textAnalytics/approval', obj, {headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  getMonkeylearnLimits(uuid, email): Observable<any>{
    return this.http.get(this.config.PORTAL_API_URL + 'textAnalytics/approval/id/' + uuid + '/approver/' + email, {headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
  }

  /**
   * Get TA Monkey Learn limits
   */
   getTALimits(clientUUID): Observable<any> {
    return this.http.get(this.config.PORTAL_API_URL + 'client/' + clientUUID + '/ta/limits', {headers : headers })
      .map(res => res)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))

   }
 
}
