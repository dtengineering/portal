import { FormBuilder, Validators, UntypedFormGroup, UntypedFormControl, AsyncValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { Observable } from "rxjs/Rx";

import { AuthenticationService } from "../services/authentication.service";
import { catchError, map } from 'rxjs/operators';

const language = localStorage.getItem('language') ? localStorage.getItem('language') : 'en'

/**
 * Asynchronous validator to check whether the given password is right
 * @param control
 */
export function checkPassword(control: UntypedFormControl) {
    //console.log("Confirm correct password")
    return this._account.checkExistingPassword(control.value, sessionStorage.getItem("userUUID"))
      .map(res => {
        if (res.success) {
          //console.log("Correct password")
          return null
        } else {
          //console.log("Incorrect password")
          this.updateUserForm.controls.currentPasswordControl.setErrors({ invalidPassword: true })
          return { invalidPassword: true }
        }
      })
  }

  /**
 * Update User Password pattern validator
 * @param control
 */
export function updatePwdPatternValidator(control: UntypedFormControl): { [key: string]: any } {
  let minLengthRegex = /^(\w){8,}$/
  let capitalLetterRegex = /[A-Z]/
  let numberRegex = /[0-9]/
  let specialCharacterRegex = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W_]){1,})(?!.*\s).{8,}$/


  if (control.value.length < 8 && control.value.length >= 1) {
    //console.log("Min length case")
    return { invalidLength: true }
  } else if (!capitalLetterRegex.test(control.value) && control.value.length >= 1) {
    //console.log("Capital letter case")
    return { invalidCapital: true }

  } else if (!numberRegex.test(control.value) && control.value.length >= 1) {
    //console.log("Number case")
    return { invalidNumber: true }

  } else if (!specialCharacterRegex.test(control.value) && control.value.length >= 1) {
    //console.log("Special character case")
    return { invalidSpecial: true }

  } else {
    //console.log("Password requirement satisfied")
    return null
  }

}

/**
 * Matching passwords
 * @param passwordKey
 * @param confirmPasswordKey
 */
// FORM GROUP VALIDATORS
export function matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
  return (group: UntypedFormGroup): { [key: string]: any } => {
    let password = group.controls[passwordKey];
    let confirmPassword = group.controls[confirmPasswordKey];
    if (password.value !== confirmPassword.value) {
      return {
        mismatchedPasswords: true
      };
    }
  }
}

export function generalValidator(control: FormControl): { [key: string]: any } {
  //var genRegexp = /^[a-zA-Z0-9!"$%&'<@#(.:? )>*\+,\/;\[\\\]\^\'_`{|}~=-–]+$/
  var genRegexp = /^[a-zA-Z0-9!"$%&’'<@#(.:? -)>*\+,\/;\[\\\]\^\'_`{|}~=-]+$/
  if(language === 'ar'){
    if (control.value && control.value.trim().length === 0){
      return { invalidText: true };
    }
  }
  else{
    if (control.value && !genRegexp.test(control.value)) {
      return { invalidText: true };
    }
 }
}

export function templateNameExists(control: FormControl) {
  // console.log("business name validator")
  let templateName: any = control.value
  let templateType = 'dropthought'
  console.log("I am inside template name exists validator")
  console.log(templateName)
  let pageId = -1
  let exactMatch = false
  let timezone:string = Intl.DateTimeFormat().resolvedOptions().timeZone
  let templateExists = null
  if (templateName && templateName.trim().length > 0) {
    return this.templateService.getAllTemplate(templateType, pageId, exactMatch, timezone, this.language, templateName.trim())
    .pipe(map(res => {
        console.log(res)
        if (res && res['result'] && res['result'].length > 0) {
          this.createTemplateSurveyForm.controls.createTemplateName.setErrors({ templateExists: true })
          return { templateExists: true }
        } else {
          console.log("Survey name does not exist")
          return null
        }

      }
    ),catchError(err => {return err;}))
  }
  else {
    //console.log("else case")
    templateExists = null
    //return companyExists
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(templateExists)
      })
    })
  }
}

  export function gameplanNameExists(control: FormControl) {
    // console.log("business name validator")
    let gameplanName: any = control.value
    // let templateType = 'dropthought'
    console.log("I am inside gameplan name exists validator")
    console.log(gameplanName)
    let userId = 0
    let gameplanExists = null
    if (gameplanName && gameplanName.trim().length > 0) {
      return this.gameplanService.getAllGameplans(userId, gameplanName.trim())
      .pipe(map(res => {
          console.log(res)
          if (res && res['result'] && res['result'].length > 0) {
            this.createGameplanSurveyForm.controls.createGameplanName.setErrors({ gameplanExists: true })
            console.log("Gameplan name exists", this.createGameplanSurveyForm.controls.createGameplanName.hasError('gameplanExists'))
            return { gameplanExists: true }
          } else {
            console.log("Gameplan name does not exist")
            return null
          }
  
        }
      ),catchError(err => {return err;}))
    }
    else {
      gameplanExists = null
      return new Observable(observer => {
        setTimeout(() => {
          observer.next(gameplanExists)
        })
      })
    }

  }