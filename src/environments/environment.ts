// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  logging: true,
  production: false,
  prodme: false,
  prodin: false,
  sandbox: false,
  sandboxin: false,
  dr: false,
  drus: false,
  demo: false,
  email: true,
  sms: true,
  qrcode: true,
  sharableLink: true,
  kiosk: true,
  logic: true,
  theme: true,
  analytics: true,
  respondent: true,
  segment: true,
  metadataQuestion: true,
  ratings: false,
  notification: true,
  VERSION_URL: 'http://localhost:4200/',
  API_URL: 'http://localhost:8080/dtapp/api/',
  DOWNLOAD_URL: 'https://test.dropthought.com/api/v1/download/',
  HELP_URL: 'https://dt-help-center.s3-website.us-east-2.amazonaws.com/help',
  PUBLISH_BANNER_URL: 'https://ops.dropthought.com/api/v2/storage/image',
  GRAPHQL_URL: 'https://hi5mysqlegress-dot-airislabs-eliza.appspot.com/graphql',
  GRAPHQL_URL_PROD:
    'https://hi5mysqlegress-dot-airislabs-eliza.appspot.com/graphql',
  /*ELIZA_SUMMARY_URL:
    "https://dtv2-data-api-qa-dot-airislabs-eliza.appspot.com/eliza/data/summary/api/v1",
  ELIZA_URL:
    "https://dtv2-data-api-qa-dot-airislabs-eliza.appspot.com/eliza/data/api/v1", */
  ELIZA_SUMMARY_URL: 'https://stage-api.dropthought.com/eliza/data/summary/api/v1',
  ELIZA_URL: 'https://stage-api.dropthought.com/eliza/data/api/v1',
  ELIZA_WORDCLOUD_URL: 'https://stage-api.dropthought.com/eliza/data/wordcloud/api/v1',
  ELIZA_FEEDBACK_URL: 'https://stage-api.dropthought.com/eliza/data/feedback/api/v1',
  ELIZA_EXPORT_CSV_URL:
    'https://dtv2-data-api-qa-dot-airislabs-eliza.appspot.com/eliza/data/export/csv/api/v1',
  ELIZA_EXPORT_XLS_URL:
    'https://dtv2-data-api-qa-dot-airislabs-eliza.appspot.com/eliza/data/export/xls/api/v1',
  PORTAL_URL: 'http://localhost:8088/api/v1/portal/',
  TEMPLATE_URL: 'http://localhost:8088/api/v1/',
  TEMPLATE_PREVIEW_URL: 'https://stage-eux.dropthought.com/',
  TEXT_ANALYTICS_URL: 'https://stage-api.dropthought.com/eliza',
  SERVICE_WORKER: true
};
