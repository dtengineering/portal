export const environment = {
  logging: false,
  production: false,
  prodme: false,
  prodin: false,
  sandbox: false,
  sandboxin: false,
  dr: true,
  drus: false,
  demo: false,
  email: true,
  sms: true,
  qrcode: true,
  sharableLink: true,
  kiosk: true,
  logic: true,
  theme: true,
  analytics: true,
  respondent: true,
  segment: true,
  metadataQuestion: true,
  ratings: true,
  notification: true,
  VERSION_URL: 'https://api-dr.dropthought.com/',
  API_URL: 'https://api-dr.dropthought.com/dtapp/api/',
  DOWNLOAD_URL: 'https://api-dr.dropthought.com/api/v1/download/',
  HELP_URL: 'https://dt-help-center.s3-website.us-east-2.amazonaws.com/help',
  PUBLISH_BANNER_URL: 'https://ops.dropthought.com/api/v2/storage/image',
  ELIZA_SUMMARY_URL: 'https://api-dr.dropthought.com/eliza/data/summary/api/v1',
  ELIZA_URL: 'https://api-dr.dropthought.com/eliza/data/api/v1',
  ELIZA_FEEDBACK_URL: 'https://test.dropthought.com/eliza/data/feedback/api/v1',
  ELIZA_WORDCLOUD_URL: 'https://api.dropthought.com/eliza/data/wordcloud/api/v1',
  PORTAL_URL: 'https://api-dr.dropthought.com/api/v1/portal/',
  TEMPLATE_URL: 'https://api-dr.dropthought.com/api/v1/',
  TEMPLATE_PREVIEW_URL: 'https://eux-dr.dropthought.com/',
  TEXT_ANALYTICS_URL: 'https://api-dr.dropthought.com/eliza',
  SERVICE_WORKER: true
};
