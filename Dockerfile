FROM nginx:latest
LABEL maintainer="Mohan Doss K <mhdos@dropthought.com>"
COPY dist/portal /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf
